package gens;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RangomGraph {

	static BufferedWriter out = null;

	static Map<Integer,Set<Integer>> g;
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		int n = 5000;  //number of nodes
		int p = 5000;
		
		String[] argv = {""+n, ""+p};

		if(args.length < 1) args = argv;
		
		String folder = "C:\\graphs\\";
		
		String fname = args[0]+"-"+args[1];
		String finFname = fname;
		
		File f = new File(folder+finFname);
		int i = 0;
		while(f.exists())
		{
			finFname = fname+"-0"+i;
			f = new File(folder+finFname);
			i++;
			System.out.println("changing"+i);
		}
		
		out = new BufferedWriter(new FileWriter(f), 10000) ;
		
		generate(Integer.parseInt(args[0]), Integer.parseInt(args[1]), out);

		out.flush();
		out.close();
	}
	

	public static void generate(int n, int p, Writer out) throws IOException
	{
		
		out.write("header: n= "+n+"\r");
		
		for(int i = 0; i < n-1; i++)
		{
			if(i%50 == 0) System.out.println("Doing: "+i);
			
			for(int j = i+1; j < n; j++)
			{
				if(Math.random()*100 < p) out.write("e "+i+" "+j+"\r");
			}
			
		}
			
	}
	public static void generateNM(int n, int m, PrintStream out) throws IOException
	{
		out.println("header: n= "+n+" m="+m);
		
		g = new HashMap<Integer, Set<Integer>>();
		
		for(int i = 0; i<n; i++)
		{
			g.put(i, new HashSet<Integer>());
		}
		
		int a,b;
		
		int tries;
		boolean almostFull = false;
		
		for(int i = 0; i < m; i++)
		{	
			tries = 0;
			
			do
			{
				if(almostFull)
				{
					a = (int) (Math.random()*n);
					
					while(g.get(a).size() >= n - a - 1)  //we look for a node that's not connected to at least 1 node greater than it
					{
						a--;
						if(a < 0) a = n-1;
					}
					
					if(!forceConnect(a,n))
					{
						System.out.println("FORCE CONNECT FAILED. EXITING");
						System.exit(0);
					}
					break;
				}
				else
				{
					if(tries>100){
						almostFull = true;
						System.out.println("Almost full! n="+n+" m="+m);
					}
				
					a = (int)(Math.random()*n);
					b = (int)(Math.random()*n);
					tries++;
				}
			}
			while ( !connect(a,b) );
			
		}
		
		printGraph(out);
			
	}	
	
	public static String str(int i)
	{
		if(i >= 10000) return ""+i;
		if(i >= 1000) return "0"+i;
		if(i >= 100 ) return "00"+i;
		if(i >= 10  ) return "000"+i;
		return "0000"+i;
	}
	
	private static boolean connect(int a, int b)
	{
		if(a > b)
		{
			return g.get(b).add(a);
		}
		else if( b > a)
		{
			return g.get(a).add(b);
		}
		
		return false;
	}
	
	public static boolean forceConnect(int a, int n)
	{
		int b = (int)(Math.random()*n);
		
		for(int i = 0; i < n; i++)
		{
			if(connect(a, (b+i)%(n)   )) return true;
		}
		return false;
	}
	
	private static void printGraph(PrintStream out)
	{
		for(int i : g.keySet())
		{
			for(int j : g.get(i))
			{
				out.print("e "+i+" "+j+"\r\n");
			}
		}
	}
}
