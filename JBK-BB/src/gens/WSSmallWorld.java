package gens;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WSSmallWorld {

	static BufferedWriter out,out2, out3 ;//= System.out;
//	static BufferedWriter writer;
//	static StringBuffer buff = new StringBuffer();
	static List<Set<Integer>> g;
	
	static long t0 = System.currentTimeMillis();
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		int n = 20000;  //number of nodes
		int socSize = 300;//200; //15 //number of local edges
		double totJumps = 5000.; //total target number of jumps to have in the graph (approx).
		
		int totArches = n*socSize/2;
		double jumpChance = totJumps/totArches;
		
		String[] argv = {""+n,""+jumpChance, ""+socSize};

		if(args.length < 1) args = argv;
		
		String folder = "C:\\Users\\Alessio\\Documents\\GEPHI\\tesi\\";
		String extension = ".snc";
		
		String fname = "SW WS n"+args[0]+" b"+args[1]+" k"+args[2];
		String finFname = fname;
		
		File f = new File(folder+finFname+extension);
		int i = 0;
		while(f.exists())
		{
			finFname = fname+"-0"+i;
			f = new File(folder+finFname+extension);
			i++;
			System.out.println("changing"+i);
		}
		
//		out = new PrintStream(f);
//		out = System.out;
		out = new BufferedWriter(new FileWriter(f, true),10000);
		out2 = new BufferedWriter(new FileWriter(f+".g", true),10000);
		out3 = new BufferedWriter(new FileWriter(f+".clq", true),10000);
		
		generate(Integer.parseInt(args[0]), Double.parseDouble(args[1]), Integer.parseInt(args[2]));

	}
	
	
	public static void generate(int n, double beta, int socSize) throws IOException
	{
		double factor;
		boolean inserted;
		int temp;
		
		//init structure
		g = new ArrayList<Set<Integer>>(n);
		
		for(int i = 0; i < n; i++) g.add(new HashSet<Integer>(socSize/2)); // g.add(new TreeSet<Integer>());
		
		//create a lattice with k/2 connection in each direction for each node
		//(since the graph is undirected, i < j -> (i,j) is represented, (j,i) is implicit
		for(int i = 0; i < n; i++)
		{
			long t1 = System.currentTimeMillis();
			if(i%100 == 0)
			{
				System.out.println("done "+i+" - "+(t1-t0));
				t0 = t1;
			}
			
			for(int j = 1; j <= socSize/2; j++)
			{
					connect(i, (i+j)%n);
			}
			
		}
		
		//replace edges with random edges.
		Set<Integer> toRemove = new HashSet<Integer>(socSize/2);
		
		for(int i = 0; i < n; i++)
		{
			//replace every edge with a random edge, with probability "beta"
			for(int j : g.get(i))
			{
				if(Math.random() < beta)
				{
					toRemove.add(j);
				}
			}
			
			g.get(i).removeAll(toRemove);
			
			for(int j = 0; j < toRemove.size(); j++)
			{
				while(true)   //connect i to a new random edge, which is not i and not already connected to i
				{
					if(connect(i,(int) (Math.random()*n))) break;
				}
			}
			
			toRemove.clear();
		}
			
		//now write the graph
		printGraph();
		
	}
	
	private static boolean connect (int a, int b)
	{
		if(a < b)
		{
			return g.get(a).add(b);
		}
		else if (a > b)
		{
			return g.get(b).add(a);
		}
		else
		{
			return false;
		}
	}

	private static void printGraph () throws IOException
	{
//		out.write("[WS-Small-World] n= "+g.size()+"\r\n");
		out3.write("[WS-Small-World] n= "+g.size()+"\r\n");
		System.out.println("printing SNC+GEPHI format");
		for(int i = 0; i < g.size(); i++)
		{
			for(int j : g.get(i))
			{
				out2.write(str(i)+";"+str(j)+"\r\n");
				out.write(i+" e "+j+"\r\n");
				out3.write("e "+i+" "+j+"\r\n");
			}
		}
		
		out.flush();
		out.close();
		out2.flush();
		out2.close();
		out3.flush();
		out3.close();
		
	}
	
	private static String str(int i)
	{
		if(i >= 10000) return ""+i;
		if(i >= 1000) return "0"+i;
		if(i >= 100 ) return "00"+i;
		if(i >= 10  ) return "000"+i;
		return "0000"+i;
	}
}
