package gens;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;

public class MultipleGenerator {

public static void main(String[] args) throws Exception {
	
	String folder = "C:\\Users\\Alessio\\Documents\\UNI\\TESI-PATRICK\\Graphs\\random\\BASW-n1000";
	
	int graphs = 100;
	
	int n = 1000;
	int p0 = 0;
	int pstep = 1;
	
//	generateFixedN(graphs, n, p0, pstep, folder);
	
//	generateFixedM(100, 4800, 100, 1, folder);
	
	generateBASWfixedN(graphs, n, 10, 5, folder);
}

	public static void generateBASWfixedN(int graphs, int n, int mstart, int mstep, String folder) throws IOException
	{
		File fold = new File(folder);
		
		fold.mkdirs();

		int m = mstart;
		int m0 = m;
		
		for(int i = 0; i < graphs; i++)
		{
			/*if(i%10 == 0)*/ System.out.println("Generating "+i);
			
			File f = getFile(folder, "BASW-"+n+"-"+m);
			
			BufferedWriter out = new BufferedWriter(new FileWriter(f) , 10000 );
			
			BASmallWorld.generate(n, m0, m , out);
			
			out.flush();
			out.close();
			
			m += mstep;
			m0 = m;
		}
	}

	public static void generateFixedN(int graphs, int n, int p0, int pstep, String folder) throws Exception
	{
		File fold = new File(folder);
		
		fold.mkdirs();

		int p = p0;
		
		for(int i = 0; i < graphs; i++)
		{
			File f = getFile(folder, n, p);
			
			Writer out = new BufferedWriter(new FileWriter(f), 10000);
			
			RangomGraph.generate(n, p, out);
			out.close();
			
			p += pstep;
		}
	}
	
	public static void generateFixedM(int graphs, int m, int n0, int nstep, String folder) throws Exception
	{
		File fold = new File(folder);
		
		fold.mkdirs();

		int n = n0;
		
		for(int i = 0; i < graphs; i++)
		{
			/*if(i%10 == 0)*/ System.out.println("Generating "+i);
			
			File f = getFile(folder, n, m);
			
			PrintStream out = new PrintStream(f);
			
			RangomGraph.generateNM(n, m, out);
			
			n += nstep;
		}
	}


	public static File getFile(String folder, int n, int p)
	{
		String fname = folder+"\\"+n+"-"+p;
		
		int i = 0;
		
		File f;
		
		do
		{
			f = new File(fname+"-0"+i);
			i++;
		}
		while(f.exists());
		
		return f;
	}

	public static File getFile(String folder, String gname)
	{
		String fname = folder+"\\"+gname;
		
		int i = 0;
		
		File f;
		
		do
		{
			f = new File(fname+"-0"+i);
			i++;
		}
		while(f.exists());
		
		return f;
	}

}
