package gens;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BASmallWorld {

//	static PrintStream out = System.out;
//	static BufferedWriter writer;
//	static StringBuffer buff = new StringBuffer();
	static List<Set<Integer>> g;
	static List<Integer> linksPool;
	static int temp;
	
	static BufferedWriter out, out2, out3;// = new PrintStream(f);
	
	static long t0 = System.currentTimeMillis();
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		int n = 50000;  //number of nodes
		int m0 = 200;//100; //
		int m = 100;//50;  //
		
		String[] argv = {""+n,""+m0, ""+m};

		if(args.length < 1) args = argv;
		
		String folder = "C:\\Users\\Alessio\\Documents\\GEPHI\\tesi\\";
		String extension = ".snc";
		
		String fname = "BASW n"+args[0]+" mz"+args[1]+" m"+args[2];
		String finFname = fname;
		
		File f = new File(folder+finFname+extension);
		int i = 0;
		while(f.exists())
		{
			finFname = fname+"-0"+i;
			f = new File(folder+finFname+extension);
			i++;
			System.out.println("changing"+i);
		}
		
		
//		out = System.out;
		out = new BufferedWriter(new FileWriter(f, true),10000);
		out2 = new BufferedWriter(new FileWriter(f+".g", true),10000);
		out3 = new BufferedWriter(new FileWriter(f+".clq", true),10000);
		
		generate(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), out);
	}
	
	
	public static void generate(int n, int m0, int m, BufferedWriter out) throws IOException
	{
		
		//init structure
		g = new ArrayList<Set<Integer>>(n);
		
		for(int i = 0; i < n; i++) g.add(new HashSet<Integer>(m)); // g.add(new TreeSet<Integer>());
		
		linksPool = new ArrayList<Integer>(n*m);
		
		//init the starting graph
		//for now: create a complete graph
		initTree(g,m0);
		
		System.out.println("graph initialized");
		
		//add the new edges and connect them.
		
		for(int i = m0; i < n; i++)
		{
			if(i%10 == 0) System.out.println("Doing: "+i);
			
			//connect the new node to m old nodes, choosing them with probability influenced by the degrees of the nodes
			for(int j = 0; j < m; j++)
			{
				int xx = 0;
				do
				{
					xx++;
					if(xx>10 && xx%1000 == 0) System.out.println("xx = "+xx);
					//the number of times a node appears in linksPool is given by the node's degree.
					//hence getting a random node from linksPool will give every node with probability d(i)/m   (degree of the node / total number of edges so far),
					//which is exactly the formula given by the Barbasi-Albers model.
					temp = getRandomFrom(linksPool);
				}
				while(!connect(temp,i));  //get another candidate if the current one is connecter to i or is i itself.

			}
			
		}
			
		//now write the graph
		printGraph(out);
		
	}
	
	private static int getRandomFrom(List<Integer> l) {

		return l.get( (int)(Math.random()*l.size()) );
	}


	private static void initGraphComplete(List<Set<Integer>> g, double m0) {
		//for now, this is a complete graph with labels from 0 to m0-1
		//(since the graph is undirected, i < j -> (i,j) is represented, (j,i) is implicit
		
		
		for(int i = 0; i < m0 -1 ; i++)
		{
			if(i%100 == 0)
			{
				long t1 = System.currentTimeMillis();
				System.out.println("done "+i+" - "+(t1-t0));
				t0 = t1;
			}
			
			for(int j = i+1 ; j < m0; j++)
			{
					connect(i, j);
			}
		}
	}


	private static void initTree(List<Set<Integer>> g, double m0) {
		//for now, this is a complete graph with labels from 0 to m0-1
		//(since the graph is undirected, i < j -> (i,j) is represented, (j,i) is implicit
		
		connect(0,1);
		
		for(int i = 2; i < m0 ; i++)
		{
			if(i%100 == 0)
			{
				long t1 = System.currentTimeMillis();
				System.out.println("done "+i+" - "+(t1-t0));
				t0 = t1;
			}
			connect(i,getRandomFrom(linksPool));
		}
	}


	private static boolean connect (int a, int b)
	{
		if(a < b)
		{
			if(g.get(a).add(b))
			{
				linksPool.add(a);
				linksPool.add(b);
				return true;
			}
		}
		else if (a > b)
		{
			if(g.get(a).add(b))
			{
				linksPool.add(a);
				linksPool.add(b);
				return true;
			}
		}
		
		return false;
	}

	private static void printGraph (BufferedWriter out) throws IOException
	{
//		out.write("[BA-Small-World] n= "+g.size()+"\r\n");
		out3.write("[BA-Small-World] n= "+g.size()+"\r\n");
		System.out.println("printing DIMACS format");
		for(int i = 0; i < g.size(); i++)
		{
			for(int j : g.get(i))
			{
				out2.write(str(i)+";"+str(j)+"\r\n");
				out.write(i+" e "+j+"\r\n");
				out3.write("e "+i+" "+j+"\r\n");
			}
		}
		out.flush();
		out.close();
		out2.flush();
		out2.close();
		out3.flush();
		out3.close();
	}
	
	private static String str(int i)
	{
		if(i >= 10000) return ""+i;
		if(i >= 1000) return "0"+i;
		if(i >= 100 ) return "00"+i;
		if(i >= 10  ) return "000"+i;
		return "0000"+i;
	}
}
