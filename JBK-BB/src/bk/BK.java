package bk;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lib.AlgStats;
import lib.SearchAlg;
import lib.graph.Graph;

// template for Bron Kerbosch
public abstract class BK implements SearchAlg {

    protected int cliques;	// number of maximal cliques
    protected long nodes;     // number of decisions
    protected long timeLimit; // milliseconds
    protected long startTime;   // milliseconds
    protected long endTime;	// milliseconds
    protected int maxSize;    // size of max clique
    protected boolean storeSolution = true; //false: faster, but only records number of cliques and max size
	public static boolean DEBUG = (System.getProperty("DEBUG") != null);
    public boolean aborted = false;

    protected Set<Integer> kernel = null;
    protected Set<Integer> borderVis = null;
    
    
    public BK (Graph graph) {
	nodes = maxSize = 0;
	startTime = timeLimit = -1;
    }

    public BK (InputStream is) {
    	throw new RuntimeException("This constructor must be overridden.");
	}
    
	public void search(){
	nodes                = 0;
	
	if(DEBUG) System.out.println("Starting search; Timeout: "+timeLimit+"ms");
	
	startTime              = System.currentTimeMillis();
	
	start();
	
	endTime	= System.currentTimeMillis();

    }
	

//    public abstract void setFilters(List<Integer> toVisit, List<Integer> excluded, Set<Integer> required, int requiredNum, int minSize);
	
	public abstract void start();
	
	
	public void setTimeLimit(long limit){
		this.timeLimit = limit;
	}

	@Override
	public void printStats(){
		System.out.println("Cliques: "+cliques);
		System.out.println("MaxSize: "+maxSize);
		System.out.println("Nodes: "+nodes);
		System.out.println("Time: "+(endTime-startTime));
		System.out.println("Aborted: "+aborted);
	}
	
	@Override
	public AlgStats getStats(){
		
		Map<String,String> stats = new HashMap<String,String>();
		
		stats.put("clq", ""+cliques);
		stats.put("nodes", ""+nodes);
		stats.put("time", ""+(endTime-startTime));
		stats.put("max", ""+maxSize);
		stats.put("limit", ""+timeLimit);
		stats.put("alg",this.getClass().getName());
		
		return new AlgStats(stats);
	}
	
	@Override
	public void setAborted(){
		this.endTime = System.currentTimeMillis();
		this.aborted = true;
	}

	
	@Override
	public void setFilters(Set<Integer> kernel, Set<Integer> borderVis) {
		this.kernel = kernel;
		this.borderVis = borderVis;
	}
}
