package bk.lists;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import lib.DegeneracyUtils;
import lib.graph.Graph;

public class Eppstein extends Tomita {

	public Eppstein(Graph g){
		super(g); 
	}

    public Eppstein(InputStream is) {
		super(is);
	}

    @Override
	public void start(){
    	
    	List<Integer> P = DegeneracyUtils.degeneracyOrderingList(this.g);
    	
    	int maxlabel = 0;
    	for(int n : P){
    		if(n>maxlabel) maxlabel = n;
    	}
    	
    	int[] position = new int[maxlabel+1];
    	int size = P.size();
    	
    	for(int i = 0; i<size; i++){
    		position[P.get(i)] = i;
    	}
    	
    	
    	List<Integer> newC,newP,newX;
    	
    	
    	for(int i = 0; i<size; i++){
    		int vi = P.get(i);
    		
    		newC = new ArrayList<Integer>();
    		newC.add(vi);
    		
    		newP = degenIntersect(g.neighbors(vi),position,i+1,size);
    		newX = degenIntersect(g.neighbors(vi),position,0,i-1);
    		
    		expand(newC,newP,newX);
    	}
    	
	
    
    }

    private List<Integer> degenIntersect(List<Integer> neighs, int[] position, int start, int end){
    	
    	List<Integer> res = new ArrayList<Integer>(neighs.size());
    	
    	for(int n : neighs){
    		if(position[n] >= start && position[n] <= end){
    			res.add(n);
    		}
    	}
    	
    	return res;
    }

}
