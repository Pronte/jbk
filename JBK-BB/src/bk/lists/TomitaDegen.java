package bk.lists;


import java.util.*;
import java.io.*;

import lib.DegeneracyUtils;
import lib.ListUtils;
import lib.graph.IntGraph;

public class TomitaDegen extends Tomita {

	public TomitaDegen(IntGraph g){
		super(g); 
	}

    public TomitaDegen(InputStream is) {
		super(is);
	}

    @Override
	public void start(){
    	
    	List<Integer> P = DegeneracyUtils.degeneracyOrderingList(this.g);
    	
    	List<Integer> X = new ArrayList<Integer>();
    	
    	List<Integer> newC,newP,newX;
    	
    	int size = P.size();
    	
    	for(int i = 0; i<size; i++){
    		int vi = P.remove(0);
    		
    		newC = new ArrayList<Integer>();
    		newC.add(vi);
    		
    		newP = ListUtils.intersect(P,g.neighbors(vi));
    		newX = ListUtils.intersect(X,g.neighbors(vi));
    		
    		expand(newC,newP,newX);
    		
    		
    		X.add(vi);
    	}
    	
	
    
    }


}
