package bk.lists;


import java.io.InputStream;
import java.util.List;

import lib.ListUtils;
import lib.graph.Graph;

public class TomitaOrd extends BK1Ord {
	
	//stores the total amount of time used to compute the pivots
	protected long pivotCompTime = 0;
	
	//store the total time used to add elements to X
	protected long addToXTime = 0;

	public TomitaOrd(Graph g){
		super(g); 
	}

    public TomitaOrd(InputStream is) {
		super(is);
	}

    void expand(List<Integer> C,List<Integer> P,List<Integer> X){
		if (timeLimit > 0 && System.currentTimeMillis() - startTime >= timeLimit) return;
		nodes++;
	
		if (P.isEmpty()){
			if(X.isEmpty()) {report(C);}
			return;
		}
		
		int u = getTomitaPivot(P, X);
		
//		List<Integer> tP = ListUtils.removeOrdered(P,g.neighbors(u)); //tP still ordered		
		
		int i = 0;
		
		while(i < P.size())
		{
			if(!g.areNeighbors(u, P.get(i)))
				process(i,C,P,X); 
			else
				i++;
		}
    }
    

    //in BK1 param "i" indicates the index of the node in P. Here is the actual label of the node.
    void process (int i, List<Integer> C, List<Integer> P, List<Integer> X){
    	
    	int v = P.remove(i); //even though the iteration is done on a subset of the elements, elements are only removed from P so P stays ordered.
    	
    	
	    C.add(v);

	    List<Integer> newP = intersect(P,g.neighbors(v));
	    List<Integer> newX = intersect(X,g.neighbors(v));	    
	    
	    
	    expand(C,newP,newX);
	    
	    C.remove((Integer)v);
	    
	    //the pivoting strategy can scramble the visit order,
	    //so elements are not inserted in X in order, hence we need to insert in order.
	    long t0 = System.currentTimeMillis();
	    ListUtils.addOrdered(X,(Integer)v);
	    addToXTime += (System.currentTimeMillis()-t0);
    }


	/**
     *Tomita's Pivoting strategy.
     *The pivot is chosen as the node in P U N
     *with the highest number of neighbors in P.
     */
    protected int getTomitaPivot(List<Integer> P, List<Integer> X){
    	
    	long t0 = System.currentTimeMillis();
    	
    	int best = P.get(0);
    	int bestVal = neighborsIn(P,best);

    	int node;
    	int val;

    	for(int i = 1; i<P.size(); i++){
    		node = P.get(i);
    		val = neighborsIn(P,node);

    		if(val>bestVal){
    			best = node;
    			bestVal = val;
    		}
    	}

    	//If X is not empty, extend the search on X
    	if(!X.isEmpty()){
    		for(int i = 0; i<X.size(); i++){
    			node = X.get(i);
    			val = neighborsIn(P,node);

    			if(val>bestVal){
    				best = node;
    				bestVal = val;
    			}
    		}
    	}
    	
    	pivotCompTime += (System.currentTimeMillis() - t0);

    	return best;
    }

    protected int neighborsIn(List<Integer> P, int node){
    	return ListUtils.intersectOrdered(P, g.neighbors(node)).size();
    }


    @Override
	public void printStats(){
    	super.printStats();
    	System.out.println("PivotCompTime: "+pivotCompTime);
    	System.out.println("addToXTime: "+addToXTime);
    }
}
