package bk.lists;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import run.SNCAccessPoint;

import com.sun.jndi.url.corbaname.corbanameURLContextFactory;

import lib.DegeneracyUtils;
import lib.graph.Graph;

public class EppsteinOrd extends TomitaOrd {

	public EppsteinOrd(Graph g){
		super(g); 
	}

    public EppsteinOrd(InputStream is) {
		super(is);
	}

    @Override
	public void start(){
    	
    	List<Integer> P = DegeneracyUtils.degeneracyOrderingList(this.g);
    	
    	int maxlabel = 0;
    	for(int n : P){
    		if(n>maxlabel) maxlabel = n;
    	}
    	
    	//kernel nodes must come first in the ordering
    	//borderVis nodes must be removed (i.e. put first in the ordering)
    	//and normal border must be after all kernels
    	//but we want to preserve the relative ordering of kernel nodes
    	//i.e. |-vvvvvvvvkkkkkkkkkbbbbbbb-|
    	if(kernel != null || borderVis != null)
    	{
    		List<Integer> tx = new ArrayList<Integer>(),tp = new ArrayList<Integer>(),tb = new ArrayList<Integer>();
    		
    		for(int i : P)
    		{
    			if(borderVis.contains(i)) tx.add(i);
    			else if (kernel.contains(i)) tp.add(i);
    			else tb.add(i);
    		}
    		
    		for(int i = 0; i < P.size(); i++)
    		{
    			if(i<borderVis.size()) P.set(i, tx.get(i));
    			else if (i < borderVis.size()+kernel.size()) P.set(i, tp.get(i-borderVis.size()));
    			else P.set(i, tb.get(i-borderVis.size()-kernel.size()) );
    		}
    	}
    	
    	
    	int[] position = new int[maxlabel+1];
    	int size = P.size();
    	
    	for(int i = 0; i<size; i++){
    		position[P.get(i)] = i;
    	}
    	
    	
    	List<Integer> newC,newP,newX;
    	
    	if(kernel != null || borderVis != null)
    	{

    		for(int i = 0; i<P.size(); i++){
        		int vi = P.get(i);
        		
        		if(!kernel.contains(vi)) continue;
        		
        		newC = new ArrayList<Integer>();
        		newC.add(vi);
        		
        		//the sets maintain the pre-existent label ordering
        		newP = degenIntersect(g.neighbors(vi),position,i+1,size);
        		newX = degenIntersect(g.neighbors(vi),position,0,i-1);
        		
        		expand(newC,newP,newX);
        	}
    	
    	}
    	else
    	{
    	
	    	for(int i = 0; i<P.size(); i++){
	    		int vi = P.get(i);
	    		
	    		newC = new ArrayList<Integer>();
	    		newC.add(vi);
	    		
	    		//the sets maintain the pre-existent label ordering
	    		newP = degenIntersect(g.neighbors(vi),position,i+1,size);
	    		newX = degenIntersect(g.neighbors(vi),position,0,i-1);
	    		
	    		expand(newC,newP,newX);
	    	}
    	
    	}
    
    }

    private List<Integer> degenIntersect(List<Integer> neighs, int[] position, int start, int end){
    	
    	List<Integer> res = new ArrayList<Integer>(neighs.size());
    	
    	for(int n : neighs){
    		if(position[n] >= start && position[n] <= end){
    			res.add(n);
    		}
    	}
    	
    	return res;
    }
    
}
