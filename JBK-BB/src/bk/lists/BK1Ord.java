package bk.lists;


import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import lib.ListUtils;
import lib.graph.Graph;
import lib.graph.MapListGraph;


//orders the neighbours lists in the graph to optimize intersection.
public class BK1Ord extends BK1 {

	private long orderingTime = -1;
	private List<Integer> orderedNodes = null;
	
	public BK1Ord(Graph g){
		super(g); 
	}

    public BK1Ord(InputStream is) {
		super(is);
	}
    
    @Override
	public void search(){
    	orderGraph();
    	super.search();
    }
    
    private void orderGraph() {
    	long t0 = System.currentTimeMillis();
    	
    	((MapListGraph) g).orderGraph();
    	
    	init_P();
    	
    	long t1 = System.currentTimeMillis();
    	
    	orderingTime = t1-t0;
	}
    
    @Override
    public List<Integer> init_P(){
    	if (orderedNodes == null){
    		orderedNodes = g.vertices();
        	Collections.sort(orderedNodes);
    	}
    	
    	return orderedNodes;
    }

    @Override
    public List<Integer> intersect (List<Integer> a, List<Integer> b){
    	return ListUtils.intersectOrdered(a, b);
    }
    
	@Override
	public void printStats(){
		System.out.println("Cliques: "+cliques);
		System.out.println("MaxSize: "+maxSize);
		System.out.println("Nodes: "+nodes);
		System.out.println("Time: "+(endTime-startTime)+" Ordering Time: "+orderingTime);
		System.out.println("Total: "+(orderingTime+endTime-startTime));
	}

}
