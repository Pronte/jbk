package bk.lists;


import java.util.*;
import java.io.*;

import lib.ListUtils;
import lib.graph.Graph;
import lib.graph.IntGraph;
/**
 * 
 * naive pivoting strategy,
 * just the first node in the P set.
 *  @deprecated (the algorithm is not exact. does not find all the cliques)
 */
public class BKPivotPlus extends BK1 {
	
	List<Integer> emptyList = new ArrayList<Integer>();

	public BKPivotPlus(IntGraph g){
		super(g); 
	}

    public BKPivotPlus(InputStream is) {
		super(is);
	}
    
    public BKPivotPlus(Graph graph, Set<Integer> visited){
    	super(graph,visited);
    }

    public BKPivotPlus (Graph graph, Set<Integer> toVisit, Set<Integer> visited){
    	super(graph,toVisit,visited);
    }
    
    void expand(List<Integer> C,List<Integer> P,List<Integer> X){
		if (timeLimit > 0 && System.currentTimeMillis() - startTime >= timeLimit) return;
		nodes++;
	
		if (P.isEmpty()){
			if(X.isEmpty()) {report(C);}
			return;
		}
		
		int u = getPivot(P, X);
		
		List<Integer> iterP = new ArrayList<Integer>(P.size());
		List<Integer> Pcompl = new ArrayList<Integer>(P.size()/2);
		
		boolean pivotInP = false;
		for(int i : P) //tweak: i should add the pivot to the set here, but i'm not, i'll add it later
		{ 
			if(i == u) {pivotInP = true;} //if the pivot was in P, i'll keep it and add it afterwards
			else if (!g.areNeighbors(i, u)){iterP.add(i);}
			else {Pcompl.add(i);}
			
		}
		
		if(!iterP.isEmpty())
		{
			int el;
			
			for(int ind = iterP.size() -1; ind >= 0; ind--)
			{
				
				el = iterP.get(ind);
				if(emptyIntersection(Pcompl,g.neighbors(el)))
				{
					iterP.remove(ind);
					Pcompl.add(el);
				}
			}		
		}
		
		//now add back the first pivot
		if(pivotInP){iterP.add(u);}
		
		for(int v : iterP){
		    	process(v,C,P,X);
		}
    }
    
    private boolean emptyIntersection(List<Integer> a, List<Integer> b)
    {
    	if(a.isEmpty() || b.isEmpty()) return true;
    	
    	for(int x : a)
    	{
    		if (b.contains(x)) return false;
    	}
    	
    	return true;
    }
    

    //in BK1 param "i" indicates the index of the node in P. Here is the actual label of the node.
    void process (int i, List<Integer> C, List<Integer> P, List<Integer> X){
    	
    	P.remove((Integer)i); 
    	int v = i;
    	
	    C.add(v);

	    List<Integer> newP = intersect(P,g.neighbors(v));
	    List<Integer> newX = intersect(X,g.neighbors(v));	    
	    
	    
	    expand(C,newP,newX);
	    
	    C.remove((Integer)v);
	    
	    X.add(v);
    }

    

    /**
     *Pivoting strategy.
     *Gets the first node in P.
     */
    protected int getPivot(List<Integer> P, List<Integer> X){
    	return P.get(0);
    }


}
