package bk.lists;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import lib.graph.Graph;
import lib.graph.IntGraph;
import lib.parser.Parser;
import bk.BK;

// template for Bron Kerbosch on an IntGraph
public abstract class BKIG extends BK {

    IntGraph g;		// graph
    ArrayList<List<Integer>> solution; // as it says

    
    public BKIG (Graph graph) {
		super(graph);
	    this.g = (IntGraph) graph;
	    if(DEBUG) System.out.println("[BKIG] Graph: n="+g.vertices().size()+", type="+g.getClass().getName());
		solution = new ArrayList<List<Integer>>();
    }

    public BKIG (InputStream is) {
		this(Parser.getIntGraph(is));
	}

    public BKIG (Graph graph, Set<Integer> visited){
    	this(graph);
    	borderVis = visited;
    }
    public BKIG (Graph graph, Set<Integer> toVisit, Set<Integer> visited){
    	this(graph);
    	borderVis = visited;
    	kernel = toVisit;
    }

	public void start(){
		
		expand0(init_empty(),init_P(),init_empty());
	}
	
	List<Integer> init_P(){
		
		return new ArrayList<Integer>(g.vertices());

	}

	abstract void expand0(List<Integer> C, List<Integer> P, List<Integer> X);
	abstract void expand(List<Integer> C,List<Integer> P,List<Integer> X);
	
	void report (List<Integer> C){ //TODO
		cliques++;
		
//		if(cliques%10000 == 0){ System.out.println("Cliques: "+cliques);}
		
		List<Integer> cc = new ArrayList<Integer>(C);
		
//		Collections.sort(cc);
				
		if(storeSolution) {solution.add(cc);}
		
		if(C.size() > maxSize) {maxSize = C.size();}
    }	

    public List<Integer> init_empty (){
    	return new ArrayList<Integer>();
    }
    
	public Object solution(){
		return solution;
	}

}
