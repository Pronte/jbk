package bk.lists;


import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lib.graph.Graph;
/**
 * 
 * naive pivoting strategy,
 * just the first node in the P set.
 *
 */
public class BKPivot extends BK1 {

	public BKPivot(Graph g){
		super(g); 
	}

    public BKPivot(InputStream is) {
		super(is);
	}
    
    public BKPivot(Graph graph, Set<Integer> visited){
    	super(graph,visited);
    }

    public BKPivot (Graph graph, Set<Integer> toVisit, Set<Integer> visited){
    	super(graph,toVisit,visited);
    }
    
    void expand(List<Integer> C,List<Integer> P,List<Integer> X){
		if (timeLimit > 0 && System.currentTimeMillis() - startTime >= timeLimit) return;
		nodes++;
	
		if (P.isEmpty()){
			if(X.isEmpty()) {report(C);}
			return;
		}
		
		int u = getPivot(P, X);
		
//		Set<Integer> iterP = new HashSet<Integer>(P.size());
//		
//		for(int i : P){ 
//			if(!g.areNeighbors(i, u)) {iterP.add(i);}
//		}
		
		int i = P.size()-1;
		while (i >= 0){
			if(!g.areNeighbors(P.get(i), u))
		    {
				process(i,C,P,X);
		    }
			
			i--;
		}
    }
    

    //in BK1 param "i" indicates the index of the node in P. Here is the actual label of the node.
    void process (int i, List<Integer> C, List<Integer> P, List<Integer> X){
    	
    	
    	int v = P.remove(i); 
    	
	    C.add(v);

	    List<Integer> newP = intersect(P,g.neighbors(v));
	    List<Integer> newX = intersect(X,g.neighbors(v));	    
	    
	    
	    expand(C,newP,newX);
	    
	    C.remove((Integer)v);
	    
	    X.add(v);
    }

    

    /**
     *Pivoting strategy.
     *Gets the first node in P.
     */
    protected int getPivot(List<Integer> P, List<Integer> X){
    	return P.get(0);
    }


}
