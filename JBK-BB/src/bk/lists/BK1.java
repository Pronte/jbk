package bk.lists;


import java.util.*;
import java.io.*;

import lib.ListUtils;
import lib.graph.Graph;
import lib.graph.IntGraph;
import lib.graph.MapListGraph;

public class BK1 extends BKIG {

	public BK1(Graph g){
		super(g); 
	}

    public BK1(InputStream is) {
		super(is);
	}

    public BK1(Graph graph, Set<Integer> visited){
    	super(graph,visited);
    }
    
    public BK1 (Graph graph, Set<Integer> toVisit, Set<Integer> visited){
    	super(graph,toVisit,visited);
    }

    void expand0(List<Integer> C,List<Integer> P,List<Integer> X){
		
		if(kernel == null && borderVis == null)
		{
			expand(C, P, X);
		}
		else
		{
			
//			System.out.println(g.vertices());
			
//			System.out.println(((MapListGraph)g).g.keySet());
			
			X.addAll(borderVis);
			P.removeAll(borderVis);
			

			System.out.println("Filters applied. |P|="+P.size()+" |K|="+kernel.size()+" |BV|="+borderVis.size());
			
			System.out.println(kernel);
			
			int ind = 0;
			while(ind < P.size()) //until there are no nodes to visit in P
			{
				if(kernel.contains(P.get(ind))){ //iterating over kernel nodes;
					process(ind,C,P,X);
				}
				else
					ind++;
			}
		}
    }
    
    void expand(List<Integer> C,List<Integer> P,List<Integer> X){
		if (timeLimit > 0 && System.currentTimeMillis() - startTime >= timeLimit) return;
		nodes++;
	
		if (P.isEmpty()){
			if(X.isEmpty()) {report(C);}
			return;
		}
		    
		while(!P.isEmpty()){
			process(P.size()-1,C,P,X);
		}
    }
    
    /**
     * @param ind : index of node to process
     */
    void process (int ind, List<Integer> C, List<Integer> P, List<Integer> X){
    	int v = P.remove(ind);
	    C.add(v);
//	    List<Integer> newP = new ArrayList<Integer>();
//	    List<Integer> newX = new ArrayList<Integer>();
//	    for (int w : P) if (g.areNeighbors(v,w)) newP.add(w);
//	    for (int w : X) if (g.areNeighbors(v,w)) newX.add(w);

// 		optimizing for adjacency lists:

	    List<Integer> newP = intersect(P,g.neighbors(v));
	    List<Integer> newX = intersect(X,g.neighbors(v));	    
	    
	    
	    expand(C,newP,newX);
	    
	    C.remove((Integer)v);
	    
	    X.add(v);
    }

    protected List<Integer> intersect (List<Integer> a, List<Integer> b){
    	return ListUtils.intersect(a, b);
    }


}
