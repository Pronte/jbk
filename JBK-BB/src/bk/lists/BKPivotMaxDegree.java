package bk.lists;


import java.util.*;
import java.io.*;

import lib.ListUtils;
import lib.graph.IntGraph;
/**
 * 
 * naive pivoting strategy,
 * just the first node in the P set.
 *
 */
public class BKPivotMaxDegree extends BKPivot {

//	//ordering of the nodes by decreasing degree
//	protected int[] degreeOrdering;
//	protected Map<Integer,Integer> degOrdLookUpTable;
	
	public BKPivotMaxDegree(IntGraph g){
		super(g); 
	}

    public BKPivotMaxDegree(InputStream is) {
		super(is);
	}

    
	/**
     *Pivoting strategy.
     *Gets the node in P U X with the highest degree.
     *
     *PIVOT CALC. COMPLEXITY: O(P+X)
     */
    protected int getPivot(List<Integer> P, List<Integer> X){
    	int maxNode = P.get(0);
    	int maxDeg = g.neighbors(maxNode).size();
    	
    	for(int n : P){
    		if(g.neighbors(n).size() > maxDeg){
    			maxNode = n;
    			maxDeg = g.neighbors(n).size();
    		}
    	}
//    	for(int n : X){
//    		if(g.neighbors(n).size() > maxDeg){
//    			maxNode = n;
//    			maxDeg = g.neighbors(n).size();
//    		}
//    	}
    	
    	
    	return maxNode;
    }


}
