package bk.lists;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import lib.graph.IntGraph;

public class BK1raw extends BKIG {

	public BK1raw(IntGraph g){
		super(g); 
	}

    public BK1raw(InputStream is) {
		super(is);
	}
    

    void expand0(List<Integer> C,List<Integer> P,List<Integer> X){
		
		if(kernel == null || borderVis == null)
		{
			expand(C, P, X);
		}
		else
		{
			System.out.println("Filters applied. |P|="+P.size()+" |K|="+kernel.size()+" |BV|="+borderVis.size());
			
			for(int i : borderVis)
			{
				X.add(i);
				P.remove(i);
			}
			
			for(int i : kernel) //iterating over kernel nodes;
			{
				process(i,C,P,X);
			}
		}
    }

    void expand(List<Integer> C,List<Integer> P,List<Integer> X){
		if (timeLimit > 0 && System.currentTimeMillis() - startTime >= timeLimit) return;
		nodes++;
	
		if (P.isEmpty()){
			if(X.isEmpty()) {report(C);}
			return;
		}
		    
		while(!P.isEmpty()){
			process(0,C,P,X);
		}
    }
    
    void process (int i, List<Integer> C, List<Integer> P, List<Integer> X){
    	int v = P.remove(i);
    	
	    C.add(v);
	    List<Integer> newP = new ArrayList<Integer>();
	    List<Integer> newX = new ArrayList<Integer>();
	    for (int w : P) if (g.areNeighbors(v,w)) newP.add(w);
	    for (int w : X) if (g.areNeighbors(v,w)) newX.add(w);    
	    
	    
	    expand(C,newP,newX);
	    
	    C.remove((Integer)v);
	    
	    X.add(v);
    }


}
