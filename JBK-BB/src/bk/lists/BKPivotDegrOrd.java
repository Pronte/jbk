package bk.lists;


import java.util.*;
import java.io.*;

import lib.ListUtils;
import lib.graph.IntGraph;
/**
 * 
 * naive pivoting strategy,
 * just the first node in the P set.
 *
 */
public class BKPivotDegrOrd extends BKPivot {

	//ordering of the nodes by decreasing degree
	protected int[] degreeOrdering;
	protected Map<Integer,Integer> degOrdLookUpTable;
	
	public BKPivotDegrOrd(IntGraph g){
		super(g); 
	}

    public BKPivotDegrOrd(InputStream is) {
		super(is);
	}


    @Override
    public List<Integer> init_P(){
    	List<Integer> nodes = g.vertices();
    	
    	Collections.sort(nodes, new Comparator<Integer>(){ //max degree comparator: an edge with higher degree
    								@Override				// is "smaller" than one with a lower degree
    								public int compare(Integer o1, Integer o2) {
    									return g.neighbors(o2).size() - g.neighbors(o1).size();
    								}
    							});
    	
    	return nodes;
    }
    
    

    
	/**
     *Pivoting strategy.
     *Gets the node in P U X with the highest degree.
     *
     *PIVOT CALC. COMPLEXITY: O(1)
     */
    protected int getPivot(List<Integer> P, List<Integer> X){
    	return P.get(0);
    }


}
