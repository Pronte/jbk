package bk.sparse;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import lib.graph.Graph;
import lib.parser.Parser;

public class TomitaXPivot extends BK1 {

	public TomitaXPivot(Graph graph) {
		super(graph);
	}
	
	public TomitaXPivot(InputStream is){
		this(Parser.getSparseGraph(is));
	}

	@Override
	protected void expand() {
		if (timeLimit > 0 && System.currentTimeMillis() - startTime >= timeLimit) return;
		nodes++;
		
		if(g.P_empty()){
			if(g.X_empty()) report();
			return;
		}
		
		List<Integer> iterP = new ArrayList<Integer>(g.P_size());
		
		int u = getPivot(R);
		
		int Pstart = g.P_start();
		int Pend = g.P_start() + g.P_size();
		
		for(int i = Pstart; i<Pend; i++){
			int vertex = g.getVertices()[i];
			
			if(!g.areNeighbors(vertex, u)) iterP.add(vertex);
		}
		
		for(int v : iterP){
			
			process(v);
		
		}
		for (int i : iterP){
			g.X2P(i);
		}
	}
	
	protected int getPivot(List<Integer> r){
		return getTomitaPivot(r);
	}

    /**
     *Tomita's Pivoting strategy.
     *The pivot is chosen as the node in P U N
     *with the highest number of neighbors in P.
     */
    protected int getTomitaPivot(List<Integer> r){
    	int best = (g.X_empty()? g.firstInP() : g.getVertices()[g.P_start() - g.X_size()]); //if X is not empty, we take the first element of X 
    	int bestVal = neighborsInP(best);

    	int node;
    	int val;

//    	int Pstart = g.P_start();
//    	int Pend = g.P_start()+g.P_size();
    	
    	//iterating over P
//    	for(int i = Pstart; i<Pend; i++){
//    		node = g.getVertices()[i];
//    		val = neighborsInP(node);
//
//    		if(val>bestVal){
//    			best = node;
//    			bestVal = val;
//    		}
//    	}

    	//If X is not empty, extend the search on X
    	if(!g.X_empty()){
    		int Xstart = g.P_start() - g.X_size();
    		int Xend = g.P_start();
    		for(int i = Xstart +1 ; i<Xend; i++){   //+1 since we already considered the first element
    			node = g.getVertices()[i];
    			
    			val = neighborsInP(node);

    			if(val>bestVal){
    				best = node;
    				bestVal = val;
    			}
    		}
    	}

    	return best;
    }

    protected int neighborsInP(int node){ //exploiting constant time of isInP(int), complexity O(d(node))
    	int neighs = 0;
    	
    	for(int i : g.neighbors(node)){
    		if(g.isInP(i)) neighs++;
    	}
    	return neighs;
    }

    
    protected int neighborsInPAlt(int node){ //old strategy, complexity O(|P|*d(node))
    	int neighNum = 0;
    	int Psize = g.P_size();
    	int Pstart = g.P_start();
    	
    	for(int i = Pstart; i<Pstart+Psize; i++){
    		int n = g.getVertices()[i];
    		
    		if(g.areNeighbors(n, node)) neighNum++;
    	}
    	return neighNum;
    }
	
	
	


}
