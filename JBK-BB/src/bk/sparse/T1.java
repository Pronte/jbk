package bk.sparse;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import lib.graph.Graph;
import lib.parser.Parser;

public class T1 extends Tomita {

	public T1(Graph graph) {
		super(graph);
	}
	
	public T1(InputStream is){
		this(Parser.getSparseGraph(is));
	}

	//true if X must be taken into account when chosing the pivot.
	private static boolean usingX = false;

    /**
     *Light pivoting strategy:
     *The pivot is chosen as the node in P with the highest degree
     * complexity : O(|P|)
     */
	@Override
    protected int getPivot(List<Integer> r){
    	int best = g.firstInP();
    	int bestVal = g.cardinality(best);

    	int node;
    	int val;

    	int Pstart = g.P_start();
    	int Pend = g.P_start()+g.P_size();
    	
    	//iterating over P
    	for(int i = Pstart; i<Pend; i++){
    		node = g.getVertices()[i];
    		val = g.cardinality(node);

    		if(val>bestVal){
    			best = node;
    			bestVal = val;
    		}
    	}

    	//If X is not empty, extend the search on X
    	if(usingX && !g.X_empty()){
    		int Xstart = g.P_start() - g.X_size();
    		int Xend = g.P_start();
    		for(int i = Xstart; i<Xend; i++){
    			node = g.getVertices()[i];
    			
    			val = g.cardinality(node);

    			if(val>bestVal){
    				best = node;
    				bestVal = val;
    			}
    		}
    	}

    	return best;
    }

}
