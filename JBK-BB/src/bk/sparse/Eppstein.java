package bk.sparse;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lib.DegeneracyUtils;
import lib.graph.Graph;
import lib.graph.SparseBKGraph;
import lib.parser.Parser;

public class Eppstein extends Tomita {

	private int maxdegree;
	
	public Eppstein(Graph graph) {
		super(graph);
	}
	
	public Eppstein(InputStream is){
		this(Parser.getSparseGraph(is));
	}


	@Override
	public void start() {
		init_R();
		
		int[] ordered = DegeneracyUtils.degeneracyOrdering(g);
		int[] position = new int[g.getMaxLabel()+1];
		
		//kernel nodes must come first in the ordering
    	//borderVis nodes must be removed (i.e. put first in the ordering)
    	//and normal border must be after all kernels
    	//but we want to preserve the relative ordering of kernel nodes
    	//i.e. |-vvvvvvvvkkkkkkkkkbbbbbbb-|
    	if(kernel != null || borderVis != null)
    	{
    		List<Integer> tx = new ArrayList<Integer>(),tp = new ArrayList<Integer>(),tb = new ArrayList<Integer>();
    		
    		for(int i : ordered)
    		{
    			if(borderVis.contains(i)) tx.add(i);
    			else if (kernel.contains(i)) tp.add(i);
    			else tb.add(i);
    		}
    		
    		for(int i = 0; i < ordered.length; i++)
    		{
    			if(i<borderVis.size()) ordered[i] = tx.get(i);
    			else if (i < borderVis.size()+kernel.size()) ordered[i] = tp.get(i-borderVis.size());
    			else ordered[i] = tb.get(i-borderVis.size()-kernel.size());
    		}
    		g.setXPBorder(borderVis.size());
    	}
		
		
		for(int i = 0; i<ordered.length; i++){
			int label = ordered[i];
			position[label] = i;
		}
		
		
		for(int i = 0; i < ordered.length; i++){
			
			if(kernel != null && !kernel.contains(i)){ System.out.println("SKIPPING "+i); continue;}
			
			clear_R();
			
			copyTo(ordered,g.getVertices()); //resetting the order, adds a total of O(n^2) steps to the alogrithm
			copyTo(position,g.getPositions());
			
			g.setXstart(0);
			g.setXPBorder(i);
			g.setPend(g.getN()); //exclusive
			
			g.addingToR(g.getVertices()[i]);
			
			expand();
			
			g.backTrack();
			
		}
		
		
	}

	private void copyTo(int[] ordered, int[] vertices) {
		for(int i = 0; i < ordered.length ; i++){
			vertices[i] = ordered[i];
		}
	}

	private void clear_R() {
		R.clear();
	}

	/*
	public int[] degenOrdering(SparseBKGraph g) {
		
		int[] vertices = g.getVertices();
		
		List<Integer> r = new ArrayList<Integer>(vertices.length);
		
		
		//n� of neighbors -> nodes with such number
		Map<Integer,List<Integer>> neighbors = new HashMap<Integer, List<Integer>>();

		maxdegree = 0;		
		
		for(int i=0; i<vertices.length; i++){
			int nd = vertices[i];
			//cheching how many neighbors the node has
			int num = g.neighbors(nd).length;
			
			if(num>maxdegree){maxdegree = num;}
			
			if(neighbors.get(num) == null){
				neighbors.put(num, new ArrayList<Integer>());
			}
			neighbors.get(num).add(nd);
		}
		if(neighbors.isEmpty()){
			System.out.println("error during degeneracy ordering computation or empty graph");
			System.exit(1);
		}
		
		System.out.println("maxdegree: "+maxdegree);
		
		degOrdRec(r,neighbors);
		
		int[] rArr = new int[vertices.length];
		
		for(int i=0; i<vertices.length; i++){
			rArr[i] = r.get(i);
		}
		
		return rArr;
	}


	//at each step removes the node with the lowest number of neighbors not in r
	private void degOrdRec(List<Integer> r, Map<Integer,List<Integer>> neighbors) {

		
		boolean found = false;
		int i = 0;
		int node = -1;
		while(!found){
			if(i>maxdegree){
				return;
			}
			
			if (neighbors.containsKey(i) && !neighbors.get(i).isEmpty()){
				node = neighbors.get(i).remove(0);
				
				r.add(node);
				
				found = true;
			} else {
				i++;
			}
		}
		
		List<Integer> updating = new ArrayList<Integer>();
		//updating neighbors
		for(i = 0; i<maxdegree; i++){
		  
		  
		  if(neighbors.containsKey(i) && !neighbors.get(i).isEmpty()){
		    for(int nb : neighbors.get(i)){
		      if(areNeighbors(node,nb)){
		    	  updating.add(nb);
		      }
		    }
		    if(!updating.isEmpty()){
		    	neighbors.get(i).removeAll(updating);
		    	
		    	if(i!=0){
			    	if(neighbors.get(i-1) == null){
			    		neighbors.put(i-1, new ArrayList<Integer>());
			    	}
			    	neighbors.get(i-1).addAll(updating);
		    	}
		    	updating.clear();
		    }
		  }
		  
		}
		if(neighbors.get(maxdegree) == null || neighbors.get(maxdegree).isEmpty()){
			maxdegree--;
			System.out.println("maxdegree: "+maxdegree);
		}
		
		degOrdRec(r,neighbors);
		
	}
	
	private boolean areNeighbors (int a, int b){
		
		return g.areNeighbors(a, b);
	}
	*/
	
}
