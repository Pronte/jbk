package bk.sparse;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sun.org.apache.bcel.internal.util.BCELifier;

import lib.graph.Graph;
import lib.graph.SparseBKGraph;
import lib.parser.Parser;
import bk.BK;

public class BK1 extends BK {
	SparseBKGraph g;
	List<Integer> R;
	
	List<Integer> startingP = null;
	List<Integer> excludedVertices = null;
	boolean useStartingP = false;
	
	private List<List<Integer>> solution;

	public BK1(Graph graph) {
		super(graph);
		this.g = (SparseBKGraph) graph;
		solution = new ArrayList<List<Integer>>();
	}
	
	public BK1(InputStream is){
		this(Parser.getSparseGraph(is));
	}

	public BK1(InputStream is, List<Integer> toVisit, List<Integer> visited){
		this(is);
		startingP = toVisit;
		excludedVertices = visited;
	}
	
	@Override			  //                               excluded         toVisit &not toVisit             
	public void start() { // [----------------------XXXXXXXXXXXXXXXXXXXXXXPPPPPPPPPPPPPPPPPPPPPPPP---------------------]
		init_R();
		orderGraph();
//		init_P();
//		init_X();
		expand0();
	}


	protected void expand0() {
		
		if(kernel == null && borderVis == null)
		{
			expand();
		}
		else
		{
			nodes++;
			
			for(int x : borderVis)
			{
				g.P2X(x);
			}
//			List<Integer> Pback = new ArrayList<Integer>(g.P_size());
			
			for(int k : kernel){
//				Pback.add(k);
				process(k);

			}
			
			//unnecessary for root node
//			for (int i : Pback){
//				g.X2P(i);
//			}
		}
	}
	
	
	protected void expand() {
		if (timeLimit > 0 && System.currentTimeMillis() - startTime >= timeLimit) return;
		nodes++;
		
		if(g.P_empty()){
			if(g.X_empty()) report();
			return;
		}
		
		List<Integer> Pback = new ArrayList<Integer>(g.P_size());
		
		while(!g.P_empty()){
			
			int v = g.firstInP();
			Pback.add(v);
			
			process(v);
		
		}
		for (int i : Pback){
			g.X2P(i);
		}
	}
	
	private int indent = 0;
	
	protected void process(int v){

		R.add(v);
		g.addingToR(v);
		
		expand();
		
		R.remove((Integer)v);
		g.backTrack(); //restore P and X
		
		g.P2X(v);
		
	}

	protected void init_R() {
		R = new ArrayList<Integer>();
	}

	protected void init_P() {
		if(startingP != null && !startingP.isEmpty()){
			useStartingP = true;
		}
	}

	protected void init_X() {
		if(excludedVertices != null && !excludedVertices.isEmpty()){
			for(int x : excludedVertices){
				if(g.isInP(x)){
					g.P2X(x);
				}
			}
		}
	}
	protected void report() {
		cliques++;
		
		if(cliques%10000 == 0) System.out.println("cliques: "+cliques+" , clq: "+R);
		
		if(maxSize < R.size()) maxSize = R.size();
		if(storeSolution) solution.add(new ArrayList<Integer>(R));
	}

	
	public Object solution(){
		return solution;
	}
	
	private void orderGraph()
	{
		for(int i = 0; i < g.getNeighbors().length; i++)
		{
			if(g.getNeighbors()[i] != null)
				Arrays.sort(g.getNeighbors()[i]);
		}
	}

}
