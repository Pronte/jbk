package bk.bitsets;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import lib.graph.Graph;
import lib.graph.bs.BSIntGraph;
import lib.parser.Parser;
import bk.BK;

public abstract class BKBS extends BK {
    BSIntGraph g;	// graph
    List<BitSet> solution; // solution

    public BKBS (Graph graph) {
    	super(graph);
		
    	if(DEBUG) System.out.println("Building alg..");
		if(DEBUG) System.out.println("Graph null? "+(graph==null));
    	
		this.g = (BSIntGraph) graph;
    }

	public BKBS (InputStream is) {
		this(Parser.getBitSetIntGraph(is));
    }

	@Override
	public void start(){
		expand0(new BitSet(),init_P(),new BitSet());	
    }

    public BitSet init_P (){
    	if(DEBUG) System.out.println("g: "+g);
    	if(DEBUG) System.out.println("g.nodes(): "+g.nodes());
    	
    	return (BitSet) g.nodes().clone();
    }

	abstract void expand0(BitSet C,BitSet P, BitSet X);
	abstract void expand(BitSet C,BitSet P, BitSet X);
	
	void report(BitSet C){
    	cliques++;
    	if(solution == null) {
    		solution = new ArrayList<BitSet>();
    	}

    	if(storeSolution){solution.add((BitSet) C.clone());}

    	if(C.cardinality() > maxSize) {maxSize = C.cardinality();}
        }
	
	public Object solution(){
		return solution;
	}
}
