package bk.bitsets;

import java.io.InputStream;
import java.util.BitSet;
import java.util.List;

import lib.graph.Graph;
import lib.graph.bs.BSIntGraph;
import lib.parser.DIMACSParser;

public class TomitaBSAlt extends BK1BS {


	public TomitaBSAlt(Graph graph) {
		super(graph);
	}

	public TomitaBSAlt(InputStream is){
		super(is);
	}

	void expand(BitSet C,BitSet P, BitSet X){
		if (timeLimit > 0 && System.currentTimeMillis() - startTime >= timeLimit) return;
		nodes++;
	
		if(P.isEmpty()){
			if(X.isEmpty()) {report(C);}
			return;
		}
	
		int u = getTomitaPivot(P, X); //neighbours of the pivot are not expanded at this step 
		
		int i = 0;
		for(i=P.nextSetBit(0); i>=0; i=P.nextSetBit(i+1)){ //iterating over P's true values
			if(!g.areNeighbors(i, u)){ //cutting the pivot's neighbours branches
				process (i,C,P,X);
			}
		}
    }

	

    /**
     *Tomita's Pivoting strategy.
     *The pivot is chosen as the node in P U N
     *with the highest number of neighbors in P.
     */
    protected int getTomitaPivot(BitSet P, BitSet X){
    	
    	
    	int best = -1;
    	int bestVal = -1;

    	int val;

    	 for (int node = P.nextSetBit(0); node >= 0; node = P.nextSetBit(node+1))
    	 {//iterating over P's true values
    		val = neighborsIn(P,node);

    		if(val>bestVal){
    			best = node;
    			bestVal = val;
    		}
    	 }

    	if(!X.isEmpty())
    	{
	       	 for (int node = X.nextSetBit(0); node >= 0; node = X.nextSetBit(node+1))
	       	 {//iterating over P's true values
	       		val = neighborsIn(P,node);
	
	       		if(val>bestVal)
	       		{
	       			best = node;
	       			bestVal = val;
	       		}
	       	 }    	
       	 }
    	return best;
    }

    protected int neighborsIn(BitSet P, int node){
    	
    	BitSet neighs = (BitSet) g.neighbors(node).clone();
    	
    	neighs.and(P);
    	
    	return neighs.cardinality();
    }
    
    //alternative strategy for the computation of the number of neighbors
    protected int neighborsInALT(BitSet P, int node){
    	int neighNum = 0;
    	
    	for(int n = P.nextSetBit(0); n>=0 ; n = P.nextSetBit(n+1)){//iterating over P's true values
    		if(g.areNeighbors(n, node)) neighNum++;
    	}
    	return neighNum;
    }

}
