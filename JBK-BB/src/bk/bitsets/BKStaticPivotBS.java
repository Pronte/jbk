package bk.bitsets;

import java.io.InputStream;
import java.util.BitSet;
import java.util.List;

import lib.graph.Graph;
import lib.graph.bs.BSIntGraph;
import lib.parser.DIMACSParser;

public class BKStaticPivotBS extends BK1BS {


	public BKStaticPivotBS(Graph graph) {
		super(graph);
	}

	public BKStaticPivotBS(InputStream is){
		super(is);
	}

	void expand(BitSet C,BitSet P, BitSet X){
		if (timeLimit > 0 && System.currentTimeMillis() - startTime >= timeLimit) return;
		nodes++;
	
		if(P.isEmpty()){
			if(X.isEmpty()) {report(C);}
			return;
		}
	
		int u = getPivot(P, X); //neighbours of the pivot are not expanded at this step 
		
		int i = 0;
		for(i=P.nextSetBit(0); i>=0; i=P.nextSetBit(i+1)){ //iterating over P's true values
			if(!g.areNeighbors(i, u)){ //cutting the pivot's neighbours branches
				process (i,C,P,X);
			}
		}
    }

	

    /**
     *Static Pivoting strategy.
     *The pivot is chosen as the node in P U N with the max degree
     */
    protected int getPivot(BitSet P, BitSet X){
    	
    	BitSet PUX = P;
    	
    	if(!X.isEmpty()){
    		PUX = (BitSet) P.clone();
    		PUX.or(X);
    	}
    	
    	int best = -1;
    	int bestVal = -1;

    	int val;

    	 for (int node = PUX.nextSetBit(0); node >= 0; node = PUX.nextSetBit(node+1)) {//iterating over PUX's true values
    		val = g.neighbors(node).cardinality();

    		if(val>bestVal){
    			best = node;
    			bestVal = val;
    		}
    	}

    	return best;
    }

}
