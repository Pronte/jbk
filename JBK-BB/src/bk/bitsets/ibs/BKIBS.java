package bk.bitsets.ibs;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import lib.graph.Graph;
import lib.graph.bs.BSIntGraph;
import lib.graph.bs.IBSIntGraph;
import lib.graph.bs.IBitSet;
import lib.parser.Parser;
import bk.BK;

/**
 * IBS: Improved BitSets
 * 
 * BitSets with a stored index for the first set element.
 * Proved to perform worse than the plain BitSet 
 *
 */
public abstract class BKIBS extends BK {
    IBSIntGraph g;	// graph
    List<IBitSet> solution; // solution

    public BKIBS (Graph graph) {
    	super(graph);
		
    	if(DEBUG) System.out.println("Building alg..");
		if(DEBUG) System.out.println("Graph null? "+(graph==null));
    	
		this.g = (IBSIntGraph) graph;
    }

	public BKIBS (InputStream is) {
		this(Parser.getIBitSetIntGraph(is));
    }

	@Override
	public void start(){
		expand0(new IBitSet(),init_P(),new IBitSet());	
    }

    public IBitSet init_P (){
    	if(DEBUG) System.out.println("g: "+g);
    	if(DEBUG) System.out.println("g.nodes(): "+g.nodes());
    	
    	return (IBitSet) g.nodes().clone();
    }

	abstract void expand0(IBitSet C,IBitSet P, IBitSet X);
	abstract void expand(IBitSet C,IBitSet P, IBitSet X);
	
	void report(IBitSet C){
    	cliques++;
    	if(solution == null) {
    		solution = new ArrayList<IBitSet>();
    	}

    	if(storeSolution){solution.add((IBitSet) C.clone());}

    	if(C.cardinality() > maxSize) {maxSize = C.cardinality();}
        }
	
	public Object solution(){
		return solution;
	}
}
