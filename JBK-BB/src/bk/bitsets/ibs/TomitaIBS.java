package bk.bitsets.ibs;

import java.io.InputStream;
import java.util.BitSet;
import java.util.List;

import lib.graph.Graph;
import lib.graph.bs.BSIntGraph;
import lib.graph.bs.IBitSet;
import lib.parser.DIMACSParser;

public class TomitaIBS extends BK1IBS {


	public TomitaIBS(Graph graph) {
		super(graph);
	}

	public TomitaIBS(InputStream is){
		super(is);
	}

	void expand(IBitSet C,IBitSet P, IBitSet X){
		if (timeLimit > 0 && System.currentTimeMillis() - startTime >= timeLimit) return;
		nodes++;
	
		if(P.isEmpty()){
			if(X.isEmpty()) {report(C);}
			return;
		}
	
		int u = getTomitaPivot(P, X); //neighbours of the pivot are not expanded at this step 
		
		int i = 0;
		for(i=P.firstSetBit(); i>=0; i=P.nextSetBit(i+1)){ //iterating over P's true values
			if(!g.areNeighbors(i, u)){ //cutting the pivot's neighbours branches
				process (i,C,P,X);
			}
		}
    }

	

    /**
     *Tomita's Pivoting strategy.
     *The pivot is chosen as the node in P U N
     *with the highest number of neighbours in P.
     */
    protected int getTomitaPivot(IBitSet P, IBitSet X){
    	
    	IBitSet PUX = P;
    	
    	if(!X.isEmpty()){
    		PUX = (IBitSet) P.clone();
    		PUX.or(X);
    	}
    	
    	int best = -1;
    	int bestVal = -1;

    	int val;

    	 for (int node = PUX.firstSetBit(); node >= 0; node = PUX.nextSetBit(node+1)) {//iterating over PUX's true values
    		val = neighborsIn(P,node);

    		if(val>bestVal){
    			best = node;
    			bestVal = val;
    		}
    	}

    	return best;
    }

    protected int neighborsIn(IBitSet P, int node){
    	
    	IBitSet neighs = (IBitSet) g.neighbors(node).clone();
    	
    	neighs.and(P);
    	
    	return neighs.cardinality();
    }
    
    //alternative strategy for the computation of the number of neighbors
    protected int neighborsInALT(IBitSet P, int node){
    	int neighNum = 0;
    	
    	for(int n = P.firstSetBit(); n>=0 ; n = P.nextSetBit(n+1)){//iterating over P's true values
    		if(g.areNeighbors(n, node)) neighNum++;
    	}
    	return neighNum;
    }

}
