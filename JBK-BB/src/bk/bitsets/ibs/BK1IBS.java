package bk.bitsets.ibs;

import java.io.InputStream;
import java.util.BitSet;
import java.util.List;
import java.util.Set;

import lib.graph.Graph;
import lib.graph.bs.IBitSet;

public class BK1IBS extends BKIBS {

    public BK1IBS (Graph graph) {
    	super(graph);
    }

	public BK1IBS (InputStream is) { 
		super(is);
    }

	void expand0(IBitSet C,IBitSet P, IBitSet X){
		
		if(kernel == null || borderVis == null)
		{
			expand(C, P, X);
		}
		else
		{
			System.out.println("Filters applied. |P|="+P.cardinality()+" |K|="+kernel.size()+" |BV|="+borderVis.size());
			
			for(int i : borderVis)
			{
				X.set(i);
				P.clear(i);
			}
			
			for(int i : kernel) //iterating over kernel nodes;
			{
				process(i,C,P,X);
			}
		}
    }

	void expand(IBitSet C,IBitSet P, IBitSet X){
		if (timeLimit > 0 && System.currentTimeMillis() - startTime >= timeLimit) return;
		nodes++;
	
		if(P.isEmpty() && X.isEmpty()) {report(C); return;}

		for(int i=P.firstSetBit(); i>=0; i=P.nextSetBit(i+1)){//iterating over P's true values
			process(i,C,P,X);		
		}
    }
	
	void process(int i, IBitSet C,IBitSet P, IBitSet X){

		if(P.get(i)){ //redundant control
			
			//add c
			C.set(i);
			//intersect p x
			IBitSet newP = (IBitSet) P.clone();
			IBitSet newX = (IBitSet) X.clone();
			IBitSet neighbors = g.neighbors(i);

			newP.and(neighbors);
			newX.and(neighbors);
			//recur
			expand(C,newP,newX);

			//remove c p
			C.clear(i);
			P.clear(i);
			//add x
			X.set(i);
		}

	}

}
