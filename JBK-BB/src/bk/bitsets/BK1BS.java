package bk.bitsets;

import java.io.InputStream;
import java.util.BitSet;

import lib.graph.Graph;

public class BK1BS extends BKBS {

    public BK1BS (Graph graph) {
    	super(graph);
    }

	public BK1BS (InputStream is) {
		super(is);
    }



	void expand0(BitSet C,BitSet P, BitSet X){
		
		if(kernel == null || borderVis == null)
		{
			expand(C, P, X);
		}
		else
		{
			System.out.println("Filters applied. |P|="+P.cardinality()+" |K|="+kernel.size()+" |BV|="+borderVis.size());
			
			for(int i : borderVis)
			{
				X.set(i);
				P.clear(i);
			}
			
			for(int i : kernel) //iterating over kernel nodes;
			{
				process(i,C,P,X);
			}
		}
    }
	
	void expand(BitSet C,BitSet P, BitSet X){
		if (timeLimit > 0 && System.currentTimeMillis() - startTime >= timeLimit) return;
		nodes++;
	
		if(P.isEmpty() && X.isEmpty()) {report(C); return;}

		for(int i=P.nextSetBit(0); i>=0; i=P.nextSetBit(i+1)){//iterating over P's true values
			process(i,C,P,X);		
		}
    }
	
	void process(int i, BitSet C,BitSet P, BitSet X){

		if(P.get(i)){ //redundant control
			
			//add c
			C.set(i);
			//intersect p x
			BitSet newP = (BitSet) P.clone();
			BitSet newX = (BitSet) X.clone();
			BitSet neighbors = g.neighbors(i);

			newP.and(neighbors);
			newX.and(neighbors);
			//recur
			expand(C,newP,newX);

			//remove c p
			C.clear(i);
			P.clear(i);
			//add x
			X.set(i);
		}

	}


}
