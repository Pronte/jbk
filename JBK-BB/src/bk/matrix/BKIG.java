package bk.matrix;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import lib.ListUtils;
import lib.VertexCoverCalculator;
import lib.graph.Graph;
import lib.graph.MatrixGraph;
import lib.parser.Parser;
import bk.BK;

public abstract class BKIG extends BK {

    MatrixGraph g;		// graph
    ArrayList<List<Integer>> solution; // as it says
	
    List<Integer> excludedVertices = null; //vertices to be considered to be already visited (already in X)
    Set<Integer> requiredVertices = null; //vertices required
    List<Integer> startingP = null;
    
    int requiredNum = 0;
    int minSize = 0;
    
    public BKIG (Graph graph) {
		super(graph);
	    this.g = (MatrixGraph) graph;
	    if(DEBUG) System.out.println("[BKIG] Graph: n="+g.vertices().size()+", type="+g.getClass().getName());
		solution = new ArrayList<List<Integer>>();
    }

    public BKIG (InputStream is) {
		this(Parser.getMatrixGraph(is));
	}

    public BKIG (Graph graph, List<Integer> visited){
    	this(graph);
    	excludedVertices = visited;
    }
    public BKIG (Graph graph, List<Integer> toVisit, List<Integer> visited){
    	this(graph);
    	excludedVertices = visited;
    	startingP = toVisit;
    }
    
//    public void setFilters(List<Integer> toVisit, List<Integer> excluded, Set<Integer> required, int requiredNum, int minSize)
//    {
//    	startingP = toVisit;
//    	excludedVertices = excluded;
//    	requiredVertices = required;
//    	
//    	this.requiredNum = requiredNum;
//    	this.minSize = minSize;
//    }

	public void start(){
		expand0(init_empty(),init_P(),init_X());
	}
	
	List<Integer> init_P(){
		
		if(System.getProperty("VERTEXCOVER") != null)
		{
			startingP = new ArrayList<Integer> (VertexCoverCalculator.vertexCoverMG((MatrixGraph)g) );
			System.out.println("Vertex cover size: "+startingP.size());
		}
		
		if(startingP == null) startingP = new ArrayList<Integer>(g.vertices());
				
		if(!init_X().isEmpty()){
			ListUtils.removeFrom(startingP,init_X());
		}
		
		return startingP;
	}
	
	List<Integer> init_X(){
		if(excludedVertices == null){
			excludedVertices = new ArrayList<Integer>();
		}
		return excludedVertices;
	}

	abstract void expand0(List<Integer> C,List<Integer> P,List<Integer> X);
	abstract void expand(List<Integer> C,List<Integer> P,List<Integer> X);
	
	void report (List<Integer> C){
		cliques++;
				
		if(storeSolution)
		{
			List<Integer> cc = new ArrayList<Integer>(C);
//			Collections.sort(cc);
			solution.add(cc);
		}
		
		if(C.size() > maxSize) {maxSize = C.size();}
    }	

    public List<Integer> init_empty (){
    	return new ArrayList<Integer>();
    }
    
	public Object solution(){
		return solution;
	}

}