package bk.matrix;


import java.util.*;
import java.io.*;

import lib.ListUtils;
import lib.graph.Graph;
import lib.graph.IntGraph;

public class TomitaXPivot extends Tomita {

	public TomitaXPivot(Graph g){
		super(g); 
	}

    public TomitaXPivot(InputStream is) {
		super(is);
	}
    
    public TomitaXPivot(Graph graph, List<Integer> visited){
    	super(graph,visited);
    }

    public TomitaXPivot (Graph graph, List<Integer> toVisit, List<Integer> visited){
    	super(graph,toVisit,visited);
    }
    
    @Override
    /**
     *Tomita's Pivoting strategy modified.
     *The pivot is chosen as the node in N
     *with the highest number of neighbors in P.
     */
    protected int getTomitaPivot(List<Integer> P, List<Integer> X){
    	int best = (X.isEmpty()? P.get(0) : X.get(0));
    	int bestVal = neighborsIn(P,best);

    	int node;
    	int val;

//    	for(int i = 1; i<P.size(); i++){
//    		node = P.get(i);
//    		val = neighborsIn(P,node);
//
//    		if(val>bestVal){
//    			best = node;
//    			bestVal = val;
//    		}
//    	}
    	
    	//If X is not empty, extend the search on X
    	if(!X.isEmpty()){
    		for(int i = 1; i<X.size(); i++){ //.get(0) is already taken in the initialization
    			node = X.get(i);
    			val = neighborsIn(P,node);

    			if(val>bestVal){
    				best = node;
    				bestVal = val;
    			}
    		}
    	}
//    	if(fromX) pivotsInX++;
//    	else pivotsInP++;

    	return best;
    }
    
    @Override
    /**
     *Pivoting strategy.
     *Gets the best pivot according to Tomita's eval function which is also a required node.
     *returns -1 if not found.
     */
    protected int getTomitaRequiredPivot(List<Integer> P, List<Integer> X){
    	int best = -1;
    	int bestVal = -1;

    	int val;

//    	for(int i = 1; i<P.size(); i++){
//    		node = P.get(i);
//    		val = neighborsIn(P,node);
//
//    		if(val>bestVal){
//    			best = node;
//    			bestVal = val;
//    		}
//    	}
    	
    	//If X is not empty, extend the search on X
    	if(!X.isEmpty()){
    		for(int node : X){ //.get(0) is already taken in the initialization
    			if(requiredVertices.contains(node))
    			{
    				val = neighborsIn(P,node);
    				
    				if(val>bestVal){
    					best = node;
    					bestVal = val;
    				}
    			}
    		}
    	}
    	
    	if(best < 0) //if there were no required nodes in X, get the first required found in P
    	{
    		for(int node : P)
    		{
    			if(requiredVertices.contains(node))
    				{
    				best = node;
    				break;
    				}
    		}
    	}
    	
//    	if(fromX) pivotsInX++;
//    	else pivotsInP++;

    	return best;
    }

}
