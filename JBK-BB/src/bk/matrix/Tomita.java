package bk.matrix;


import java.util.*;
import java.io.*;

import lib.ListUtils;
import lib.graph.Graph;
import lib.graph.IntGraph;

public class Tomita extends BKPivot {

	public Tomita(Graph g){
		super(g); 
	}

    public Tomita(InputStream is) {
		super(is);
	}
    
    public Tomita(Graph graph, List<Integer> visited){
    	super(graph,visited);
    }

    @Override
    protected int getPivot(List<Integer> P, List<Integer> X){
    	return getTomitaPivot(P, X);
    }
    
    @Override
    protected int getRequiredPivot(List<Integer> P, List<Integer> X){
    	return getTomitaRequiredPivot(P,X);
    }

    public Tomita (Graph graph, List<Integer> toVisit, List<Integer> visited){
    	super(graph,toVisit,visited);
    }
    
    /**
     *Tomita's Pivoting strategy.
     *The pivot is chosen as the node in P U N
     *with the highest number of neighbors in P.
     */
    protected int getTomitaPivot(List<Integer> P, List<Integer> X){
    	int best = P.get(0);
    	int bestVal = neighborsIn(P,best);

    	int node;
    	int val;

    	for(int i = 1; i<P.size(); i++){
    		node = P.get(i);
    		val = neighborsIn(P,node);

    		if(val>bestVal){
    			best = node;
    			bestVal = val;
    		}
    	}
    	
    	//If X is not empty, extend the search on X
    	if(!X.isEmpty()){
    		for(int i = 0; i<X.size(); i++){
    			node = X.get(i);
    			val = neighborsIn(P,node);

    			if(val>=bestVal){
    				best = node;
    				bestVal = val;
    			}
    		}
    	}
    	return best;
    }

    /**
     *Pivoting strategy.
     *Gets the best pivot according to Tomita's eval function which is also a required node.
     *returns -1 if not found.
     */
    protected int getTomitaRequiredPivot(List<Integer> P, List<Integer> X){
    	
    	int best = -1;
    	int bestVal = -1;

    	int val;

    	for(int node : P){
    		if(requiredVertices.contains(node))
    		{
    			val = neighborsIn(P,node);
        		if(val>bestVal)
        		{
	    			best = node;
	    			bestVal = val;
	    		}
    		}
    	}
    	
    	//If X is not empty, extend the search on X
    	if(!X.isEmpty() && bestVal > -1){  //if bestVal is still -1 there are no required verts in P, so we can cut the branch here, even if there are required verts in X.
    		for(int node : X)
    		{
    			if(requiredVertices.contains(node))
    			{
    				val = neighborsIn(P,node);
	
	    			if(val>=bestVal)
	    			{
	    				best = node;
	    				bestVal = val;
	    			}
    			}
    		}
    	}
    	return best;
    }
    
    protected int neighborsIn(List<Integer> P, int node){
    	int neighNum = 0;
    	for(int n : P){
    		if(g.areNeighbors(n, node)) neighNum++;
    	}
    	return neighNum;
    }


}
