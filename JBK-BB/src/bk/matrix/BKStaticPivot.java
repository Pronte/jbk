package bk.matrix;


import java.util.*;
import java.io.*;

import lib.ListUtils;
import lib.graph.IntGraph;
/**
 * 
 * naive pivoting strategy,
 * just the first node in the P set.
 *
 */
public class BKStaticPivot extends BKPivot {

//	//ordering of the nodes by decreasing degree
//	protected int[] degreeOrdering;
//	protected Map<Integer,Integer> degOrdLookUpTable;
	
	public BKStaticPivot(IntGraph g){
		super(g); 
	}

    public BKStaticPivot(InputStream is) {
		super(is);
	}

    @Override
	/**
     *Pivoting strategy.
     *Gets the node in P U X with the highest degree.
     *
     *PIVOT CALC. COMPLEXITY: O(P+X)
     */
    protected int getPivot(List<Integer> P, List<Integer> X){
    	int maxNode = P.get(0);
    	int maxDeg = g.cardinality(maxNode);
    	
    	for(int n : P){
    		if(g.cardinality(n) > maxDeg){
    			maxNode = n;
    			maxDeg = g.cardinality(n);
    		}
    	}
    	for(int n : X){
    		if(g.cardinality(n) > maxDeg){
    			maxNode = n;
    			maxDeg = g.cardinality(n);
    		}
    	}
    	return maxNode;
    }
    
    @Override
    protected int getRequiredPivot(List<Integer> P, List<Integer> X){
    	
    	int maxNode = -1;
    	int maxDeg = -1;
    	
    	for(int n : P){
    		if(requiredVertices.contains(n) && g.cardinality(n) > maxDeg)
    		{
    			maxNode = n;
    			maxDeg = g.cardinality(n);
    		}
    	}
    	if(maxDeg > -1) //if maxDeg is still -1 there are no required nodes in P, so we can cut the branch here.
    	{
    		for(int n : X){
    			if(requiredVertices.contains(n) && g.cardinality(n) > maxDeg){
    				maxNode = n;
    				maxDeg = g.cardinality(n);
    			}
    		}
    	}
    	return maxNode;
    }

    
}
