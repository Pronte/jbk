package bk.matrix;


import java.util.*;
import java.io.*;

import lib.ListUtils;
import lib.graph.Graph;
import lib.graph.IntGraph;

public class BKLayeredPivot extends Tomita {

	private int depth = 0;
	private int tomita = 3;
	
	
	public BKLayeredPivot(IntGraph g){
		super(g); 
	}

    public BKLayeredPivot(InputStream is) {
		super(is);
	}
    
    public BKLayeredPivot(Graph graph, List<Integer> visited){
    	super(graph,visited);
    }
    public BKLayeredPivot (Graph graph, List<Integer> toVisit, List<Integer> visited){
    	super(graph,toVisit,visited);
    }

    @Override
    protected int getPivot(List<Integer> P, List<Integer> X){
    	if(true || depth > tomita)
    	{
    		return getTomitaPivot(P, X);
    	}
    	else
    	{
    		return getNaivePivot(P, X);
    	}
    }
    
    @Override
    protected int getRequiredPivot(List<Integer> P, List<Integer> X){
    	if(true || depth > tomita)
    	{
    		return getTomitaRequiredPivot(P,X);
    	}
    	else
    	{
    		return getNaiveRequiredPivot(P, X);
    	}
    }

    /**
     *Pivoting strategy.
     *Gets the first node in P.
     */
    protected int getNaivePivot(List<Integer> P, List<Integer> X){
    	return P.get(0);
    }

    /**
     *Pivoting strategy.
     *Gets the first node in P.
     *returns -1 if not found.
     */
    protected int getNaiveRequiredPivot(List<Integer> P, List<Integer> X){
    	
    	for(int i : P)
    	{
    		if(requiredVertices.contains(i)) return i;
    	}
    	
    	return -1;
    }


    @Override
    void expand(List<Integer> C, List<Integer> P, List<Integer> X) {
    	depth++;
//    	for(int i = 0; i < depth; i++)
//    	{
//    		System.out.print("-");
//    	}
//    	System.out.println();
    	super.expand(C, P, X);
    	depth--;
    }

}
