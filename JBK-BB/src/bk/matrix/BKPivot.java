package bk.matrix;


import java.util.*;
import java.io.*;

import lib.ListUtils;
import lib.graph.Graph;
import lib.graph.IntGraph;
/**
 * 
 * naive pivoting strategy,
 * just the first node in the P set.
 *
 */
public class BKPivot extends BK1 {
//	public int pivotsInP = 0;
//	public int pivotsInX = 0;

	public BKPivot(Graph g){
		super(g); 
	}

    public BKPivot(InputStream is) {
		super(is);
	}
    
    public BKPivot(Graph graph, List<Integer> visited){
    	super(graph,visited);
    }

    public BKPivot (Graph graph, List<Integer> toVisit, List<Integer> visited){
    	super(graph,toVisit,visited);
    }
    
    void expand(List<Integer> C,List<Integer> P,List<Integer> X){
		if (timeLimit > 0 && System.currentTimeMillis() - startTime >= timeLimit) return;
		nodes++;
		
		if(P.size() + C.size() < minSize)
		{
			return;
		}
		
		if (P.isEmpty()){
			if(X.isEmpty()) {report(C);}
			return;
		}
		
		int u;
//		List<Integer> iterP = new ArrayList<Integer>(P.size());
		
		u = getPivot(P, X);
		
//		for(int i : P){ 
//			if(!g.areNeighbors(i, u)) {iterP.add(i);}
//		}
		
		
		
		int i = P.size()-1;
		while (i >= 0){
			if(!g.areNeighbors(P.get(i), u))
		    {
				process(i,C,P,X);
		    }
			
			i--;
		}
		
//		if(P.size() + X.size() == g.vertices().size()) System.out.println("LAST ITER! Pivots in P: "+pivotsInP+" Pivots in X: "+pivotsInX);
    }
    

    //in BK1 param "i" indicates the index of the node in P. Here is the actual label of the node.
    void process (int i, List<Integer> C, List<Integer> P, List<Integer> X){
    	
    	int v = P.remove(i); 
    	
	    C.add(v);

	    List<Integer> newP = intersectWithNeighbors(P,v);
	    List<Integer> newX = intersectWithNeighbors(X,v);	    
	    
	    
	    expand(C,newP,newX);
	    
	    C.remove((Integer)v);
	    
	    X.add(v);
    }

    

    /**
     *Pivoting strategy.
     *Gets the first node in P.
     */
    protected int getPivot(List<Integer> P, List<Integer> X){
    	return P.get(0);
    }

    /**
     *Pivoting strategy.
     *Gets the first node in P.
     *returns -1 if not found.
     */
    protected int getRequiredPivot(List<Integer> P, List<Integer> X){
    	
    	for(int i : P)
    	{
    		if(requiredVertices.contains(i)) return i;
    	}
    	
    	return -1;
    }


}
