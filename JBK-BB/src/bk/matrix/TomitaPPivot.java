package bk.matrix;


import java.util.*;
import java.io.*;

import lib.ListUtils;
import lib.graph.Graph;
import lib.graph.IntGraph;

public class TomitaPPivot extends Tomita {

	public TomitaPPivot(IntGraph g){
		super(g); 
	}

    public TomitaPPivot(InputStream is) {
		super(is);
	}
    
    public TomitaPPivot(Graph graph, List<Integer> visited){
    	super(graph,visited);
    }

    public TomitaPPivot (Graph graph, List<Integer> toVisit, List<Integer> visited){
    	super(graph,toVisit,visited);
    }
    @Override
    /**
     *Tomita's Pivoting strategy.
     *The pivot is chosen as the node in P U N
     *with the highest number of neighbors in P.
     */
    protected int getTomitaPivot(List<Integer> P, List<Integer> X){
    	int best = P.get(0);
    	int bestVal = neighborsIn(P,best);

    	int node;
    	int val;

    	for(int i = 1; i<P.size(); i++){
    		node = P.get(i);
    		val = neighborsIn(P,node);

    		if(val>bestVal){
    			best = node;
    			bestVal = val;
    		}
    	}
    	
    	//If X is not empty, extend the search on X
//    	if(!X.isEmpty()){
//    		for(int i = 1; i<X.size(); i++){ //.get(0) is already taken in the initialization
//    			node = X.get(i);
//    			val = neighborsIn(P,node);
//
//    			if(val>bestVal){
//    				best = node;
//    				bestVal = val;
//    				fromX = true;
//    			}
//    		}
//    	}
//    	if(fromX) pivotsInX++;
//    	else pivotsInP++;

    	return best;
    }
    
    @Override
    /**
     *Tomita's Pivoting strategy.
     *The pivot is chosen as the node in P U N
     *with the highest number of neighbors in P.
     */
    protected int getTomitaRequiredPivot(List<Integer> P, List<Integer> X){
    	int best = -1;
    	int bestVal = -1;

    	int val;

    	for(int node : P){
    		if(requiredVertices.contains(node))
    		{
    			val = neighborsIn(P,node);

    			if(val>bestVal){
    				best = node;
    				bestVal = val;
    			}
    		}
    	}
    	
    	//If X is not empty, extend the search on X
//    	if(!X.isEmpty()){
//    		for(int i = 1; i<X.size(); i++){ //.get(0) is already taken in the initialization
//    			node = X.get(i);
//    			val = neighborsIn(P,node);
//
//    			if(val>bestVal){
//    				best = node;
//    				bestVal = val;
//    				fromX = true;
//    			}
//    		}
//    	}
//    	if(fromX) pivotsInX++;
//    	else pivotsInP++;

    	return best;
    }

    protected int neighborsIn(List<Integer> P, int node){
    	int neighNum = 0;
    	for(int n : P){
    		if(g.areNeighbors(n, node)) neighNum++;
    	}
    	return neighNum;
    }


}
