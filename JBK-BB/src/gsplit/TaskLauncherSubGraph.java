package gsplit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lib.SearchAlg;
import lib.graph.IntGraph;

public class TaskLauncherSubGraph {

	public boolean DEBUG = System.getProperty("DEBUG") != null;
	
	Map<String,String> stats;
	SearchAlg sa;
	List<List<Integer>> solution = new ArrayList<List<Integer>>();
	
	
	public void launchTask(IntGraph ig, Set<Integer> visited){
		
		sa = new bk.lists.Tomita(ig, visited);

		if(DEBUG) System.out.println("----------------------------------------------");
		if(DEBUG) System.out.println("Visited:"+visited);
		
		sa.search();
		
		if (stats == null){
			stats = sa.getStats().stats;
		} else {

			Map<String,String> ns = sa.getStats().stats;

			stats.put("clq", stats.get("clq") + ns.get("clq"));
			stats.put("nodes", stats.get("nodes") + ns.get("nodes"));
			stats.put("time", stats.get("time") + ns.get("time"));
			stats.put("max", ( Long.parseLong(stats.get("max")) > Long.parseLong(ns.get("max")) ? stats.get("max") : ns.get("max") )   );
		}
		
		
		if(DEBUG) System.out.println("Sol:"+sa.solution());
		
		if(DEBUG) System.out.println("----------------------------------------------");
		
		
		solution.addAll((List<List<Integer>>)sa.solution());
	}

	public void reset() {
		stats = null;
	}

	public void printStats() {
		for(String s : stats.keySet()){
			System.out.println(s+": "+stats.get(s));
		}
	}
	
	public void printSolution(){
		System.out.println(solution);
	}
}
