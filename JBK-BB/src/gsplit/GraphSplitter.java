package gsplit;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lib.AlgStats;
import lib.ListUtils;
import lib.SearchAlg;
import lib.graph.IntGraph;
import lib.graph.MapListGraph;
import lib.parser.Parser;

public class GraphSplitter implements SearchAlg {

	public boolean DEBUG = System.getProperty("DEBUG") != null;
	
	private TaskLauncherFullGraph tl;
	private MapListGraph g;
	
	private int size;
	private int minLabel = 0;
	
	private List<Integer> visited;
	
	public GraphSplitter(InputStream is){
		this(new TaskLauncherFullGraph(),new MapListGraph(Parser.getMapListGraph(is)) );
	}
	
	public GraphSplitter (TaskLauncherFullGraph t, MapListGraph graph){
		this.tl = t;
		this.g = graph;
		
		visited = new ArrayList<Integer> ();
		
		g.orderGraph();
		
		size = g.vertices().size();
		
		minLabel = Collections.min(g.vertices());
	}
	
	public void start(){
		tl.reset();
		
		splitAndLaunchDistance0();
		
	}
	
	public void printStats(){
		tl.printStats();
	}
	public void printSolution(){
		tl.printSolution();
	}
	
	//create chunks with all vertices within distance "distance" from the key
		public void splitAndLaunchDistance(int distance){
			//TODO
		}
		
	//create chunks with all vertices within distance 1 from the key (i.e. neighbors of the key)
		public void splitAndLaunchDistance1(){
			
			int k = selectKey();
			
			List<Integer> A, O;
			
			A = new ArrayList<Integer>(g.neighbors(k));
			O = new ArrayList<Integer>(A.size());
			
			A.add(k);
			
			for(int el : A){
				O.addAll(g.neighbors(el)); //TODO exclude duplicates
			}
			
			
			//TODO
		}
		
	//create chunks with all vertices within distance 0 from the key (i.e. the key)
		public void splitAndLaunchDistance0(){
			
			int k = selectKey();
			
			if(k < 0){
				System.out.println("Finished!!");
				tl.printStats();
				return;
			}
			
			List<Integer> A, O;
			
			A = new ArrayList<Integer>(1);
			A.add(k);
			
			O = new ArrayList<Integer>(g.neighbors(k));
			
			List<Integer> united = ListUtils.uniteOrdered(A,O);
			
			tl.launchTask(g, new HashSet<Integer>(united), new HashSet<Integer>(ListUtils.intersectOrdered(visited, united )));
			
			/** -with "subGraph generation"
			 * 
			IntGraph newG = buildGraph(united);

			tl.launchTask(newG, ListUtils.intersectOrdered(visited, united ));
			 */
			
			ListUtils.addOrdered(visited, A);
			
			splitAndLaunchDistance0();
		}
	
	
	//create chunks of maximum "capacity" vertices
	public void splitAndLaunchCapacity(int capacity){
		//TODO
	}
	
	
	
	/**
	 * assuming that visited and vertices both ordered, minimum label being minLabel.
	 * O(1) - O( visited )
	 */
	private int selectKey(){

		if(visited.isEmpty()) return minLabel;
		
		int vsize = visited.size();
		
		List<Integer> vertices = this.g.vertices();

		if(vsize == vertices.size()) return -1; //if the sizes are the same we visited everything, return -1
		
		//assuming both visited and vertices are ordered, the first element to be found different means that the element in vertices is not included in visited
		for(int i=0; i<vsize; i++){
			if(visited.get(i) != vertices.get(i) ) return vertices.get(i);
		}
		
		//if we're here, visited doesn't have "holes",
		//so we take the (vsize+1)th element of vertices (in the "vsize" position)
		return vertices.get(vsize);
		
	}
	
	private IntGraph buildGraph(List<Integer> a){
		
		Map<Integer,List<Integer>> map = new HashMap<Integer,List<Integer>> ();
		
		for(int x : a){
			map.put(x, ListUtils.intersectOrdered(a,g.neighbors(x)));
		}
		
		
		IntGraph ig = new MapListGraph(map);
		
		return ig;
	}

	@Override
	public void search() {
		this.start();
		
	}

	@Override
	public void setTimeLimit(long limit) {
		System.out.println("GSPLITTER TIMEOUT NOT IMPLEMENTED");
		
	}

	@Override
	public AlgStats getStats() {
		return new AlgStats(tl.stats);
	}

	@Override
	public void setAborted() {
		//throw new RuntimeException ("not implemented");
		System.out.println("GSPLITTER ABORTED");
	}

	@Override
	public Object solution() {
		return tl.solution;
	}
	
	
	@Override
	public void setFilters(Set<Integer> kernel, Set<Integer> borderVis) {
		System.out.println("Filters not needed/implemented for this class of algorithms.");
	}
}
