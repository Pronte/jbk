package util;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lib.AlgStats;
import lib.DegeneracyUtils;
import lib.graph.Graph;
import lib.graph.IntGraph;
import lib.parser.Parser;
import bk.BK;

public class GraphStats extends BK {
	
	IntGraph g;

	public GraphStats(InputStream is) {
		this(Parser.getIntGraph(is));
	}


	public GraphStats(Graph graph) {
		super(graph);
		
		g = (IntGraph) graph;
	}


	@Override
	public Object solution() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void start() {	
	}
	
	
	@Override
	public AlgStats getStats(){
		
		List<Integer> v = this.g.vertices();
		
		int n = v.size();
		
		long m = 0;
		for(int x : v)
		{
			m += g.neighbors(x).size();
		}
		
		m = m/2;
		
		double dens = (m*2.0)/(((long)n-1)*n);
		
		int degen = DegeneracyUtils.degeneracy(g);
		
		//cardinality -> #nodes with such cardinality
		//v.size()-1 is the maximum cardinality of a node (i.e. the max index), hence the length of the array must be v.size()
		int[] cards = new int[v.size()];
		
		Arrays.fill(cards, 0);
		
		for(int i : v)
		{
			cards[g.neighbors(i).size()]++;
		}
		
		//calculating d* index
		int num = 0, dstar = 0;
		
		for(int deg = cards.length-1; deg >= 0; deg--)
		{
			if(deg < num) break;
			
			num+=cards[deg];
			
			dstar = Math.max(dstar, Math.min(deg, num));
		}
		
		Map<String,String> stats = new HashMap<String,String>();
		
//		stats.put("time", ""+0);
//		stats.put("alg",this.getClass().getName());
		stats.put("n", ""+v.size());
		stats.put("m", ""+m);
		stats.put("dens", ""+dens);
		stats.put("deg", ""+degen);
		stats.put("dstar", ""+dstar);

		return new AlgStats(stats){
			@Override
			public String toString()
			{
				return 	/*"--GRAPH STATS--\n"+*/
						"Vertices: "+get("n")+"\n"+
						"Edges: "+get("m")+"\n"+
						"Density: "+get("dens")+"\n"+
						"Degeneracy: "+get("deg")+"\n"+
						"Dstar: "+get("dstar")+"\n";
			}
		};
	}

}
