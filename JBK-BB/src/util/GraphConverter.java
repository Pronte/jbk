package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import lib.graph.MapListGraph;
import lib.parser.ASCIIARCSParser;
import lib.parser.Parser;

public class GraphConverter {

	public static void toAsciiArcs(String graphFile)
	{
		

		File gf = new File(graphFile);
		String format;
		
		if(gf.getName().contains("."))
		{
			format = graphFile.substring(graphFile.lastIndexOf(".")+1);
		}
		else
		{
			format = "dimacs";
		}
		
		System.out.println("Converting format "+format+" to asciiarcs..");
		
		toAsciiArcs(graphFile, format);
		
	}
	
	
	public static void toAsciiArcs(String graphFile, String format)
	{
		
		
		File gf = new File(graphFile);
		String outName;
		
		if(gf.getName().contains("."))
		{
			outName = graphFile.substring(0, graphFile.lastIndexOf("."))+".aa";
		}
		else
		{
			outName = graphFile+".aa";
		}
		
		PrintWriter out = null;
		
		File outFile = new File (outName);
		
		if(outFile.exists()){
			System.out.println("File "+outFile+" already exists! skipping..");
			return;
		}
		
		try {
			out = new PrintWriter(outFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Parser.setParser(format);
		
		MapListGraph g = null;
		
		try {
			g = new MapListGraph(Parser.getMapListGraph(new FileInputStream(gf)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		ASCIIARCSParser.save(g, out);
		
		System.out.println("done!");
		
	}
	
}
