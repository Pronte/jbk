package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ResultsParser {

	public static void main(String[] args) throws Exception {
		

		String f = args[1]; //"C:/graphs/treetw5/";
		String rf = f+"results/";
		File resfolder= new File(rf);
		
		String[] algs = resfolder.list();
		List<List<Integer>> ress = new ArrayList<List<Integer>>(algs.length);
		for(String n : algs)
		{
			List<Integer> l = getTimes(rf+n);
			ress.add(l);
//			System.out.println(n);
//			System.out.println(l);
//			System.out.println(Collections.max(l));
		}
		
		String fstats = f+"util.GraphStats.csv";
		
		List<List<Double>> plist = new ArrayList<List<Double>>();

		plist.add(getProp(fstats, "Vertices"));
		plist.add(getProp(fstats, "Edges"));
		plist.add(getProp(fstats, "Degeneracy"));
		plist.add(getProp(fstats, "Density"));
		plist.add(getProp(fstats, "Dstar"));
		
		
		
		int[] best = new int[ress.get(0).size()];
		
		for(int i = 0; i < ress.get(0).size(); i++)
		{
			int min = 0;
			int val = ress.get(0).get(i);
			
			for(int j = 1; j < ress.size(); j++)
			{
				if(ress.get(j).get(i) < val)
				{
					min = j;
					val = ress.get(j).get(i);
				}
			}
			
			best[i] = min;
		}

		int ob = 0;
		for(int i=0; i < best.length; i++) ob += ress.get(best[i]).get(i);
		
		String[] dtrees = {"old"};//,"new","06","boh"};
		int[] sums = new int[dtrees.length];
		Arrays.fill(sums, 0);
		
		int[][]choices = new int[dtrees.length][ress.get(0).size()];
//		int[] choices = new int[ress.get(0).size()];
//		int[] choices2 = new int[ress.get(0).size()];
		
		
		for(int i = 0; i < ress.get(0).size(); i++)
		{
			choices[0][i] = predictOld(plist, i);
//			choices[1][i] = predict(plist, i);
//			choices[2][i] = predict06(plist, i);
//			choices[3][i] = predictBoh(plist, i);
		}

		for(int i=0; i < best.length; i++)
		{
			for(int j=0; j < sums.length; j++)
			{
				sums[j] += ress.get(choices[j][i]).get(i);	
			}
		}
		
		for(int i = 0; i < dtrees.length; i++)
		{
			System.out.println(dtrees[i]+":");
			System.out.println(Arrays.toString(choices[i]));
		}
		
//		for(int i=0; i < best.length; i++) treeSum2 += ress.get(choices2[i]).get(i);
		
		int same = 0;
//		for(int i = 0; i < best.length; i++) if(best[i]==choices[i]) same++;

		System.out.println("BEST ALG:");
		System.out.println(Arrays.toString(best));
//		System.out.println("CHOICE2:");
//		System.out.println(Arrays.toString(choices2));
//		System.out.println("SAME: "+same+" our of "+best.length);
		
//		System.out.println("PROPS:");
//		for(int i=0; i < plist.size(); i++) System.out.println(plist.get(i));
		

		
		
//		for(int i = 0; i < algs.length; i++)
//		{
//			System.out.println(i+": "+algs[i]+" (best "+count(best,i)+" times, timeout: "+countGT(ress.get(i),120000)+" times)");
//		}
		
		String of = f+"results.txt";
		
		BufferedWriter out = new BufferedWriter(new FileWriter(of), 10000);
		
		out.write("Alg overall best timeout\n");
		
		for(int i = 0; i < algs.length; i++)
		{
			out.write(algs[i].substring(algs[i].indexOf(".")+1, algs[i].indexOf(".csv"))+" "+sum(ress.get(i))+" "+count(best,i)+" "+countGT(ress.get(i),120000)+"\n");
			System.out.println(i+": "+algs[i].substring(algs[i].indexOf(".")+1, algs[i].indexOf(".csv"))+" "+sum(ress.get(i))+" "+count(best,i)+" "+countGT(ress.get(i),120000));
		}
		
		out.write("best "+ob+"\n");
		
		for(int i = 0 ; i < dtrees.length; i++)
		{
			out.write(dtrees[i]+" "+sums[i]+"\n");
			System.out.println(dtrees[i]+" "+sums[i]);
		}
		
		out.close();
		
		System.out.println("best "+ob);
		of = f+"dataset.csv";
		out = new BufferedWriter(new FileWriter(of), 10000);
		out.write("\"Vertex\",\"Edges\",\"Degeneracy\",\"Density\",\"c_Index\",\"Algorithm\"\n");
		for(int i= 0; i< best.length; i++)
		{
			out.write(tist(plist.get(0).get(i))+","+tist(plist.get(1).get(i))+","+tist(plist.get(2).get(i))+","+plist.get(3).get(i)+","+tist(plist.get(4).get(i))+",\""+algs[best[i]]+"\"\n");
		}
		out.close();
		
		//"Vertex","Edges","Degeneracy","Density","c_Index","Algorithm"
		//10,17,3,0.37777777,4,"bk.matrix.TomitaXPivot"
	}

	public static int predictOld(List<List<Double>> stats, int g)
	{
		if(stats.get(3).get(g) >= 0.0045) //density
		{
			if (stats.get(4).get(g) >= 27)
			{
				return 0;// 5; //matrix //bs.tomita
			} 
			else
			{
				return 1;//6; //matrix.XPivot
			}
		} 
		else
		{
			return 2;//8; //"sparse.XPivot"
		}
	}
	//0"Vertex",1"Edges",2"Degeneracy",3"Density",4"c_Index","Algorithm"
		public static int predict06(List<List<Double>> stats, int g)
		{
			if(stats.get(1).get(g) < 28020)
			{
				if(stats.get(0).get(g) < 3294)
				{
					return 0; //bs.tomita
				}
				else
				{
					return 7; //sparse.tomita
				}
			}
			else 
				return 5; //matrix.tomita
		}
		
		//0"Vertex",1"Edges",2"Degeneracy",3"Density",4"c_Index","Algorithm"
				public static int predictBoh(List<List<Double>> stats, int g)
				{
					if(stats.get(4).get(g) < 69.5)
					{
						if(stats.get(0).get(g) >= 555)
						{
							return 0; //bs.tomita
						}
						else
						{
							return 6; //matrix.xpivot
						}
					}
					else
					{
						if(stats.get(3).get(g) >= 0.02289)
						{
							return 0; //bs.tomita
						}
						else
						{
							return 6; //matrix.xpivot
						}
					}
				}

		
		
		//0"Vertex",1"Edges",2"Degeneracy",3"Density",4"c_Index","Algorithm"
		public static int predict05(List<List<Double>> stats, int g)
		{
			if(stats.get(1).get(g) < 28020)
			{
				if(stats.get(0).get(g) < 3294)
				{
					if(stats.get(1).get(g) < 25420)
					{
						return 0; //bs.tomita
					}
					else
					{
						return 1; //list.eppstein
					}
				}
				else
				{
					return 7; //sparse.tomita
				}
			}
			else 
				return 5; //matrix.tomita
		}

	//0"Vertex",1"Edges",2"Degeneracy",3"Density",4"c_Index","Algorithm"
	public static int predict(List<List<Double>> stats, int g)
	{
		if(stats.get(1).get(g) < 33250)
		{
			return 0; //bs.static
		}
		else if(stats.get(1).get(g) < 49800)
		{
			if(stats.get(0).get(g) < 3006)
			{
				return 0; //bs.static
			}
			else if(stats.get(2).get(g) >= 40.5)
			{
				return 0; //bs.static
			}
			else
			{
				return 7; //matrix.xpivot
			}
		} 
		else if(stats.get(3).get(g) >= 0.01771)
		{
			return 0;//bs.static
		}
		else
		{
			return 7; //matrix.xpivot
		}
		
	}
	
	
	public static String tist(double d)
	{
		return ""+((int)d);
	}
	
	public static int count(int[] a, int v)
	{
		int c = 0;
		for(int i : a) if(i == v) c++;
		
		return c;
	}
	public static int countGT(List<Integer> a, int v)
	{
		int c = 0;
		for(int i : a) if(i >= v) c++;
		
		return c;
	}
	
	public static int sum(List<Integer> a)
	{
		int timeout = 120000;
		int timeoutMalus = 0;
		int c = 0;
		for(int i : a){
			c+=i;
			if(i >= timeout) c+=timeoutMalus;
		}
		
		return c;
	}


	public static List<Integer> getTimes(String file) throws Exception
	{
		File f = new File(file);

		Scanner s = new Scanner(new BufferedReader(new FileReader(f), 10000 ));

		List<Integer> res = new ArrayList<Integer>();
		
		String l;
		
		while(s.hasNextLine())
		{
			l = s.nextLine();
			if(l.startsWith("Time"))
			{
				res.add(Integer.parseInt(l.substring(l.indexOf(" ")+1)));
			}
		}
		
		s.close();
		
		return res;

	}
	public static List<Double> getProp(String file, String prop) throws Exception
	{
		File f = new File(file);

		Scanner s = new Scanner(new BufferedReader(new FileReader(f), 10000 ));

		List<Double> res = new ArrayList<>();
		
		String l;
		
		while(s.hasNextLine())
		{
			l = s.nextLine();
			if(l.startsWith(prop))
			{
				res.add(Double.parseDouble(l.substring(l.indexOf(" ")+1)));
			}
		}
		
		s.close();
		
		return res;

	}



}