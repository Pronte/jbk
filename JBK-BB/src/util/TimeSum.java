package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TimeSum {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		
		System.out.println( times("C:\\graphs\\tree\\Slave1times-mix-0.0088") ) ;

	}
	
	
	public static double times(String file) throws FileNotFoundException
	{
		
		Scanner s = new Scanner(new File(file));
		
		double sum = 0;
		double max = 0;
		double tmp = 0;
		while(s.hasNextLine())
		{
			tmp = time(s.nextLine());
			sum += tmp;
			if(tmp > max) max = tmp;
		}
		
		s.close();
		
		System.out.println("max: "+max);
		
		return sum;
		
	}

	public static double time(String line)
	{
		try{
			String ss = line.substring(line.indexOf(":")+2, line.lastIndexOf(' '));
			
			System.out.println(ss);
//			System.out.println(Double.parseDouble(ss));
//			System.out.println("------------------------------------------------");
			return Double.parseDouble(ss);
		} catch (Exception e)
		{
			System.out.println(line);
			e.printStackTrace();
			return 0;
		}
		
	}
}
