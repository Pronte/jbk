package run;

import java.util.BitSet;

import lib.graph.bs.IBitSet;

public class BSTests {
public static void main(String[] args) {

	BitSet b = new BitSet();
	IBitSet ib = new IBitSet();
	
	/*
	for(int i = 30000000; i < 50000000; i++){
		boolean val = Math.random() < 0.5;
		
		b.set(i, val);
		ib.set(i,val);
	}*/

	b.set(5);
	b.set(3000);
	b.set(600000);
	b.set(90000000);
	b.set(90000001);
	ib.set(5);
	ib.set(3000);
	ib.set(600000);
	ib.set(90000000);
	ib.set(90000005);

	b.clear(5);
	b.clear(3000);
	//b.clear(600000);
	b.clear(90000000);
	ib.clear(5);
	ib.clear(3000);
	//ib.clear(600000);
	ib.clear(90000000);
	
	long t0 = System.currentTimeMillis();
	
	int k = b.nextSetBit(0);
	
	long t1 = System.currentTimeMillis();
	
	int h = ib.nextSetBit(0);
	
	long t2 = System.currentTimeMillis();
	

	System.out.println("BitSet ind: "+k+" time: "+(t1-t0));
	System.out.println("IBitSet ind: "+h+" time: "+(t2-t1));
	
}
}
