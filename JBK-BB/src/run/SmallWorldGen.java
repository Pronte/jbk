package run;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;

public class SmallWorldGen {

	static PrintStream out = System.out;
	static BufferedWriter writer;
	static StringBuffer buff = new StringBuffer();
	
	static long t0 = System.currentTimeMillis();
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		String[] argv = {"50000","100", "0.1","10","5"};

		args = argv;
		
		String folder = "C:\\Users\\Alessio\\Documents\\GEPHI\\";
		String extension = ".CSV";
		
		String fname = "SW mul n"+args[0]+" p"+args[1]+" m"+args[2]+" s"+args[3]+" e"+args[4];
		String finFname = fname;
		
		File f = new File(folder+finFname+extension);
		int i = 0;
		while(f.exists())
		{
			finFname = fname+"-0"+i;
			f = new File(folder+finFname+extension);
			i++;
			System.out.println("changing"+i);
		}
		
		out = new PrintStream(f);
//		writer = new BufferedWriter(new FileWriter(f, true));
		
		generate(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Double.parseDouble(args[2]), Integer.parseInt(args[3]), Double.parseDouble(args[4]));

	}
	
	
	public static void generate(int n, int coeff, double minProb, int socSize, double exp) throws IOException
	{
		double c;
		int dist;
		double factor;
		//out.println("[SMALL WORLD 0.1] n="+n+" coeff="+coeff);
		for(int i = 1; i < n; i++)
		{
			long t1 = System.currentTimeMillis();
			if(i%100 == 0)
			{
				System.out.println("done "+i+" - "+(t1-t0));
				t0 = t1;
			}
			for(int j = i+1; j <= n; j++ )
			{
				dist = j-i;
				if(dist > n/2) dist = n-dist;
				
				factor = socSize - dist;

				if(factor < 0 ) { factor = 1/(factor*factor*factor); }
				else if(factor > 0) { factor = 10*factor*factor*factor; }
				else {factor = 10; }
				
//				c = minProb + (coeff*(Math.pow(exp, (socSize - (dist)))));
				c = minProb + coeff*factor;
				if(Math.random()*10000 < c)
				{
//					writer.write(i+";"+j);
					out.println(i+";"+j);
//					buff.append(i+";"+j+"\n");
				}
			}
		}
		
		
		
	}

}
