package run;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import util.ResultsParser;
import lib.AlgStats;
import lib.SearchAlg;
import lib.graph.Graph;
import lib.graph.MapListGraph;
import lib.graph.MatrixGraph;
import lib.graph.SparseBKGraph;
import lib.graph.bs.BSIntGraph;
import lib.parser.Parser;
import lib.parser.SNCParser;

public class SNCAccessPoint {

	/**
	 * @param args
	 * usage: args[0] - graph file path, args[1] - alg name, args[2] - timeout
	 * @throws FileNotFoundException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws Exception
	{
		System.out.println("Usage: outfolder part_folder || 'parse' outfolder");
		
		if(args[0].equals("parse")){ResultsParser.main(args); return;}
		
		Parser.setParser("snc");

//		String gf = null;
//		gf = "C:/GRAPHS/test.blk";
//		gf = "C:/GRAPHS/milleventi.dat";

//		String alg = "bk.lists.Tomita";
		String alg = "bk.matrix.Tomita";
//		String alg = "bk.bitsets.TomitaBS";
		
		 
//		runAlg(gf, alg, -1);
		String tfold = args[0];// "C:/graphs/treecat/";
		String folder = args[1]; //"C:/graphs/Cat_part";
//		String folder = "C:/graphs/Twitter_Triples_Execution/Preprocessing/Partitioning/Partitioning_Data";
//		String block = "C:/graphs/Twitter_Triples_Execution/Preprocessing/Partitioning/Partitioning_Data/Partition_0_1016.blk";
		
		
		
		File f = new File(folder);

		List<String> files = new ArrayList<String>();
		
		for(File gf : f.listFiles())
		{
			if(gf.getAbsolutePath().endsWith("blk"))
			{
//				System.out.println("adding: "+gf.getAbsolutePath());
				files.add(gf.getAbsolutePath());
			}
		}

		String[] algs = {"util.GraphStats","bk.bitsets.TomitaBS","bk.matrix.TomitaXPivot","bk.sparse.TomitaXPivot"};
//		String[] algs = {"util.GraphStats"};//"bk.sparse.BK1", "bk.sparse.Tomita","bk.sparse.TomitaXPivot"};
		
		
//		runAlg(block, algs[0], -1);
		
		for(String a : algs)
		{
//			if(true) break;
			System.out.println("----------------------------------------------------------------");
			System.out.println("ALG: "+a);
			
			AlgStats[] stats = test(a, files);
			
			File of = new File(tfold+"results/"+a+".csv");
			
			System.out.println(of.getAbsolutePath());
			of.createNewFile();
				
			BufferedWriter out = new BufferedWriter(new FileWriter(of), 10000);
				
				
			for(int g = 0; g < stats.length; g++)
			{
				out.write(files.get(g)+"\n");
				out.write(stats[g].toString());
			}
			
			out.flush();
			out.close();
		}
		
		
	}
	
	
	public static AlgStats[] test(String alg, List<String> files) throws Exception
	{
		AlgStats[] stats = new AlgStats[files.size()];
		
		
		
		for(int i = 0; i < files.size(); i++)
		{
			String f = files.get(i);
			
			System.out.println(f);

			stats[i] = runAlg(f, alg, 120000);

			System.out.println(stats[i]);
				
			stats[i].setCliques(null);
		}
		
		return stats;
	}
	
		
	public static AlgStats runAlg(String graphfile, String algClass, long tlimit) throws Exception{
		
		System.out.println("Class: "+algClass.toString());
		
		
		SNCParser p = new SNCParser();
		
		Map<String,Object> info = p.getBlockInfo(new FileInputStream(graphfile));

		Set<Integer> kernel = (Set<Integer>) info.get("kernel");
		Set<Integer> borderVis = (Set<Integer>) info.get("bordervis");
		List<Set<Integer>> neighs = (List<Set<Integer>>) info.get("neighs");
		int maxLabel = Integer.parseInt((String) info.get("maxlabel"));
		
		
		
		//build graph here
		
		Graph g = null;
		
		if(algClass.contains(".lists."))
			g = getMapListGraph(neighs, maxLabel);
		else if(algClass.contains(".matrix."))
			g = getMatrixGraph(neighs, maxLabel);
		else if(algClass.contains(".bitsets."))
			g = getBSGraph(neighs, maxLabel);
		else if(algClass.contains(".sparse."))
			g = getSparseGraph(neighs, maxLabel);
		else if(algClass.contains("util.GraphStats"))
			g = getMapListGraph(neighs, maxLabel);
		else{
			System.out.println("Alg package of "+algClass+" not recognized!!");
			System.exit(1);
		}

		Class ac = Class.forName(algClass);
		
		Constructor c = ac.getConstructor(Graph.class);
		SearchAlg sa = (SearchAlg) c.newInstance(g);
		
		System.out.println("k: "+kernel.size()+" bv: "+borderVis.size());//+" gv: "+((MapListGraph)g).vertices().size());
		
		sa.setFilters(kernel, borderVis);
		//if(DEBUG) System.out.println("Algotithm object created, class="+sa.getClass().getName());
		
		sa.setTimeLimit(tlimit);
		
		sa.search();
		
		sa.printStats();
		
		AlgStats stats = sa.getStats();
		
		stats.setCliques(sa.solution());
		
		return stats;
	}
	

	
	public static Graph getSparseGraph(List<Set<Integer>> neighs, int maxLabel)
	{
		int[][] neighborsVecs = new int[maxLabel+1][];
		
		for(int i = 0; i <= maxLabel ; i++)
		{
			neighborsVecs[i] = new int[neighs.get(i).size()];
			int count = 0;
			for(int j : neighs.get(i))
			{
				neighborsVecs[i][count] = j;
				count++;
			}
		}
		
		return new SparseBKGraph(neighborsVecs);
	}	
	
	public static Graph getBSGraph(List<Set<Integer>> neighs, int maxLabel)
	{
		BSIntGraph g = new BSIntGraph();
		
		for(int i = 0; i < neighs.size() ; i++)
		{
			if(neighs.get(i) != null && !neighs.get(i).isEmpty())
			{
				for(int j : neighs.get(i))
					g.setNeighbors(i,j);
			}
		}
		
		return g;
	}
	

	
	public static Graph getMatrixGraph(List<Set<Integer>> neighs, int maxLabel)
	{	
		return new MatrixGraph(neighs);
	}
	

	
	public static Graph getMapListGraph(List<Set<Integer>> neighs, int maxLabel)
	{
		Map<Integer,List<Integer>> mlg = new HashMap<Integer, List<Integer>>();
		
		for(int i = 0; i < neighs.size(); i++)
		{
			if(neighs.get(i) != null && !neighs.get(i).isEmpty())
			{
				mlg.put(i, new ArrayList<Integer>(neighs.get(i)));
			}
			else
				System.out.println(neighs.get(i));
		}
		
		return new MapListGraph(mlg);
	}
	
	
	

	public static Class getAlgClass(String alg) throws ClassNotFoundException {

			
		if(alg == null || alg.equals("") || alg.equalsIgnoreCase("default")){
					
					alg = "bk.sparse.Tomita";
//					alg = "bk.lists.Tomita";
		//			alg = "bk.bitsets.BK1BS";
//					alg = "bk.bitsets.TomitaBS";
		//			alg = "bk.lists.BK1";
		//			alg = "bk.lists.BKPivot";
						
					
		} else if(alg.equals("BK1")) alg = "bk.lists.BK1";
		else if(alg.equals("Tomita")) alg = "bk.lists.Tomita";
		else if(alg.equals("BK1Ord")) alg = "bk.lists.BK1Ord";
		else if(alg.equals("TomitaOrd")) alg = "bk.lists.TomitaOrd";
		else if(alg.equals("BK1BS")) alg = "bk.bitsets.BK1BS";
		else if(alg.equals("TomitaBS")) alg = "bk.bitsets.TomitaBS";
		
		try{
			if (! alg.startsWith("bk."))
				return Class.forName("bk.sparse."+alg);
			else
				return Class.forName(alg);
		} catch (ClassNotFoundException e){
			return Class.forName("bk.lists."+alg);
		}
	}


	public static void boh(String[] args) throws Exception
	{
//		System.setProperty("DEBUG", null);
		Parser.setParser("snc");
		
		String[] argsHack = {"C:\\Graphs\\milleventi.dat"};
		
		args = argsHack;
		
		String fpath = args[0];
		Long timeout = -1L;
		String algName = "bk.sparse.Eppstein";
		
		if(args.length > 2)
		{
			try
			{
				timeout = Long.parseLong(args[2]);
				algName = args[1];
			}
			catch (NumberFormatException e)
			{
				try
				{
					timeout = Long.parseLong(args[1]);
					algName = args[2];
				}
				catch(NumberFormatException e2)
				{
					System.err.println("INVALID ARGUMENTS. Expected: file path - [alg name] - [timeout]");
					System.exit(1);
				}
			}
		}
		else if( args.length > 1)
		{
			try
			{
				timeout = Long.parseLong(args[1]);
			}
			catch (NumberFormatException e)
			{
				algName = args[1];
			}
		}

//		InputStream graphfile = new FileInputStream(new File(fpath));
		
//		Class algClass = getAlgClass(algName);
		
		
		AlgStats stats = runAlg(fpath, algName, timeout);
		
	}


}
