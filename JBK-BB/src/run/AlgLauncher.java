package run;

import java.util.Timer;

import lib.SearchAlg;

public class AlgLauncher implements Runnable {

	SearchAlg sa;
	long timeout;
	static Object waiter;
	boolean ended = false;
	
	public AlgLauncher(SearchAlg sa, long timeout){
		this.sa = sa;
		this.timeout = timeout;
	}
	
	public void launch(){
		Thread t = new Thread(this);
		
		waiter = new Object();
		
		
		t.start();
		
		try {
			synchronized(waiter){
				waiter.wait(timeout);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if(!ended){
			t.interrupt();
			sa.setAborted();
		}
	}
	
	
	public void run(){
		sa.search();
		ended = true;
		synchronized(waiter){
			waiter.notifyAll();
		}
	}
	
}
