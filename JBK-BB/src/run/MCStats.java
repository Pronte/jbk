package run;

import java.util.*;
import java.io.File;

public class MCStats { 
  
    public static void main(String[] args) throws Exception {

    	String alg = args[0];

    	String graphsFname = "graphs.txt";

    	if(args.length>1) { graphsFname = args[1]; }

    	Scanner s = new Scanner(new File(graphsFname));

    	List<String> graphs = new ArrayList<String>();

    	while(s.hasNextLine()){
    		graphs.add(s.nextLine());
    	}

    	for(String graph : graphs){
    	
    	System.out.println("####################"+graph+"####################");

    	String[] argv = {alg,graph};

    	MCEnv.main(argv);
    	}

}

}