package run;

import gsplit.GraphSplitter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import lib.AlgStats;
import lib.ListUtils;
import lib.Logger;
import lib.SearchAlg;
import lib.parser.Parser;
import bk.BK;

public class Tester {
	
	public static boolean DEBUG = true;
	
	
	// this is supposed to be the "GRAPHS" folder in the same location as the portable eclipse folder	
	private static String path = null;
	
	public static String fs = File.separator;
	
	//assumed use on portable eclipse contained in a folder having "ECLIPSE" in its name (e.g. "ECLIPSEPORTABLE").
	//it is assumed that in the same folder as the eclipse distribution there is a "GRAPHS" folder containing graphs.
	public static void main(String[] args) throws Exception {
		
		String g = "C:/graphs/lasagne/wiki-Vote.nde";
		String format = "nde";
		String alg = "bk.bitsets.TomitaBS";
		
		run(new String[]{g,format,alg});
		
	}
		
	public static void run(String[] args) throws Exception{
		
		if(DEBUG) System.setProperty("DEBUG", "true"); // anything not null is true
		
		String help = "Usage: java -jar this.jar graph_file format [algorithm]\n"
				+ "-format is one of: \n ascii (simple list of edges) \n nde (see http://lasagne-unifi.sourceforge.net/) \n dimacs (see http://lpsolve.sourceforge.net/5.5/DIMACS_maxf.htm) \n clq (see https://turing.cs.hbg.psu.edu/txn131/clique.html)\n"
				+ "-algorithm is one of: \n"
				+ " BKPivot \n Tomita \n Eppstein \n TomitaXPivot \n TomitaBS (uses bitsets) [default option] \n TomitaM (uses adj matrix)\n"
				+ "For more options you can try the exact class names in package \"bk\" in the sources (e.g. bk.bitsets.BKPivot) but you should check correctness of their result\n";
		
		if(args.length < 2) {
			System.out.println(help);
			System.exit(0);
		}
		
		String gname = args[0];
		String format = args[1];
		Parser.setParser(format);
		String algname;
		if(args.length == 2){
			algname = "default";
		}
		else{
			algname = args[2];
		}
		
		
		AlgStats s = runAlg(gname, algname);
		
		System.out.println(s.toString());
		
		//do something with s
		
	}
		
	public static void runSomeTests() throws Exception{
		
		String alg = "bk.sparse.Tomita";
		
		//TODO: set splitter on sparse.eppstein, but also not. maybe.
//		splitterTest();

//		alg = "gsplit.GraphSplitter";
		
		List<String> algs = new ArrayList<String>();
		algs.add("bk.matrix.Tomita");
		algs.add("bk.matrix.TomitaXPivot");
//		algs.add("bk.sparse.Tomita");
//		algs.add("bk.sparse.TomitaMG");
		algs.add("bk.sparse.Eppstein");
//		algs.add("bk.sparse.TomitaXPivot");
		algs.add("bk.bitsets.TomitaBS");
//		algs.add("bk.bitsets.TomitaBSAlt");
//		algs.add("bk.bitsets.BKStaticPivotBS");
//		algs.add("bk.lists.TomitaOrd");
//		algs.add("bk.lists.EppsteinOrd");
		algs.add("util.GraphStats");
		
		List<String> graphs = new ArrayList<String>();
		graphs.add(getFolder("oldenv") + "\\100-60-00");
		graphs.add(getFolder("oldenv") + "\\1000-20-00");
		graphs.add(getFolder("GEPHI") + "\\DIMACS\\SWWS-10000-200-250.CSV");
		graphs.add(getFolder("GEPHI") + "\\DIMACS\\SWWS-10000-100-80.CSV");
		
		graphs = getAllFilesAsString(getFolder("random")+"\\BASW-n1000");

		Collections.sort(graphs, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				// TODO Auto-generated method stub
				return value(o1) - value(o2);
			}
			
			private int value(String s)
			{
				String[] arr = s.split("-");
				return Integer.parseInt(arr[arr.length-2]);
			}
			
		});
//		System.setProperty("VERTEXCOVER", "TRUE");

		Parser.setParser("clq");
//		alg = "bk.lists.TomitaOrd";
//		alg = "bk.lists.EppsteinOrd";
//		alg = "bk.matrix.BKLayeredPivot";
//		alg = "bk.matrix.BKPivot";
//		alg = "bk.matrix.Tomita";
//		alg = "bk.matrix.TomitaXPivot";
		alg = "bk.matrix.BKPivot";
//		alg = "bk.bitsets.TomitaBS";
//		alg = "bk.sparse.Eppstein";
		
//		testOldEnv(alg);
		
		
		
//		Collections.reverse(graphs);
		
//		Parser.setParser("eppstein");
//		separatedEppDSComparison(algs);
		
//		comparison(algs, graphs, getFolder("res")+"\\BASW-n1000");
		
//		getAllEppsteinDataSetGraphs();
		
		
		String benchmark = "C:\\Users\\Alessio\\Documents\\UNI\\TESI-PATRICK\\Graphs\\BENCH\\";
		
		List<String> files  = getAllFilesAsString(benchmark);
		
//		comparison(algs,files,getFolder("res")+"\\tesi\\benchmark");

		String graph1 = getFolder("oldenv")+"\\1000-20-00";
//		String graph1 = getFolder("GEPHI")+"\\tesi\\BASW1.clq";
		String graph2 = getFolder("GEPHI")+"\\tesi\\BASW2.clq";
		String graph3 = getFolder("GEPHI")+"\\tesi\\BASW3.clq";
		String graph4 = getFolder("GEPHI")+"\\tesi\\WSSW1.clq";
		String graph5 = getFolder("GEPHI")+"\\tesi\\WSSW2.clq";
		
		List<String> ggg = new ArrayList<String>();
		ggg.add(graph1);
//		ggg.add(graph2);
//		ggg.add(graph3);
//		ggg.add(graph4);
//		ggg.add(graph5);
		
//		comparison(algs, ggg, getFolder("res")+"\\tesi");

		Parser.setParser("dimacs");
//		Parser.setParser("nde");

		//Parser.setParser("ascii");
		alg = "bk.sparse.TomitaXPivot";
//		alg = "bk.sparse.Tomita";
//		alg = "bk.bitsets.TomitaBS";
//		alg = "bk.lists.Eppstein";
//		alg = "bk.lists.EppsteinOrd";
//		graph2 = "C:\\Users\\Alessio\\Dropbox\\ferragina\\graph\\gcc\\gcc-asciiarcs";
//		graph2 = "C:\\Users\\Alessio\\Dropbox\\ferragina\\graph\\administratorEdges-asciiarcs";
//		runAlg(graph1, alg);
		
		SNCAccessPoint.runAlg("C:/graphs/test.blk", alg, -1);
		
//		eppsteinDSTester(alg);
	

//		filteredTest(alg, getFolder("oldenv")+"\\100-60-00");
//		filteredTest(alg, getFolder("GEPHI")+"\\Dimacs\\SWWS-10000-200-250.CSV");
		
//		runAlg("E:\\Darren Strash\\data\\marknewman\\marknewman-internet","BK1BS",-1);
//		runAlg("E:\\Darren Strash\\data\\marknewman\\marknewman-astro","TomitaOrd",-1);
//		runAlg("E:\\Darren Strash\\data\\marknewman\\marknewman-condmat","TomitaOrd",-1);

//		testOldEnv("bk.matrix.Tomita");
//		eppsteinDSTester("bk.matrix.TomitaXPivot");
	
//		testrobertodatasets();
		
	}
	
	public static void testrobertodatasets() throws FileNotFoundException
	{
		System.out.println("MAX MEM: "+Runtime.getRuntime().maxMemory()/1024+" Kbytes");
		
		Parser.setParser("clq");
		String resf = getFolder("res");
		String graphs = getFolder("graphs");
		
		List<String> benchmark = getAllFilesAsString(graphs+"\\BENCHMARK");
		List<String> bhoslib = getAllFilesAsString(graphs+"\\BHOSLIB");
		
		String alg = "bk.matrix.TomitaXPivot";
		List<String> algs = new ArrayList<String> ();
		algs.add(alg);
		
		String outFolder = resf+"\\BENCHMARK";
		
//		System.out.println("BENCHMARK..........");
//		comparison(algs, benchmark, outFolder);
//		System.out.println("done");
		
		outFolder = resf+"\\BHOSLIB";

		System.out.println("BHOSLIB..........");
		comparison(algs, bhoslib, outFolder);
		System.out.println("done");
	}
	
	public static void filteredTest(String alg, String graphfile) throws Exception
	{
		int tlimit = -1;
		int requiredNum = 100;
		int minSize = 0;
		
		
		List<Integer> toVisit = null;
		
		List<Integer> excluded =  ListUtils.emptyList();
		Set<Integer> required = new HashSet<Integer>();

		for(int xx = 0; xx < 102; xx++)
		{
			required.add(xx);
//			required.add(1+(int)(Math.random()*100));
		}
		
		runFilteredAlg(new FileInputStream(graphfile), alg, tlimit, toVisit, excluded, required, requiredNum, minSize);
	}
	
	public static void separatedEppDSComparison(List<String> algs)
	{
		String outparentname = getFolder("res")+"\\Eppstein Data Set";
		
		Map<String, List<String>> graphs = getAllEppsteinDataSetGraphs();

		graphs.remove("pajek");
		graphs.remove("random");
		graphs.remove("snap");
		graphs.remove("biogrid");
		graphs.remove("marknewman");
		
		for(String folderName : graphs.keySet())
		{
			File outfolder = new File(outparentname+"\\"+folderName);
			
			if(!outfolder.exists()) outfolder.mkdirs();
			
//			for(String graph : graphs.get(folderName))
//			{
				try {
//					System.out.println("Running on: "+graph);
					comparison(algs, graphs.get(folderName), outfolder.getAbsolutePath());
				} catch (FileNotFoundException e) {
					System.out.println("UNABLE TO CREATE OUTPUT STREAM FOR: ");
					System.out.println("PATH: "+outfolder.getAbsolutePath());
				}
//			}
		}
	}
	
	/**
	 * Groups for file (1 file: 1 graph -> all the algorithms)
	 * 
	 * @param algs - name of the classes to instantiate
	 * @param graphs - absolute paths of the graph files
	 * @throws FileNotFoundException 
	 */
	public static void comparison(List<String> algs, List<String> graphs, String outFolder) throws FileNotFoundException
	{
		if(algs.isEmpty() || graphs.isEmpty())
		{
			System.out.println("Empty input!!"); return;
		}
		
		File f = new File(outFolder);
		f.mkdirs();
		
		for(int j = 0; j < graphs.size(); j++)
		{
			List<String> gl = new ArrayList<String>();
			gl.add(graphs.get(j));
			
			System.out.println("DOING: "+graphs.get(j));
			
			AlgStats[][] algXgraphStatMatr = generateComparisonMatrix(algs, gl);
			
			PrintWriter out = new PrintWriter(outFolder+"\\"+graphs.get(j).substring(graphs.get(j).lastIndexOf("\\"))+"-comp.txt");
			
			out.println("GRAPH: "+graphs.get(j));
			
			List<AlgStats> al = new ArrayList<AlgStats>(algs.size());
			
			for(int i = 0; i < algs.size(); i++)
			{
				al.add(algXgraphStatMatr[i][0]);
			}
			
			Collections.sort(al);
			
			for(AlgStats s : al)
			{
				out.println(s);
			}
			
			out.close();
		}
		
	}
	
	/**
	 * Groups for algorithm (1 file: 1 algorithm -> all the files)
	 * 
	 * @param algs - name of the classes to instantiate
	 * @param graphs - absolute paths of the graph files
	 * @throws FileNotFoundException 
	 */
	public static void comparison2(List<String> algs, List<String> graphs, String outFolder) throws FileNotFoundException
	{
		if(algs.isEmpty() || graphs.isEmpty())
		{
			System.out.println("Empty input!!"); return;
		}
		
		
		
		
		for(int i = 0; i < algs.size(); i++)
		{
			List<String> al = new ArrayList<String>();
			al.add(algs.get(i));
			
			System.out.println("DOING: "+algs.get(i));
			
			AlgStats[][] algXgraphStatMatr = generateComparisonMatrix(al, graphs);
			
			PrintWriter out = new PrintWriter(outFolder+"\\"+algs.get(i).substring(algs.get(i).lastIndexOf("\\"))+"-comp.txt");
			
			List<AlgStats> gl = new ArrayList<AlgStats>(graphs.size());
			
			for(int j = 0; j < graphs.size(); j++)
			{
				gl.add(algXgraphStatMatr[0][j]);
			}
			
			Collections.sort(al);
			
			for(AlgStats s : gl)
			{
				out.println(s);
			}
			
			out.close();
		}
		
	}

	public static AlgStats[][] generateComparisonMatrix(List<String> algs, List<String> graphs) throws FileNotFoundException
	{

		AlgStats[][] algXstatMatr = new AlgStats[algs.size()][graphs.size()];
		String alg;
		String graph;
		
		for(int i = 0; i < algs.size(); i++)
		{
			alg = algs.get(i);
			for(int j = 0 ; j < graphs.size(); j++)
			{
				graph = graphs.get(j);
				try
				{
					algXstatMatr[i][j] = runAlg(graph, alg);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					algXstatMatr[i][j] = null;
					e.printStackTrace();
				}
			}
		}
		
		return algXstatMatr;
	}

	public static void comparison(List<String> algs, String graph, String outFolder) throws FileNotFoundException
	{
		List<String> l = new ArrayList<String> ();
		l.add(graph);
		
		comparison(algs,l,outFolder);
	}
	
	public static void comparison2(List<String> algs, String graph, String outFolder) throws FileNotFoundException
	{
		List<String> l = new ArrayList<String> ();
		l.add(graph);
		
		comparison2(algs,l,outFolder);
	}
	
	public static PrintStream getComparisonOutputStream(String folder, String filename) throws FileNotFoundException
	{
		String foldername = folder+"\\"+filename;
		
		File result = new File ( foldername + ".txt");
		
		int i = 0;
		while(result.exists())
		{
			result = new File(foldername+"-"+i+".txt");
			i++;
		}
		
		return new PrintStream(result);
	}
	
	public static void splitterTest() throws Exception{
		
		String folder = getFolder("oldenv");
		String fname = folder+"\\graphs.txt";
		
		Scanner s = new Scanner(new File(fname));
		
		while (s.hasNextLine()){
			
			File gfile = new File(folder+fs+s.nextLine());
			
			System.out.println(gfile.getAbsolutePath());
			
			InputStream is = new FileInputStream(gfile);
			
			GraphSplitter gs = new GraphSplitter(is);
			
			gs.start();
			
			gs.printStats();
			if (DEBUG) gs.printSolution();
			
		}
		
	}
	
	public static void testOldEnv(String alg) throws Exception{
		System.out.println("Alg: "+alg);
		String folder = getFolder("oldenv");
		File graphs = new File(folder+fs+"graphs.txt");
		
		Scanner s = new Scanner(graphs);
		
		while(s.hasNextLine()){
			String gname = s.nextLine();
			
			InputStream is = new FileInputStream(new File(folder+fs+gname));
			
			System.out.println("------GRAPH: "+gname+"-------");
			runAlg(is, alg, 10000);
		}
	}
	
	public static void eppsteinDSTester(String alg) throws Exception {
		Parser.setParser("eppstein");
		
		String gfolder = getFolder("eppds");
		
		File folder = new File (gfolder);
		
		Logger.initLog(getFolder("log")+"\\"+alg);
		runRecOnAllFiles(alg, folder, 0, 20000);
		Logger.closeLog();
	}
	
	private static void runRecOnAllFiles(String alg, File folder, int indent, long timeout) {
		for(int i = 0; i<indent; i++){
			Logger.log(">>>");
		}
		Logger.logln(folder.getName());
		
		
		for(String nm : folder.list()){
			File fof = new File(folder.getAbsolutePath()+File.separator+nm);
			
			if(fof.isDirectory()){
				runRecOnAllFiles(alg, fof, indent+1, timeout);
			} else if (Logger.filter(nm) && fof.exists()) {
				try {
					System.out.println("Graph: "+nm);
					AlgStats stats = runAlg(new FileInputStream(fof), alg, timeout);
					
					Logger.logStats(nm, stats, indent);

				} catch (Exception e) {
					System.out.println("Exception on file: "+fof.getAbsolutePath());
					Logger.logln("Exception on file: "+fof.getAbsolutePath(),indent);
					e.printStackTrace();
				}
			} else {
				System.out.println("File \""+nm+"\" Excluded.");
			}
		}
	}

	public static String getAlgClassName(String alg) throws Exception {

			
		if(alg == null || alg.equals("") || alg.equalsIgnoreCase("default")){
					
		
		//			alg = "bk.lists.Tomita";
		//			alg = "bk.bitsets.BK1BS";
					alg = "bk.bitsets.TomitaBS";
		//			alg = "bk.lists.BK1";
		//			alg = "bk.lists.BKPivot";
						
					
			
		} else if(alg.equals("BK1")) alg = "bk.lists.BK1";
		else if(alg.equals("Tomita")) alg = "bk.lists.Tomita";
		else if(alg.equals("BK1Ord")) alg = "bk.lists.BK1Ord";
		else if(alg.equals("TomitaOrd")) alg = "bk.lists.TomitaOrd";
		else if(alg.equals("BK1BS")) alg = "bk.bitsets.BK1BS";
		else if(alg.equals("TomitaBS")) alg = "bk.bitsets.TomitaBS";
		else if(alg.equals("TomitaM")) alg = "bk.matrix.Tomita";
		else if (! alg.contains(".")) alg = "bk.lists."+alg;
		
		return alg;
	}

	public static void testOldCode() throws Exception{
		String folder = getFolder("old env port");
		File graphs = new File(folder+fs+"graphs.txt");
		
		Scanner s = new Scanner(graphs);
		
		// only works with algorithm in package old
		String alg = "old.Tomita";
		
		while(s.hasNextLine()){
			String gname = s.nextLine();
			
			String g = folder+fs+gname;
			
			System.out.println("------GRAPH: "+gname+"-------");
		
			String[] args = {alg,g};
		
			old.MCEnv.main(args);
		}
	}
	
	public static AlgStats runAlg(InputStream graphfile, String alg, long tlimit) throws Exception{

		alg = getAlgClassName(alg);
		
		Class algClass = Class.forName(alg); 
		Constructor c = algClass.getConstructor(InputStream.class);
		SearchAlg sa = (SearchAlg) c.newInstance(graphfile);
		
		if(DEBUG) System.out.println("Algotithm object created, class="+sa.getClass().getName());		
		
		sa.setTimeLimit(tlimit);
		
//		(new AlgLauncher(sa, tlimit)).launch();
		sa.search();
		
		sa.printStats();
		
		return sa.getStats();
	}
	
	public static AlgStats runFilteredAlg(InputStream graphfile, String alg, long tlimit, List<Integer> toVisit, List<Integer> excluded, Set<Integer> required, int requiredNum, int minSize) throws Exception{

		alg = getAlgClassName(alg);
		
		Class algClass = Class.forName(alg); 
		Constructor c = algClass.getConstructor(InputStream.class);
		SearchAlg sa = (SearchAlg) c.newInstance(graphfile);
		
		if(DEBUG) System.out.println("Algotithm object created, class="+sa.getClass().getName());
		
		sa.setTimeLimit(tlimit);

		((BK)sa).setFilters(new HashSet<Integer>(toVisit), new HashSet<Integer>(excluded));
//		(new AlgLauncher(sa, tlimit)).launch();
		sa.search();
		
			
		sa.printStats();
		
		return sa.getStats();
	}
	
	public static AlgStats runAlg(String fname, String alg, long tlimit) throws Exception{
		return runAlg(new FileInputStream(new File(fname)), alg, tlimit);
	}
	
	public static AlgStats runAlg(String fname, String alg) throws Exception{
		return runAlg(new FileInputStream(new File(fname)), alg, -1);
	}
	
	public static AlgStats runAlg(InputStream graph, String alg) throws Exception{
		return runAlg(graph,alg,-1);
	}
	
	// this is supposed to be the "GRAPHS" folder in the same location as the portable eclipse folder
	public static String getGraphsFolder(){
		return getLocalFolder()+"\\GRAPHS";
	}
	
	public static Map<String,List<String>> getAllEppsteinDataSetGraphs()
	{
		String parentname = getFolder("eppds");
		
		Map<String,List<String>> graphs = new HashMap<String,List<String>>();
		
		File parent = new File (parentname);
		
		for( String foldername : parent.list())
		{
			List<String> files = new ArrayList<String>();
			
			File folder = new File (parent.getAbsolutePath()+"\\"+foldername);
			
			for(String graph : folder.list())
			{
				files.add(folder.getAbsolutePath()+"\\"+graph);
			}
			
			graphs.put(foldername, files);
		}
		
		return graphs;
	}
	
	public static List<String> getAllFilesAsString(String parent)
	{
		List<String> l = new ArrayList<String>();
		
		File f = new File(parent);
		
		if(f.exists() && f.isDirectory())
		{
			for(String fname : f.list())
			{
				l.add(parent+"\\"+fname);
			}
		}
		else 
		{
			System.out.println("NOT AN EXISTING FOLDER!!!!");
		}
		
		return l;
	}
	
	public static String getLocalFolder(){
		if(path != null ) return path;
		
		File f = new File("");
		
		path = f.getAbsolutePath();
		path = path.substring(0, path.lastIndexOf("ECLIPSE"));
		
		path = path.substring(0,path.lastIndexOf(fs));
		return path;
	}
	
	public static String getFolder(String id){
		String folder;
		System.out.println("Returning BK ENVIRONMENT folder by default");
		return "C:\\Users\\Alessio\\Dropbox\\BK\\BK ENVIRONMENT";
		
		/*
		if(id.equals("portable")){
			folder = getGraphsFolder();
		} else if(id.equals("oldenv")){
			folder = "C:\\Users\\Alessio\\Documents\\UNI\\TESI-PATRICK\\CODE\\BK ENVIRONMENT";
		} else if(id.equals("graphs")){
			folder = "C:\\Users\\Alessio\\Documents\\UNI\\TESI-PATRICK\\Graphs";
		} else if(id.equals("old env port")){
			folder = getLocalFolder()+"\\CODE\\BK ENVIRONMENT";
		} else if(id.equals("local")){
			folder = getLocalFolder();
		} else if(id.equalsIgnoreCase("GEPHI")){
			folder = "C:\\Users\\Alessio\\Documents\\GEPHI";
		} else if(id.equalsIgnoreCase("res") || id.equalsIgnoreCase("log")){
			folder = "C:\\Users\\Alessio\\Documents\\UNI\\TESI-PATRICK\\Results";
		} else if(id.equalsIgnoreCase("eppsteindataset") || id.equalsIgnoreCase("eppds")){
			folder = "C:\\Users\\Alessio\\Documents\\UNI\\TESI-PATRICK\\Darren Strash\\data";
		} else if(id.equalsIgnoreCase("random") ){
			folder = "C:\\Users\\Alessio\\Documents\\UNI\\TESI-PATRICK\\Graphs\\random";
		} else folder = getLocalFolder();
		
		return folder;
		*/
	}

	public static String getOldEnvGraphsFile(){
		return getFolder("oldenv")+"\\graphs.txt";
	}
	
	public static void test1Pen() throws Exception{
		File g1 = new File(getFolder("portable")+fs+"DIMACS"+fs+"BHOSLIB"+fs+"frb30-15-2.clq"); 
		File g2 = new File(getFolder("portable")+fs+"DIMACS"+fs+"BHOSLIB"+fs+"frb30-15-4.clq"); 
		
		System.out.println(g1);
		System.out.println(g1.exists());

		runAlg(new FileInputStream(g1), "bk.bitsets.TomitaBS",500000);		
	}
}



