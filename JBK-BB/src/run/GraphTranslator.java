package run;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class GraphTranslator {

	public static void main(String[] args) {
		
		String gfolder = Tester.getFolder("oldenv");
		String gfname = Tester.getOldEnvGraphsFile();
		
		Scanner s = null;
		
		try {
			s = new Scanner (new File (gfname));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		while(s.hasNextLine()){
			String graphfname = gfolder + File.separator + s.nextLine();
			
			File graph = new File (graphfname);
			
			String out = graphfname+"-dolfi";
			
				try {
					randomGraph2rdf(new FileInputStream(graph), out);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			
		}
		
	}



	public static void randomGraph2rdf(InputStream is, String outFileName) throws IOException	   {

		File outFile = new File (outFileName);
		
		if(!outFile.exists()){
			System.out.println("CREATING : "+outFile.getAbsolutePath());
		} else {
			System.out.println("OVERWRITING : "+outFile.getAbsolutePath());
		}
		
		PrintWriter out = new PrintWriter(outFile);
		
		
		Scanner s = new Scanner (is);
		String line;
		String[] pline;
		
		while(s.hasNextLine()){
		
			line = s.nextLine();
			
			if(line.length() > 0 && line.startsWith("e")){
				
				//structure: e n1 n2
				//output: n1 e n2
				
				pline = line.split(" ");
				
				String translatedLine = pline[1] + " " + pline[0] + " " + pline[2];
				
				
				out.println(translatedLine);
				//out.flush();
				
			}
			
		}
		
		
			out.close();
			
			System.out.println("Done.");

	}

	
}
