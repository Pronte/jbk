package run;

import java.io.*;
import lib.*;

/**
 *@author Patrick Prosser, modified by Alessio Conte.
 */
public class MCEnv {

    static int[] degree;   // degree of vertices
    static boolean [][] A; // adjacency matrix
    static int n;


    public static void main(String[] args)  throws IOException , Exception{

    InputStream is = new FileInputStream(args[1]);
	
	((SearchAlg) Class.forName(args[0]).getConstructor(InputStream.class).newInstance(is)).search();

	/*
	System.gc();
	if (args.length > 2) mc.timeLimit = 1000 * (long)Integer.parseInt(args[2]);
	long cpuTime = System.currentTimeMillis();
	mc.search();
	cpuTime = System.currentTimeMillis() - cpuTime;
	System.out.println("Max size: "+ mc.maxSize);
	System.out.println("Nodes: "+ mc.nodes);
	System.out.println("Cpu Time: "+ cpuTime);
	for (int i=0;i<mc.n;i++) if (mc.solution[i] == 1) System.out.print(i+1 +" ");
	System.out.println();
	*/
    }
}
