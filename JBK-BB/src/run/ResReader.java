package run;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import javax.sound.midi.SysexMessage;

import lib.AlgStats;

public class ResReader {

	public static int NODES = 0;
	public static int EDGES = 1;
	public static int DEGEN = 2;
	public static int DENS = 3;
	public static int DSTAR = 4;
	
	
	//				0		1		2		3		4		
	//graph id -> Vertices Edges Degeneracy	Density c-Index 
	static List<double[]> gstats;
	static List<Map<Integer,Integer>> g2alg2t;
	static List<String> gnames;
	static List<String> algnames;
	static Map<String,Integer> alg2id;
	static Map<Integer,String> best;
	

	public static void main(String[] args) throws Exception {

		read("C:/graphs/execs");
		
		
		computeTimes();
		
		
		
//		System.out.println("train: "+train);
//		System.out.println("size: "+train.size());
		
//		makeTrainingAndTestSets();
		
	}
	
	public static List<Integer> readds1() throws Exception
	{
		
		String f = "C:/graphs/stats/dataset1.csv";
		
		Scanner s = new Scanner(new File(f));
		
		s.nextLine(); //header
		
		List<Integer> training = new ArrayList<Integer>();
		
		while(s.hasNextLine())
		{
			String l = s.nextLine();
			
			int g = matchGraph(l);
			
			if(g != -1) training.add(g);
			else
			{
				System.out.println("NO MATCH FOR THIS GRAPH:\n"+l);
			}
		}
		
		return training;
	}
	
	public static int matchGraph(String line)
	{
		String[] ln = line.split(",");
		
		
		for(int i = 0; i < gstats.size(); i++)
		{
			boolean match = true;
			
			for(int j = 0; j < gstats.get(i).length; j++)
			{
				if(gstats.get(i)[j] != Double.parseDouble(ln[j])) match = false;
			}
			
			if(match)
			{
				return i;
			}
		}
		
		
		return -1;
		
	}
	
	
	public static void computeTimes() throws Exception
	{
		//best t1 
		int[] sums = new int[4];
		Arrays.fill(sums, 0);
		
		List<Integer> train = readds1();
		Set<Integer> trains = new HashSet<Integer>(train);
		
		
		List<Integer> good = new ArrayList<Integer>();
		
		for(int i = 0; i < gnames.size(); i++)
		{
			if(g2alg2t.get(i).size() > 9 && !trains.contains(i)) good.add(i);
		}
		
		System.out.println("Testing on "+good.size()+" graphs. training: "+train.size());
		
		for(int i : good)
		{
			sums[0] += timeOf(i, best.get(i));
			sums[1] += timeOf(i, predict1(i));
			sums[2] += timeOf(i, predict2(i));
//			sums[3] += timeOf(i, "bk.bitsets.TomitaBS");
//			sums[3] += timeOf(i, "bk.matrix.BKStaticPivot");
		}

		System.out.println("Best: "+sums[0]);
		System.out.println("T1: "+sums[1]);
		System.out.println("T2: "+sums[2]);

		//printSum("bk.bitsets.TomitaBS", good);
//		printSum("bk.matrix.BKStaticPivot", good);
		//printSum("bk.sparse.TomitaXPivot", good);
//		printSum("bk.matrix.TomitaXPivot", good);
		//printSum("bk.sparse.TomitaMG", good);
		//printSum("bk.sparse.Tomita", good);
//		printSum("bk.matrix.Tomita", good);
		
		for(String a : algnames)
		{
			try{
				printSum(a, good);
			} catch (Exception e)
			{
				System.out.println(a+": fail");
			}
		}
		
//		System.out.println(algnames);
	}
	
	public static int printSum(String alg, List<Integer> graphs)
	{
		int sum = 0;
		
		for(int i : graphs)
		{
			sum += timeOf(i, alg);
		}

		System.out.println(alg+": "+sum);
		
		return sum;
	}	
	
	public static int timeOf(int graph, String alg)
	{
		if(alg == "nothing" || g2alg2t.get(graph).isEmpty()){
			System.out.println("graph "+graph+" unsolved.");
			return 0;
		}
		else
		{
			try{
				return g2alg2t.get(graph).get(alg2id.get(alg));
			}
			catch(Exception e)
			{
				System.out.println("Alg: "+alg+" g: "+graph);
//				System.out.println("map null? "+(g2alg2t.get(graph)== null));
//				System.out.println("map "+g2alg2t.get(graph));
				throw new RuntimeException("invalid data request");
			}
		}
	}
	
	public static String predict1(int gid)
	{
		//				0		1		2		3		4		
		//graph id -> Vertices Edges Degeneracy	Density c-Index 
		double[] stats = gstats.get(gid);
		
		
		if(stats[DEGEN] >= 25)
		{
			if(stats[NODES] < 8558)
			{
				if( stats[DEGEN] >= 51.5)
				{
					return "bk.bitsets.TomitaBS";
				}
				else
				{
//					return "bk.matrix.TomitaXPivot";
					return "bk.matrix.BKStaticPivot";
				}
			}
			else
			{
				return "bk.sparse.TomitaXPivot";
			}
		} 
		else
		{
			if(true ||  stats[EDGES] >= 493.5)
			{
				return "bk.matrix.TomitaXPivot";
			}
			else
			{
				return "bjifjir";
//				return "bk.sparse.TomitaMG";
//				return "bk.sparse.Tomita";
			}
		}
	}
	
	
	public static String predict2(int gid)
	{
		//				0		1		2		3		4		
		//graph id -> Vertices Edges Degeneracy	Density c-Index 
		double[] stats = gstats.get(gid);
		
		
		if(stats[DSTAR] > 220)
		{
			return "bk.matrix.BKStaticPivot";
		} 
		else
		{
			if(stats[DEGEN] >= 46)
			{
				return "bk.bitsets.TomitaBS";
			}
			else
			{
				if(stats[EDGES] < 3114)
				{
					return "bk.matrix.TomitaXPivot";
				}
				else
				{
					return "bk.sparse.TomitaXPivot";
				}
			}
		}
	}
	
	
	public static void makeTrainingAndTestSets() throws FileNotFoundException
	{
		int c = 0;
		List<Integer> good = new ArrayList<Integer>();
		
		for(int i = 0; i < g2alg2t.size(); i++)
		{
			if(g2alg2t.get(i).keySet().size() >= 9)
			{
				c++;
				good.add(i);
			}
		}

		System.out.println("----------------SIZE: "+good.size());
		
		List<Integer> training = new ArrayList<Integer>();
		
		//random a portion of 'good' to training
		
		int num = (int) (0.5*good.size());
		
		while(num-- > 0)
		{
			double r = Math.random()*good.size();
			
			training.add(good.remove((int)r));
		}
		
		List<Integer> all = new ArrayList<Integer>(good);
		all.addAll(training);
		
		
		Collections.sort(good);
		Collections.sort(training);
		Collections.sort(all);

		System.out.println("Training GRAPHS: ");
		for(int i : training)
		{
			System.out.print(gnames.get(i)+" ");
		}
		
		System.out.println();
		System.out.println("Testing GRAPHS: ");
		for(int i : good)
		{
			System.out.print(gnames.get(i)+" ");
		}
		System.out.println();
		
		
		System.out.println(good.size()+" "+training.size()+" = all "+all.size());
		
		
//		"Vertex","Edges","Degeneracy","Density","c_Index","Algorithm"
//		10,17,3,0.37777777,4,"bk.matrix.TomitaXPivot"
//		10,31,5,0.68888889,6,"bk.sparse.Tomita"
//		20,171,14,0.90000000,16,"bk.matrix.BKStaticPivot"
		

		String outfolder="C:/graphs/stats/"; 
		
		String outf = outfolder+"dataset.csv";
		
		PrintWriter out = new PrintWriter(outf);
		
		out.println("\"Vertex\",\"Edges\",\"Degeneracy\",\"Density\",\"c_Index\",\"Algorithm\"");
		
		double[] stat;
		
		for(int i : training)
		{
			stat = gstats.get(i);
			out.println(s(stat[0])+","+s(stat[1])+","+s(stat[2])+","+stat[3]+","+s(stat[4])+","+"\""+best.get(i)+"\"");
		}
		
		out.close();
		
		out = new PrintWriter(outfolder+"sets.txt");

		out.print("Test:");
		for(int i : good) out.print(" "+i);
		out.println();
		
		out.print("Training:");
		for(int i : training) out.print(" "+i);
		out.println();
		
		out.close();
		
	}

	private static String s(double d)
	{
		return ""+((int)d);
	}
	
	
	public static int getAlgId(String alg)
	{
		if(!alg2id.containsKey(alg))
		{
			algnames.add(alg);
			alg2id.put(alg, algnames.size()-1);
		}

		return alg2id.get(alg);
	}


	public static void read(String folder) throws Exception
	{
		
		gstats = new ArrayList<double[]>();
		g2alg2t = new ArrayList<Map<Integer,Integer>>();
		gnames = new ArrayList<String>();
		algnames = new ArrayList<String>();
		alg2id = new HashMap<String,Integer>();
		
		File f = new File(folder);


		for(String fname : f.list())
		{
			Scanner s = new Scanner(new File(folder+"/"+fname));

			double[] arr = new double[5];
			gnames.add(fname.substring(0,fname.indexOf(".")));
			gstats.add(arr);
			g2alg2t.add(new HashMap<Integer,Integer>());

			while(s.hasNextLine())
			{
				String l = s.nextLine();

				int index = -1;

				if(l.startsWith("Vertices")) index = 0;
				else if(l.startsWith("Edges")) index = 1;
				else if(l.startsWith("Degeneracy")) index = 2;
				else if(l.startsWith("Density")) index = 3;
				else if(l.startsWith("c-Index")) index = 4;

				if(index != -1)
				{
					arr[index] = Double.parseDouble(l.substring(l.lastIndexOf(" ")+1, l.length()));
				}

				if(index == 4) break;
			}

//			System.out.println("Graph: "+gnames.get(gnames.size()-1));
//			System.out.println("Params: "+Arrays.toString(arr));
			

			s.close();
			s = new Scanner(new File(folder+"/"+fname));
			
			
			Map<Integer,Integer> a2t = g2alg2t.get(g2alg2t.size()-1);
			
			while(s.hasNextLine())
			{
				String l = s.nextLine();

				if(l.startsWith("Alg"))
				{
					String alg = l.substring(l.indexOf(" ")+1);
					int id = getAlgId(alg);
					
					String tl = s.nextLine();
					int time = Integer.parseInt(tl.substring(tl.indexOf(" ")+1));
					
					a2t.put(id, time);
				}
				
				
			}

			s.close();
		}
		
		
		best = new HashMap<Integer,String>();
		
		for(int i=0; i< gnames.size(); i++)
		{
			Map<Integer,Integer> a2t = g2alg2t.get(i);
			
			int min=-1, minv = Integer.MAX_VALUE;
			
			for(int j : a2t.keySet())
			{
				if(a2t.get(j) < minv){
					min = j;
					minv = a2t.get(j);
				}
			}
			if(min == -1) //no alg solved it
			{
				best.put(i,"nothing");
			}
			else
				best.put(i,algnames.get(min));
		}
		
		for(int i = 0; i < g2alg2t.size(); i++)
		{
//			System.out.println("Graph: "+gnames.get(i));
//			System.out.println(g2alg2t.get(i));
		}

	}

}
