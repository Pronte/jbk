package lib;

import lib.graph.IntGraph;
import lib.graph.MatrixGraph;
import lib.graph.SparseBKGraph;

import java.util.*;

public class VertexCoverCalculator {

	static int maxdegree;
	static int maxlabel;
	static IntGraph g;
	private static SparseBKGraph sg;

	static int[] degreeOrd;
	static int[] position;
	static int[] degree;

	public static Set<Integer> vertexCover(IntGraph g) {
		
		if(g instanceof MatrixGraph)
		{
			return vertexCoverMG((MatrixGraph)g);
		}

		VertexCoverCalculator.g = g;
		List<Integer> nodes = new ArrayList<Integer>(g.vertices());

		List<Integer> indSet = new ArrayList<Integer>();

		Set<Integer> deleted = new HashSet<Integer>();

		int x;

		while(!nodes.isEmpty())
		{
			x = nodes.remove( (int)(Math.random()*nodes.size()));
			deleted.add(x);
			indSet.add(x);

			for(int nn : g.neighbors(x))
			{
				if(deleted.add(nn))
				{
					nodes.remove((Integer)nn);
				}
			}
		}

		if(System.getProperty("DEBUG") != null) System.out.println("IndSet size: "+indSet.size());

		//at this point deleted contains all the nodes, so if we remove the indSet from it, we get a vertex cover.
		deleted.removeAll(indSet);

		return deleted;
	}
	public static Set<Integer> vertexCoverMG(MatrixGraph g) {

		VertexCoverCalculator.g = g;
		List<Integer> nodes = new ArrayList<Integer>(g.vertices());

		List<Integer> indSet = new ArrayList<Integer>();

		Set<Integer> deleted = new HashSet<Integer>();

		int x;

		while(!nodes.isEmpty())
		{
			x = nodes.remove( (int)(Math.random()*nodes.size()));
			deleted.add(x);
			indSet.add(x);

			boolean[] neighs = g.neighborsM(x);
			
			for(int i = 0; i < neighs.length; i++)
			{	
				if( neighs[i] && deleted.add(i))
				{
					nodes.remove((Integer)i);
				}
			}
		}

		if(System.getProperty("DEBUG") != null) System.out.println("IndSet size: "+indSet.size());

		//at this point deleted contains all the nodes, so if we remove the indSet from it, we get a vertex cover.
		deleted.removeAll(indSet);

		return deleted;
	}

}
