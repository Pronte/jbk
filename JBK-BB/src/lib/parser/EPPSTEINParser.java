package lib.parser;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import lib.graph.Graph;
import lib.graph.IntGraph;
import lib.graph.MapListGraph;
import lib.graph.MatrixGraph;
import lib.graph.SparseBKDoubleGraph;
import lib.graph.SparseBKGraph;
import lib.graph.SparseMatrixListGraph;
import lib.graph.bs.BSIntGraph;
import lib.graph.bs.IBSIntGraph;

public class EPPSTEINParser implements ParserImpl {
	
	boolean DEBUG = System.getProperty("DEBUG") == null;

	@Override
	public  IntGraph getIntGraph(InputStream is){
		
		MapListGraph graph = new MapListGraph(this.getMapListGraph(is));
		graph.orderGraph();

		return graph;
	}

	@Override
	public  BSIntGraph getBitSetIntGraph(InputStream is){
		
		Scanner s = new Scanner(is);
		
		BSIntGraph graph = new BSIntGraph();
		
		String header = s.nextLine();
				header = header+" "+s.nextLine(); //header has 2 lines
		String[] line;
		
		int a, b;
		
		System.out.println("[epp]Parsing: "+header);
		
		while(s.hasNextLine()){
			try{
				line = s.nextLine().split(",");
				
				a = Integer.parseInt(line[0]);
				b = Integer.parseInt(line[1]);
				
				if(a!=b) graph.setNeighbors(a,b);
			} catch (Exception e){
				
			}
		}
		if(DEBUG) System.out.println("parsed graph: "+graph);
		return graph;
	}

	@Override
	public  Map<Integer,List<Integer>> getMapListGraph(InputStream is){
		
		Scanner s = new Scanner(is);
		
		
		Map<Integer,List<Integer>> graph = new HashMap<Integer,List<Integer>>();

		String header = s.nextLine();
				header = header+" "+s.nextLine(); //header has 2 lines
		String[] line;
		
		int a, b;
		
		System.out.println("[epp]Parsing: "+header);
		
		while(s.hasNextLine()){
			try{
				line = s.nextLine().split(",");
	
				a = Integer.parseInt(line[0]);
				b = Integer.parseInt(line[1]);
	
				if(graph.get(a) == null){
					graph.put(a, new ArrayList<Integer>());
				}
				if(graph.get(b) == null){
					graph.put(b, new ArrayList<Integer>());
				}
				
				//adding the edge in both directions, avoiding duplicates and loops.
				if(a!=b && !graph.get(a).contains(b)){
					graph.get(a).add(b);
					graph.get(b).add(a);
				}
			} catch (Exception e){
				
			}
		}
		
		return graph;
	}
	@Override
	public IBSIntGraph getIBitSetIntGraph(InputStream is){

		Scanner s = new Scanner(is);
		
		IBSIntGraph graph = new IBSIntGraph();

		String header = s.nextLine();
				header = header+" "+s.nextLine(); //header has 2 lines
		String[] line;
		
		int a, b;

		System.out.println("[epp]Parsing: "+header);
		
		while(s.hasNextLine()){
			try{
				line = s.nextLine().split(",");
				
				a = Integer.parseInt(line[0]);
				b = Integer.parseInt(line[1]);
	
				graph.setNeighbors(a,b);
			} catch (Exception e){
				
			}
		}
		
		return graph;
	}


	@Override
	public SparseBKGraph getSparseGraph(InputStream is) {
		return new SparseBKGraph(getMapListGraph(is));
	}

	@Override
	public MatrixGraph getMatrixGraph(InputStream is) {
		Scanner s = new Scanner(is);

		String header = s.nextLine();
		
		int n = Integer.parseInt(header.split(" ")[0]);
		
		header = header+" "+s.nextLine(); //header has 2 lines
		
		boolean[][] matrix = new boolean[n][n];
		
		for(int i=0; i<n; i++){
			for(int j=0; j<n; j++){
				matrix[i][j] = false;
			}
		}
		
		String[] line;
		
		int a, b;

		System.out.println("[epp]Parsing: "+header);
		
		while(s.hasNextLine()){
			try{
				line = s.nextLine().split(",");
				
				a = Integer.parseInt(line[0]);
				b = Integer.parseInt(line[1]);
	
				if(a != b)
				{
					matrix[a][b] = true;
					matrix[b][a] = true;
				}
			} catch (Exception e){
				
			}	
		}

		MatrixGraph graph = new MatrixGraph(matrix);
		
		return graph;
	}

	@Override
	public SparseBKDoubleGraph getSparseDoubleGraph(InputStream is) {
		return new SparseBKDoubleGraph(this.getMapListGraph(is));
	}

	@Override
	public SparseMatrixListGraph getSparseMatrixListGraph(InputStream is) {
		return new SparseMatrixListGraph(this.getMapListGraph(is));
	}

}
