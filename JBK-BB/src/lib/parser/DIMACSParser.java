package lib.parser;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import lib.graph.Graph;
import lib.graph.IntGraph;
import lib.graph.MapListGraph;
import lib.graph.MatrixGraph;
import lib.graph.SparseBKDoubleGraph;
import lib.graph.SparseBKGraph;
import lib.graph.SparseMatrixListGraph;
import lib.graph.bs.BSIntGraph;
import lib.graph.bs.IBSIntGraph;

public class DIMACSParser implements ParserImpl {

	boolean DEBUG = System.getProperty("DEBUG") == null;
	
	@Override
	public  IntGraph getIntGraph(InputStream is){
		
		MapListGraph graph = new MapListGraph(this.getMapListGraph(is));
		graph.orderGraph();
		
		return graph;
	}

	@Override
	public  BSIntGraph getBitSetIntGraph(InputStream is){
		
		Scanner s = new Scanner(is);
		
		BSIntGraph graph = new BSIntGraph();
		
		String header = s.nextLine();
		String[] line;
		
		int a, b;
		
		System.out.println("Parsing: "+header);
		
		while(s.hasNextLine()){
			try{
				line = s.nextLine().split(" ");
				
				//line[0] is the letter e (for edge)
				//there's an edge between line[1], and line[2]
	
				a = Integer.parseInt(line[1]);
				b = Integer.parseInt(line[2]);
	
				if(a != b) graph.setNeighbors(a,b);
			} catch (Exception e){
				
			}
		}
		
		return graph;
	}
	
	@Override
	public  Map<Integer,List<Integer>> getMapListGraph(InputStream is){
		
		Scanner s = new Scanner(is);
		
		
		Map<Integer,List<Integer>> graph = new HashMap<Integer,List<Integer>>();
		
		String header = s.nextLine();
		String[] line;
		
		int a, b;
		
		System.out.println("Parsing: "+header);
		
		while(s.hasNextLine()){
			try{
				line = s.nextLine().split(" ");
				
				
				//line[0] is the letter e (for edge)
				//there's an edge between line[1], and line[2]
	
				a = Integer.parseInt(line[1]);
				b = Integer.parseInt(line[2]);
	
				if(graph.get(a) == null){
					graph.put(a, new ArrayList<Integer>());
				}
				if(graph.get(b) == null){
					graph.put(b, new ArrayList<Integer>());
				}
	
				//adding the edge in both directions, avoiding duplicates and loops.
				if(a!=b && !graph.get(a).contains(b)){
					graph.get(a).add(b);
					graph.get(b).add(a);
				}	
			} catch (Exception e){
				
			}	
		}
		
		return graph;
	}
	
	@Override
	public IBSIntGraph getIBitSetIntGraph(InputStream is){

		Scanner s = new Scanner(is);
		
		IBSIntGraph graph = new IBSIntGraph();
		
		String header = s.nextLine();
		String[] line;
		
		int a, b;
		
		System.out.println("Parsing: "+header);
		
		while(s.hasNextLine()){
			try{
				line = s.nextLine().split(" ");
				
				//line[0] is the letter e (for edge)
				//there's an edge between line[1], and line[2]
	
				a = Integer.parseInt(line[1]);
				b = Integer.parseInt(line[2]);
	
				graph.setNeighbors(a,b);
			} catch (Exception e){
				
			}
		}
		
		return graph;
	}

	@Override
	public SparseBKGraph getSparseGraph(InputStream is) {
		return new SparseBKGraph(getMapListGraph(is));
	}

	@Override
	public MatrixGraph getMatrixGraph(InputStream is) {
		Scanner s = new Scanner(is);

		String header = s.nextLine();
		
		System.out.println("Header: "+header);
		
		String ns = header.split(" ")[2]; // header: "p #n #m"
		
		int n = Integer.parseInt(ns);
		
		MatrixGraph g = new MatrixGraph(n+1);
		
		
		String[] line;
		
		int a = -1, b = -1;
		
		while(s.hasNextLine()){
			try{
				line = s.nextLine().split(" ");
				
				//line[0] is the letter e (for edge)
				//there's an edge between line[1], and line[2]
	
				a = Integer.parseInt(line[1]);
				b = Integer.parseInt(line[2]);
	
				g.setNeighbors(a, b);
				
			} catch (Exception e){
				System.out.println("woops! could not add the edge ("+a+","+b+",)");
			}
		}
		
		return g;
	}

	@Override
	public SparseBKDoubleGraph getSparseDoubleGraph(InputStream is) {
		return new SparseBKDoubleGraph(this.getMapListGraph(is));
	}

	@Override
	public SparseMatrixListGraph getSparseMatrixListGraph(InputStream is) {
		return new SparseMatrixListGraph(this.getMapListGraph(is));
	}

}
