package lib.parser;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import lib.graph.Graph;
import lib.graph.IntGraph;
import lib.graph.MatrixGraph;
import lib.graph.SparseBKDoubleGraph;
import lib.graph.SparseBKGraph;
import lib.graph.SparseMatrixListGraph;
import lib.graph.bs.BSIntGraph;
import lib.graph.bs.IBSIntGraph;

public class Parser {
	
		public static boolean DEBUG = (System.getProperty("DEBUG") != null);
	
		private static String parser_nm = "dimacs";

		private static ParserImpl parser = null;

		public static ParserImpl getParser(){
				if(parser_nm.equals("eppstein")){
					if(DEBUG) System.out.println("Parsing: eppstein format");
					parser = new EPPSTEINParser();
				} else if(parser_nm.equalsIgnoreCase("snc")){
					if(DEBUG) System.out.println("Parsing: SocialNetworksCliques binary format");
					parser = new SNCParser();
				} else if(parser_nm.equalsIgnoreCase("clq")){
					if(DEBUG) System.out.println("Parsing: .clq");
					parser = new CLQParser();
				} else if(parser_nm.equalsIgnoreCase("ascii")){
					if(DEBUG) System.out.println("Parsing: asciiarcs");
					parser = new ASCIIARCSParser();
				} else if(parser_nm.equalsIgnoreCase("nde")){
					if(DEBUG) System.out.println("Parsing: nde");
					parser = new NDEParser();
				} else {
					if(DEBUG) System.out.println("Parsing: dimacs format");
					parser = new DIMACSParser();
				}
			return parser;
		}
		
		public static void setParser(String nm){
			if(nm.equalsIgnoreCase("default")) parser_nm = "dimacs";
			parser_nm = nm;
		}
		
		public static IntGraph getIntGraph(InputStream is){
			return getParser().getIntGraph(is);
		}

		public static BSIntGraph getBitSetIntGraph(InputStream is){
			if(DEBUG) System.out.println("Getting the graph..");
			return getParser().getBitSetIntGraph(is);
		}

		public static Map<Integer,List<Integer>> getMapListGraph(InputStream is){
			return getParser().getMapListGraph(is);			
		}
		
		public static IBSIntGraph getIBitSetIntGraph(InputStream is) {
			
			return getParser().getIBitSetIntGraph(is);
		}

		public static SparseBKGraph getSparseGraph(InputStream is) {
			return getParser().getSparseGraph(is);
		}
		public static SparseBKDoubleGraph getSparseDoubleGraph(InputStream is) {
			return getParser().getSparseDoubleGraph(is);
		}
		
		public static MatrixGraph getMatrixGraph(InputStream is) {
			return getParser().getMatrixGraph(is);
		}

		public static SparseMatrixListGraph getSparseMatrixListGraph(InputStream is) {
			return getParser().getSparseMatrixListGraph(is);
		}
		
		

}
