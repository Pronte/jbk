package lib.parser;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lib.graph.Graph;
import lib.graph.IntGraph;
import lib.graph.MatrixGraph;
import lib.graph.SparseBKDoubleGraph;
import lib.graph.SparseBKGraph;
import lib.graph.SparseMatrixListGraph;
import lib.graph.bs.BSIntGraph;
import lib.graph.bs.IBSIntGraph;

public class SNCParser implements ParserImpl {

	private ByteBuffer buff;
	
	@Override
	public IntGraph getIntGraph(InputStream is) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BSIntGraph getBitSetIntGraph(InputStream is) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Integer, List<Integer>> getMapListGraph(InputStream is) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IBSIntGraph getIBitSetIntGraph(InputStream is) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SparseBKGraph getSparseGraph(InputStream is) {
		
		System.out.println("Getting binary sparse graph");
		DataInputStream dis = new DataInputStream((is));
		
		int sizeofUInt = 4, sizeOfChar = 1;
		int numberOfNodesInKernel, numberOfNodesInBorderVis, maxLabel, counter = 0;
		int collectionSize, stringElementLength, elementId, nodeOfCollection;
		
		Map<Integer, Integer> mappingNodesGlobal2Local = new HashMap<Integer, Integer>(), mappingPredicatesGlobal2local = new HashMap<Integer, Integer>();
		Map<Integer, String> nodesMapping = new HashMap<Integer,String>();
		Map<Integer, String> predicatesMapping = new HashMap<Integer,String>();
		
		List<Integer> kernel = new ArrayList<Integer>(), borderVis = new ArrayList<Integer>();
		
		buff = ByteBuffer.allocate(sizeofUInt);
		byte[] elBuff;
		String elName;
		Charset charset = Charset.forName("UTF-8");
		
		try {
			collectionSize = readInt(dis); //read nodes mapping
			
			for(int i = 0; i < collectionSize; i++)
			{
				elementId = readInt(dis);
				
				mappingNodesGlobal2Local.put(elementId, counter);

				stringElementLength = readInt(dis);
				
				elBuff = new byte[stringElementLength * sizeOfChar];
				
				dis.read(elBuff);
				elName = new String(elBuff, charset);
				elBuff = null;
				elName.substring(0, stringElementLength); //?
				
				nodesMapping.put(counter, elName);
				counter++;
			}
			
			maxLabel = counter - 1;
			counter = 0;
			
			collectionSize = readInt(dis); //read predicates mapping
			
			for(int i = 0; i < collectionSize; i++)
			{
				elementId = readInt(dis);
				
				mappingPredicatesGlobal2local.put(elementId, counter);
				
				stringElementLength = readInt(dis);
				
				elBuff = new byte[stringElementLength];
				
				dis.read(elBuff);
				elName = new String(elBuff, charset);
				elBuff = null;
				elName.substring(0, stringElementLength); //?
				
				predicatesMapping.put(counter, elName);
				counter++;
			}
			
			//read kernel
			numberOfNodesInKernel = readInt(dis);
			
			for(int i = 0; i < numberOfNodesInKernel; i++)
			{
				nodeOfCollection = readInt(dis);
				
				kernel.add(mappingNodesGlobal2Local.get(nodeOfCollection) );
			}
			
			//read border vis
			numberOfNodesInBorderVis = readInt(dis);
			
			for(int i = 0; i < numberOfNodesInBorderVis; i++)
			{
				nodeOfCollection = readInt(dis);
				
				borderVis.add(mappingNodesGlobal2Local.get(nodeOfCollection));
			}
			
			//read number of triples
			int numberOfTriples = readInt(dis);
			
			//init structure
			
			int subject, predicate, object;
			List<Set<Integer>> neighbors = new ArrayList<Set<Integer>>(maxLabel+1);
			
			for(int i = 0; i <= maxLabel; i++)
			{
				neighbors.set(i, new HashSet<Integer>());
			}
			
			//fill structure
			ByteBuffer buff3int = ByteBuffer.allocate(3*sizeofUInt);
			for(int i = 0; i < numberOfTriples; i++)
			{											// OBJ     PRED   SUBJ     
				dis.read(buff3int.array());  //array:    X X X X Y Y Y Y Z Z Z Z  in big endian.
														// SUBJ    PRED   OBJ    
				flipArray(buff3int.array()); //now:		 Z Z Z Z Y Y Y Y X X X X  in little endian.
				
				subject = buff3int.getInt(sizeofUInt*2);
				predicate = buff3int.getInt(sizeofUInt);
				object = buff3int.getInt(0);
				
				//write sucessors
				neighbors.get(mappingNodesGlobal2Local.get(subject)).add(mappingNodesGlobal2Local.get(object));
			}
			
			dis.close();
			
			int[][] neighborsVecs = new int[maxLabel+1][];
			
			for(int i = 0; i <= maxLabel ; i++)
			{
				neighborsVecs[i] = new int[neighbors.get(i).size()];
				int count = 0;
				for(int j : neighbors.get(i))
				{
					neighborsVecs[i][count] = j;
					count++;
				}
			}
			return new SparseBKGraph(neighborsVecs);
			
			
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Failure while creating the graph.");
			System.exit(1);
			return null;
		}
	}


	@Override
	public SparseBKDoubleGraph getSparseDoubleGraph(InputStream is) {
		
		System.out.println("Getting binary sparse graph");
		DataInputStream dis = new DataInputStream((is));
		
		int sizeofUInt = 4, sizeOfChar = 1;
		int numberOfNodesInKernel, numberOfNodesInBorderVis, maxLabel, counter = 0;
		int collectionSize, stringElementLength, elementId, nodeOfCollection;
		
		Map<Integer, Integer> mappingNodesGlobal2Local = new HashMap<Integer, Integer>(), mappingPredicatesGlobal2local = new HashMap<Integer, Integer>();
		Map<Integer, String> nodesMapping = new HashMap<Integer,String>();
		Map<Integer, String> predicatesMapping = new HashMap<Integer,String>();
		
		List<Integer> kernel = new ArrayList<Integer>(), borderVis = new ArrayList<Integer>();
		
		buff = ByteBuffer.allocate(sizeofUInt);
		byte[] elBuff;
		String elName;
		Charset charset = Charset.forName("UTF-8");
		
		try {
			collectionSize = readInt(dis); //read nodes mapping
			
			for(int i = 0; i < collectionSize; i++)
			{
				elementId = readInt(dis);
				
				mappingNodesGlobal2Local.put(elementId, counter);

				stringElementLength = readInt(dis);
				
				elBuff = new byte[stringElementLength * sizeOfChar];
				
				dis.read(elBuff);
				elName = new String(elBuff, charset);
				elBuff = null;
				elName.substring(0, stringElementLength); //?
				
				nodesMapping.put(counter, elName);
				counter++;
			}
			
			maxLabel = counter - 1;
			counter = 0;
			
			collectionSize = readInt(dis); //read predicates mapping
			
			for(int i = 0; i < collectionSize; i++)
			{
				elementId = readInt(dis);
				
				mappingPredicatesGlobal2local.put(elementId, counter);
				
				stringElementLength = readInt(dis);
				
				elBuff = new byte[stringElementLength];
				
				dis.read(elBuff);
				elName = new String(elBuff, charset);
				elBuff = null;
				elName.substring(0, stringElementLength); //?
				
				predicatesMapping.put(counter, elName);
				counter++;
			}
			
			//read kernel
			numberOfNodesInKernel = readInt(dis);
			
			for(int i = 0; i < numberOfNodesInKernel; i++)
			{
				nodeOfCollection = readInt(dis);
				
				kernel.add(mappingNodesGlobal2Local.get(nodeOfCollection) );
			}
			
			//read border vis
			numberOfNodesInBorderVis = readInt(dis);
			
			for(int i = 0; i < numberOfNodesInBorderVis; i++)
			{
				nodeOfCollection = readInt(dis);
				
				borderVis.add(mappingNodesGlobal2Local.get(nodeOfCollection));
			}
			
			//read number of triples
			int numberOfTriples = readInt(dis);
			
			//init structure
			
			int subject, predicate, object;
			List<Set<Integer>> neighbors = new ArrayList<Set<Integer>>(maxLabel+1);
			
			for(int i = 0; i <= maxLabel; i++)
			{
				neighbors.set(i, new HashSet<Integer>());
			}
			
			//fill structure
			ByteBuffer buff3int = ByteBuffer.allocate(3*sizeofUInt);
			for(int i = 0; i < numberOfTriples; i++)
			{											// OBJ     PRED   SUBJ     
				dis.read(buff3int.array());  //array:    X X X X Y Y Y Y Z Z Z Z  in big endian.
														// SUBJ    PRED   OBJ    
				flipArray(buff3int.array()); //now:		 Z Z Z Z Y Y Y Y X X X X  in little endian.
				
				subject = buff3int.getInt(sizeofUInt*2);
				predicate = buff3int.getInt(sizeofUInt);
				object = buff3int.getInt(0);
				
				//write sucessors
				neighbors.get(mappingNodesGlobal2Local.get(subject)).add(mappingNodesGlobal2Local.get(object));
			}
			
			dis.close();
			
			int[][] neighborsVecs = new int[maxLabel+1][];
			
			for(int i = 0; i <= maxLabel ; i++)
			{
				neighborsVecs[i] = new int[neighbors.get(i).size()];
				int count = 0;
				for(int j : neighbors.get(i))
				{
					neighborsVecs[i][count] = j;
					count++;
				}
			}
			return new SparseBKDoubleGraph(neighborsVecs,neighbors);
			
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
			return null;
		}
	}
	
	@Override
	public MatrixGraph getMatrixGraph(InputStream is) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void flipArray(byte[] array)
	{
		byte temp;
		int i, length = array.length;
		for (i = 0; i < length/2; i++)
		{
			temp = array[i];
			array[i] = array[length - 1 - i];
			array[length - 1 - i] = temp;
		}
	}
	private int readInt(DataInputStream dis) throws IOException
	{
		dis.read(buff.array());
		flipArray(buff.array());
		return buff.getInt(0);
	}
	private int readStraightInt(DataInputStream dis) throws IOException
	{
		dis.read(buff.array());
//		flipArray(buff.array());
		return buff.getInt(0);
	}

	@Override
	public SparseMatrixListGraph getSparseMatrixListGraph(InputStream is) {
		
		System.out.println("Getting binary sparse graph");
		DataInputStream dis = new DataInputStream((is));
		
		int sizeofUInt = 4, sizeOfChar = 1;
		int numberOfNodesInKernel, numberOfNodesInBorderVis, maxLabel, counter = 0;
		int collectionSize, stringElementLength, elementId, nodeOfCollection;
		
		Map<Integer, Integer> mappingNodesGlobal2Local = new HashMap<Integer, Integer>(), mappingPredicatesGlobal2local = new HashMap<Integer, Integer>();
		Map<Integer, String> nodesMapping = new HashMap<Integer,String>();
		Map<Integer, String> predicatesMapping = new HashMap<Integer,String>();
		
		List<Integer> kernel = new ArrayList<Integer>(), borderVis = new ArrayList<Integer>();
		
		buff = ByteBuffer.allocate(sizeofUInt);
		byte[] elBuff;
		String elName;
		Charset charset = Charset.forName("UTF-8");
		
		try {
			collectionSize = readInt(dis); //read nodes mapping
			
			for(int i = 0; i < collectionSize; i++)
			{
				elementId = readInt(dis);
				
				mappingNodesGlobal2Local.put(elementId, counter);

				stringElementLength = readInt(dis);
				
				elBuff = new byte[stringElementLength * sizeOfChar];
				
				dis.read(elBuff);
				elName = new String(elBuff, charset);
				elBuff = null;
				elName.substring(0, stringElementLength); //?
				
				nodesMapping.put(counter, elName);
				counter++;
			}
			
			maxLabel = counter - 1;
			counter = 0;
			
			collectionSize = readInt(dis); //read predicates mapping
			
			for(int i = 0; i < collectionSize; i++)
			{
				elementId = readInt(dis);
				
				mappingPredicatesGlobal2local.put(elementId, counter);
				
				stringElementLength = readInt(dis);
				
				elBuff = new byte[stringElementLength];
				
				dis.read(elBuff);
				elName = new String(elBuff, charset);
				elBuff = null;
				elName.substring(0, stringElementLength); //?
				
				predicatesMapping.put(counter, elName);
				counter++;
			}
			
			//read kernel
			numberOfNodesInKernel = readInt(dis);
			
			for(int i = 0; i < numberOfNodesInKernel; i++)
			{
				nodeOfCollection = readInt(dis);
				
				kernel.add(mappingNodesGlobal2Local.get(nodeOfCollection) );
			}
			
			//read border vis
			numberOfNodesInBorderVis = readInt(dis);
			
			for(int i = 0; i < numberOfNodesInBorderVis; i++)
			{
				nodeOfCollection = readInt(dis);
				
				borderVis.add(mappingNodesGlobal2Local.get(nodeOfCollection));
			}
			
			//read number of triples
			int numberOfTriples = readInt(dis);
			
			//init structure
			
			int subject, predicate, object;
			List<Set<Integer>> neighbors = new ArrayList<Set<Integer>>(maxLabel+1);
			
			for(int i = 0; i <= maxLabel; i++)
			{
				neighbors.set(i, new HashSet<Integer>());
			}
			
			//fill structure
			ByteBuffer buff3int = ByteBuffer.allocate(3*sizeofUInt);
			for(int i = 0; i < numberOfTriples; i++)
			{											// OBJ     PRED   SUBJ     
				dis.read(buff3int.array());  //array:    X X X X Y Y Y Y Z Z Z Z  in big endian.
														// SUBJ    PRED   OBJ    
				flipArray(buff3int.array()); //now:		 Z Z Z Z Y Y Y Y X X X X  in little endian.
				
				subject = buff3int.getInt(sizeofUInt*2);
				predicate = buff3int.getInt(sizeofUInt);
				object = buff3int.getInt(0);
				
				//write sucessors
				neighbors.get(mappingNodesGlobal2Local.get(subject)).add(mappingNodesGlobal2Local.get(object));
			}
			
			dis.close();
			
			int[][] neighborsVecs = new int[maxLabel+1][];
			
			for(int i = 0; i <= maxLabel ; i++)
			{
				neighborsVecs[i] = new int[neighbors.get(i).size()];
				int count = 0;
				for(int j : neighbors.get(i))
				{
					neighborsVecs[i][count] = j;
					count++;
				}
			}
			return new SparseMatrixListGraph(neighborsVecs,neighbors);
			
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
			return null;
		}
	}
	

	boolean DEBUG = false;

	public Map<String,Object> getBlockInfo(InputStream is) {
		
		System.out.println("Getting binary sparse graph");
		DataInputStream dis = new DataInputStream((is));
		
		int sizeofUInt = 4, sizeOfChar = 1;
		int numberOfNodesInKernel, numberOfNodesInBorderVis, numberOfNodesBlackList, maxLabel, counter = 0;
		int collectionSize, stringElementLength, elementId, nodeOfCollection;
		
		Map<Integer, Integer> mappingNodesGlobal2Local = new HashMap<Integer, Integer>(), mappingPredicatesGlobal2local = new HashMap<Integer, Integer>();
		Map<Integer, String> nodesMapping = new HashMap<Integer,String>();
		Map<Integer, String> predicatesMapping = new HashMap<Integer,String>();
		
		List<Integer> kernel = new ArrayList<Integer>(), borderVis = new ArrayList<Integer>();
		
		buff = ByteBuffer.allocate(sizeofUInt);
		byte[] elBuff;
		String elName;
		Charset charset = Charset.forName("UTF-8");
		boolean slow = false;
		try {
			collectionSize = readInt(dis); //read nodes mapping
			if(DEBUG) System.out.println(collectionSize);
			
			for(int i = 0; i < collectionSize; i++)
			{
				elementId = readInt(dis);
				if(DEBUG) System.out.println(elementId);
				
				mappingNodesGlobal2Local.put(elementId, counter);

				stringElementLength = readInt(dis);
				
				elBuff = new byte[stringElementLength * sizeOfChar];
				
				dis.read(elBuff);
				elName = new String(elBuff, charset);
				elBuff = null;
				if(DEBUG) System.out.println("elname: "+elName+" length: "+stringElementLength+" "+elName.length());
//				elName.substring(0, stringElementLength); //?

				
				nodesMapping.put(counter, elName);
				counter++;
			}

			maxLabel = counter - 1;
			counter = 0;
			
			collectionSize = readInt(dis); //read predicates mapping
//			System.out.println("COLL SIZE "+collectionSize);
			for(int i = 0; i < collectionSize; i++)
			{
				elementId = readInt(dis);
				
				mappingPredicatesGlobal2local.put(elementId, counter);
				
				stringElementLength = readInt(dis);
				
				elBuff = new byte[stringElementLength];
				
				dis.read(elBuff);
				elName = new String(elBuff, charset);
				elBuff = null;
//				elName.substring(0, stringElementLength); //?

				if(DEBUG) System.out.println("elname: "+elName+" length: "+stringElementLength+" "+elName.length());


				
				predicatesMapping.put(counter, elName);
				counter++;
				
			}
			
			//read kernel
			numberOfNodesInKernel = readInt(dis);
//			System.out.println("COLL SIZE "+numberOfNodesInKernel);
			for(int i = 0; i < numberOfNodesInKernel; i++)
			{
				nodeOfCollection = readInt(dis);
				
				kernel.add(mappingNodesGlobal2Local.get(nodeOfCollection) );
			}
			
			//read border vis
			numberOfNodesInBorderVis = readInt(dis);
//			System.out.println("COLL SIZE "+numberOfNodesInBorderVis);
			for(int i = 0; i < numberOfNodesInBorderVis; i++)
			{
				nodeOfCollection = readInt(dis);
				
				borderVis.add(mappingNodesGlobal2Local.get(nodeOfCollection));
			}			
			
			//read border blackList (and dump it, it's just like the normal border)
			numberOfNodesBlackList = readInt(dis);
//			System.out.println("COLL SIZE "+numberOfNodesBlackList);
			for(int i = 0; i < numberOfNodesBlackList; i++)
			{
				nodeOfCollection = readInt(dis);
			}
			
			//read number of triples
			int numberOfTriples = readInt(dis);
//			System.out.println("TRIPLES "+numberOfTriples);
			//init structure
			
			int subject, predicate, object;
			List<Set<Integer>> neighbors = new ArrayList<Set<Integer>>(maxLabel+1);
			
			for(int i = 0; i <= maxLabel; i++)
			{
				neighbors.add(new HashSet<Integer>());
			}
//			System.out.println(maxLabel);
			//fill structure
			ByteBuffer buff3int = ByteBuffer.allocate(3*sizeofUInt);
			for(int i = 0; i < numberOfTriples/3; i++)
			{											// OBJ     PRED   SUBJ     
				dis.read(buff3int.array());  //array:    X X X X Y Y Y Y Z Z Z Z  in big endian.
														// SUBJ    PRED   OBJ    
				flipArray(buff3int.array()); //now:		 Z Z Z Z Y Y Y Y X X X X  in little endian.
				
				subject = buff3int.getInt(sizeofUInt*2);
				predicate = buff3int.getInt(sizeofUInt);
				object = buff3int.getInt(0);
			//System.out.println(subject+" --> "+predicate+" --> "+object);
				//write sucessors
				mappingNodesGlobal2Local.get(subject);
				int sub = mappingNodesGlobal2Local.get(subject);
				int ob = mappingNodesGlobal2Local.get(object);
				
				if(sub != ob) //LOOPS are unwanted
					neighbors.get(sub).add(ob);
			}
			
			dis.close();

			
			Map<String,Object> res = new HashMap<String,Object>();
			
			res.put("maxlabel", ""+maxLabel);
			res.put("neighs", neighbors);
			res.put("kernel", new HashSet<Integer>(kernel));
			res.put("bordervis", new HashSet<Integer>(borderVis));
			
			return res;
			
			
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Failure while creating the graph.");
			System.exit(1);
			return null;
		}
	}


}
