package lib.parser;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import lib.graph.Graph;
import lib.graph.IntGraph;
import lib.graph.MatrixGraph;
import lib.graph.SparseBKDoubleGraph;
import lib.graph.SparseBKGraph;
import lib.graph.SparseMatrixListGraph;
import lib.graph.bs.BSIntGraph;
import lib.graph.bs.IBSIntGraph;

public interface ParserImpl {
	

		public IntGraph getIntGraph(InputStream is);
		
		public BSIntGraph getBitSetIntGraph(InputStream is);
		
		public Map<Integer, List<Integer>> getMapListGraph(InputStream is);

		public IBSIntGraph getIBitSetIntGraph(InputStream is);

		public SparseBKGraph getSparseGraph(InputStream is);

		public MatrixGraph getMatrixGraph(InputStream is);

		public SparseBKDoubleGraph getSparseDoubleGraph(InputStream is);

		public SparseMatrixListGraph getSparseMatrixListGraph(InputStream is);
}
