package lib.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lib.graph.bs.BSIntGraph;

public class MatrixGraph implements IntGraph {

	private boolean[][] matrix;
	private Set<Integer> vertices;
	private List<Integer> verticesList;
	private int[] cardinalities = null;
	
	/**
	 * @deprecated
	 */
	public MatrixGraph(boolean[][] matrix){
		this.matrix = matrix;
		vertices = null;
	}
	
	/**
	 * Creates an empty matrix graph of cardinality n
	 * @param n - cardinality of the graph (note that the available labels go from 0 to n-1).
	 */
	public MatrixGraph(int n)
	{
		this.matrix = new boolean[n][n];
		for(int i = 0; i < n; i++)
		{
			Arrays.fill(matrix[i], false);
		}
		vertices = new HashSet<Integer>(n);
	}

	public MatrixGraph(BSIntGraph graph) {
		
		
		int maxLabel = graph.nodes().length();
		
		matrix = new boolean[maxLabel+1][maxLabel+1];
		vertices = new HashSet<Integer>();
		
		for(int i = 0; i <= maxLabel; i++)
		{
			for(int j = 0; j <= maxLabel; j++)
			{
				if(i!=j && graph.areNeighbors(i, j))
					{
						matrix[i][j] = true;
						vertices.add(i);
						vertices.add(j);
					}
			}
		}
		
	}

	public MatrixGraph(MapListGraph graph) {
		
		
		int maxLabel = -1;
		
		for(int i : graph.vertices())
		{
			if(i > maxLabel) maxLabel = i;
		}
		
		matrix = new boolean[maxLabel+1][maxLabel+1];
		vertices = new HashSet<Integer>(graph.vertices());
		
		for(int i : vertices)
		{
			for(int j : graph.neighbors(i))
			{
					matrix[i][j] = true;
			}
		}
	}
	
	public MatrixGraph(List<Set<Integer>> graph) {

		vertices = new HashSet<Integer>();
		
		int maxLabel = -1;
		
		for(int i = graph.size()-1; i >= 0; i--)
		{
			if(graph.get(i) != null && !graph.get(i).isEmpty())
				{
					vertices.add(i);
					
					if(i > maxLabel) maxLabel = i;
				}
		}
		
		matrix = new boolean[maxLabel+1][maxLabel+1];
		
		for(int i : vertices)
		{
			for(int j : graph.get(i))
			{
					matrix[i][j] = true;
			}
		}
	}
	
	
	@Override
	public boolean areNeighbors(int n1, int n2) {
		return matrix[n1][n2];
	}

	/**
	 * @deprecated as this is an adjacency matrix implementation,
	 * this method runs in O(n). It is suggested to use instead "boolean[] neighborsM(int)", which runs in O(1).
	 */
	public List<Integer> neighbors(int n){
		List<Integer> neighs = new ArrayList<Integer>();
		for(int i = 0; i<matrix.length; i++){
			if(matrix[n][i]) neighs.add(i);
		}
		return neighs;
	}
	
	public boolean[] neighborsM(int n){
		return matrix[n];
	}

	@Override
	public List<Integer> vertices() {
		if(vertices != null)
		{
			if( verticesList == null )verticesList = new ArrayList<Integer>(vertices);
		}
		else if(verticesList == null)
		{
			verticesList = new ArrayList<Integer>(matrix.length);
			for(int i = 0; i<matrix.length; i++){
				verticesList.add(i);
			}
		}
		return verticesList;
	}
	
	public void setNeighbors(int a, int b)
	{
		matrix[a][b] = true;
		matrix[b][a] = true;

		vertices.add(a);
		vertices.add(b);
	}
	public int cardinality(int node) {
		if(cardinalities == null)
		{
			cardinalities = new int[matrix.length];
			
			Arrays.fill(cardinalities, 0);
			
			for(int i : vertices())
			{
				for(int j = 0; j < matrix[i].length; j++)
				{
					if(matrix[i][j])
						cardinalities[i]++;
				}
			}
		}
		return cardinalities[node];
	}

}
