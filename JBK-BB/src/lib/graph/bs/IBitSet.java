package lib.graph.bs;

import java.util.BitSet;

public class IBitSet extends BitSet {

	private int firstSet = -1;
	
	public static void main(String[] args) {
		
	}
	
	/*
		public void and(IBitSet set){
			for(int i = firstSetBit(); i>=0; i=this.nextSetBit(i+1)){
				if(!set.get(i)){
					this.clear(i);
				}
			}
		}
		public void or(IBitSet set){
			for(int i = set.firstSetBit(); i>=0; i=set.nextSetBit(i+1)){
				if(set.get(i)){
					this.set(i);
				}
			}
		}
		public void andNot(IBitSet set){
			for(int i = firstSetBit(); i>=0; i=this.nextSetBit(i+1)){
				if(set.get(i)){
					this.clear(i);
				}
			}
		}
		*/
	public void and(IBitSet set){
		super.and(set);
		firstSet = nextSetBit(0);
	}
	public void or(IBitSet set){
		if(set.firstSetBit() < firstSetBit() && set.firstSetBit() >= 0){
			this.firstSet = set.firstSetBit();
		}
		super.or(set);
	}
	public void andNot(IBitSet set){
		super.andNot(set);
		firstSet = nextSetBit(0);
	}
	

		public int firstSetBit(){
			return firstSet;
		}
		
		public void set(int bitIndex){
			if((bitIndex<firstSet || firstSet == -1)&& bitIndex>=0){
				firstSet = bitIndex;
			}
			super.set(bitIndex);
		}
		public void set(int bitIndex, boolean value){
			if(value) set(bitIndex);
			else clear(bitIndex);
		}
		public void set(int fromIndex, int toIndex){
			for(int i = fromIndex; i < toIndex; i++){
				set(i);
			}
		}
		public void set(int fromIndex, int toIndex, boolean value){
			if(value) set(fromIndex,toIndex);
			else clear(fromIndex,toIndex);
		}

		public void clear(){
			int length = length();
			for(int i = nextSetBit(0); i< length; i++){
				clear(i);
			}
		}
		public void clear(int bitIndex){
			if(bitIndex == firstSet){
				firstSet = nextSetBit(firstSet+1);
			}
			super.clear(bitIndex);
		}
		public void clear(int fromIndex, int toIndex){
			int start = firstSet;
			int end = length();
			
			if(fromIndex > start) start = fromIndex;
			if(toIndex < end) end = toIndex;

			for(int i = start; i< end; i++){
				clear(i);
			}
		}
		
		
		
}
