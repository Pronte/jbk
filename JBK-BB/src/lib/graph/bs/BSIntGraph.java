package lib.graph.bs;

import java.util.*;

import lib.graph.Graph;

public class BSIntGraph implements Graph {
	
	public HashMap<Integer,BitSet> graph;
	public BitSet nodes;
	private List<Integer> verticesl;

	public BSIntGraph (){
		graph = new HashMap<Integer,BitSet>();
		
		nodes = new BitSet();
	}

	//return a list of all the nodes in the graph
	public BitSet nodes (){
		return nodes;
	}
	
	
	public List<Integer> vertices()
	{
		if(this.verticesl == null){
			this.verticesl = new ArrayList<Integer>(nodes.cardinality());
			
			for(int i = nodes.nextSetBit(0); i >= 0; i = nodes.nextSetBit(i+1)) verticesl.add(i);
		}
		return verticesl;
	}

	//return a list of all the neighbors of the node with label n
	public BitSet neighbors(int n){
		return graph.get(n);
	}
	
	//true if n1 and n2 are neighbors, false otherwise
	public boolean areNeighbors(int n1, int n2){
		return graph.get(n1).get(n2);
	}


	//building methods
	public void setNeighbors(int j, int k){
		
		//avoiding loops.
		if(j == k) return;
		
		if(graph.get(j) == null){
			graph.put(j,new BitSet());
		}
		if(graph.get(k) == null){
			graph.put(k,new BitSet());
		}

		graph.get(j).set(k);
		graph.get(k).set(j);
		nodes.set(j);
		nodes.set(k);
	}

	public int cardinality(int i) {
		return graph.get(i).cardinality();
	}
}