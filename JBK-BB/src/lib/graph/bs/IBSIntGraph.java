package lib.graph.bs;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;

import lib.graph.Graph;

public class IBSIntGraph implements Graph{


	public HashMap<Integer,IBitSet> graph;
	public IBitSet nodes;
	private List<Integer> verticesl;

	public IBSIntGraph (){
		graph = new HashMap<Integer,IBitSet>();
		
		nodes = new IBitSet();
	}

	//return a list of all the nodes in the graph
	public BitSet nodes (){
		return nodes;
	}
	
	
	public List<Integer> vertices()
	{
		if(this.verticesl == null){
			this.verticesl = new ArrayList<Integer>(nodes.cardinality());
			
			for(int i = nodes.nextSetBit(0); i >= 0; i = nodes.nextSetBit(i+1)) verticesl.add(i);
		}
		return verticesl;
	}

	//return a list of all the neighbors of the node with label n
	public IBitSet neighbors(int n){
		return graph.get(n);
	}
	
	//true if n1 and n2 are neighbors, false otherwise
	public boolean areNeighbors(int n1, int n2){
		return graph.get(n1).get(n2);
	}


	//building methods
	public void setNeighbors(int j, int k){
		
		//avoiding loops.
		if(j==k) return;
		
		if(graph.get(j) == null){
			graph.put(j,new IBitSet());
		}
		if(graph.get(k) == null){
			graph.put(k,new IBitSet());
		}

		graph.get(j).set(k);
		graph.get(k).set(j);
		nodes.set(j);
		nodes.set(k);
	}
	
}
