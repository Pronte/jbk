package lib;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class Logger {

	private static BufferedWriter log;
	
	
	public static void logStats(String file, AlgStats stats, int indent) {
		
		logln(file,indent);
		for(String s : stats.stats.keySet()){
			logln(" -"+s+": "+stats.get(s),indent);
		}
	}

	public static void log(String s){

		try {
			log.write(s);
			log.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void logln(String s){
		log(s+"\n");
	}
	public static void logln(String s, int indent){
		StringBuffer sb = new StringBuffer();
		
		for(int i = 0; i < indent; i++)	sb.append("   ");
		sb.append(s);
		
		logln(sb.toString());
	}
	public static void closeLog(){
		try {
			log.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void initLog(String fname){
		String logfilename = fname+"-results";
		
		
		File f = new File(logfilename+".txt");
		
		int i = 0;
		while(f.exists()){
			i++;
			f = new File(logfilename+"-"+i+".txt");
		}
		
		try {
			f.createNewFile();
			log = new BufferedWriter(new FileWriter(f));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
	}
	
	public static boolean filter (String fname){
		
		if(fname == null || fname.isEmpty()) return false;
		
		if(fname.endsWith(".txt") ||fname.endsWith(".java") ||fname.endsWith(".class")){
			return false;
		}
		
		return true;
		
	}
}
