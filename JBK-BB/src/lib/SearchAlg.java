package lib;

import java.util.Set;


public interface SearchAlg {

    public void search();

    public void setTimeLimit(long limit);
    
    public AlgStats getStats();

	public void setAborted();

	public void printStats();
	
	public Object solution();
	
	public void setFilters(Set<Integer> kernel, Set<Integer> borderVis);
}
