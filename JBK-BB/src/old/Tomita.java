package old;

import java.util.*;

/**
 *@author: Alessio Conte, Patrick Prosser
 *
 */
public class Tomita extends BK {

    public Tomita (int n,boolean[][] A,int[] degree) {
	super(n,A,degree);
	cliques = 0;
    }

	
    /**
     *@param C: Clique, P: "Possible", N: "not" (excluded set)
     */
    void expand(List<Integer> C,List<Integer> P,List<Integer> N){
	nodes++;
	if (P.isEmpty() && N.isEmpty()){report(C);  return;}

	//updating the P set according to the Tomita pivoting strategy.
	//A pivot is chosen from the union of the sets P and N,
	//then the iteration is not performed on the neighbors of the pivot,
	//since those branches would have to include the pivot itself at some point, replicating the result of this branch,
	//hence they can be cut.
	if(!P.isEmpty()){	
		int pivot = getTomitaPivot(P,N);

		List<Integer> tP = new ArrayList<Integer>(P.size()); //Tomita's P: this set is used for the iteration of the algorithm, but the set P of the candidates remains the same.

		for(int v : P){
			if ( !A[pivot][v] || pivot == v) tP.add(v);
		}
		
		int m = tP.size(); // if P is empty loop is not executed
		for (int i=m-1;i>=0;i--){
		    int v = tP.get(i);
		    C.add(v);
		    ArrayList<Integer> newP = new ArrayList<Integer>(i);
		    ArrayList<Integer> newN = new ArrayList<Integer>(i);
		    for (int w : P) if (A[v][w]) newP.add(w);
		    for (int w : N) if (A[v][w]) newN.add(w);
		    expand(C,newP,newN);
		    C.remove((Integer)v);
		    P.remove((Integer)v);
		    N.add(v);
		}
	}

    }

    /**
     *Tomita's Pivoting strategy.
     *The pivot is chosen as the node in P U N
     *with the highest number of neighbors in P.
     */
    protected int getTomitaPivot(List<Integer> P, List<Integer> N){
    	int best = P.get(0);
    	int bestVal = neighborsIn(P,best);

    	int node;
    	int val;

    	for(int i = 1; i<P.size(); i++){
    		node = P.get(i);
    		val = neighborsIn(P,node);

    		if(val>bestVal){
    			best = node;
    			bestVal = val;
    		}
    	}

    	//If N is not empty, extend the search on N
    	if(!N.isEmpty()){
    		for(int i = 0; i<N.size(); i++){
    			node = N.get(i);
    			val = neighborsIn(P,node);

    			if(val>bestVal){
    				best = node;
    				bestVal = val;
    			}
    		}
    	}

    	return best;
    }

    protected int neighborsIn(List<Integer> P, int node){
    	int neighNum = 0;
    	for(int n : P){
    		if(A[n][node]) neighNum++;
    	}
    	return neighNum;
    }

}
