package old;

import java.util.*;

public class BK extends MC {

    int cliques;
    boolean print = false;

    BK (int n,boolean[][] A,int[] degree) {
	super(n,A,degree);
	cliques = 0;
    }

    void search(){
	cpuTime              = System.currentTimeMillis();
	nodes                = 0;
	ArrayList<Integer> C = new ArrayList<Integer>(n);
	ArrayList<Integer> P = new ArrayList<Integer>(n);
	ArrayList<Integer> N = new ArrayList<Integer>(n); // not set
	for (int i=0;i<n;i++) P.add(i);
	expand(C,P,N);
	System.out.println("cliques: "+ cliques);
    }

    void expand(ArrayList<Integer> C,ArrayList<Integer> P,ArrayList<Integer> N){
	nodes++;
	if (P.isEmpty() && N.isEmpty()){report(C);  return;}
	int m = P.size(); // if P is empty loop is not executed
	//for (int i=m-1;i>=0;i--){
	  for (int i=0;i<m;i++){
	    int v = P.get(0);
	    C.add(v);
	    ArrayList<Integer> newP = new ArrayList<Integer>(m/2);
	    ArrayList<Integer> newN = new ArrayList<Integer>(m/2);
	    for (int w : P) if (A[v][w]) newP.add(w);
	    for (int w : N) if (A[v][w]) newN.add(w);
	    expand(C,newP,newN);
	    C.remove((Integer)v);
	    P.remove((Integer)v);
	    N.add(v);
	}
    }

    protected void report(List<Integer> c){
    	cliques++;
    	if(c.size()>maxSize) maxSize = c.size();
    	if(print) {System.out.println(c);}
    }
}
