package old;

import java.util.*;
import java.io.*;

/**
 *@author Patrick Prosser, modified by Alessio Conte.
 */
public class MCEnv {

    static int[] degree;   // degree of vertices
    static boolean [][] A; // adjacency matrix
    static int n;

    static void readDIMACS(String fname) throws IOException {
	String s = "";
	Scanner sc = new Scanner(new File(fname));
	while (sc.hasNext() && !s.equals("p")) s = sc.next();
	sc.next();
	n      = sc.nextInt();   
	int m  = sc.nextInt();   
	degree = new int[n];
	A      = new boolean[n][n];
	while (sc.hasNext()){
	    s     = sc.next(); // skip "edge"
	    int i = sc.nextInt() - 1;
	    int j = sc.nextInt() - 1; 
	    degree[i]++; degree[j]++;
	    A[i][j] = A[j][i] = true;
	}
	sc.close();
    }

    public static void main(String[] args)  throws IOException , Exception{

	readDIMACS(args[1]);
	MC mc = null;
	if (args[0].equals("MC"))         mc = new MC(n,A,degree);
	else if (args[0].equals("BK"))    mc = new BK(n,A,degree);
	else 	// try {
				//Constructor c = Class.forName(args[0]).getConstructor(Integer.TYPE, boolean[][].class, int[].class);
				mc = (MC) Class.forName(args[0]).getConstructor(Integer.TYPE, boolean[][].class, int[].class).newInstance(n,A,degree);
			//} 
			/*
			catch (Exception e){
				System.out.println("Unable to istantiate the class "+args[0]);
				System.out.println(e);
			return;
			} */

	System.gc();
	if (args.length > 2) mc.timeLimit = 1000 * (long)Integer.parseInt(args[2]);
	long cpuTime = System.currentTimeMillis();
	mc.search();
	cpuTime = System.currentTimeMillis() - cpuTime;
	System.out.println("Max size: "+ mc.maxSize);
	System.out.println("Nodes: "+ mc.nodes);
	System.out.println("Cpu Time: "+ cpuTime);
	for (int i=0;i<mc.n;i++) if (mc.solution[i] == 1) System.out.print(i+1 +" ");
	System.out.println();
    }
}
