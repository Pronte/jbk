package eps.bitsets;

import java.io.InputStream;
import java.util.BitSet;

import lib.graph.Graph;
import lib.parser.Parser;

public class EPSMaxDeg extends EPS1 {

	public EPSMaxDeg(Graph g)
	{
		super(g);
	}

	public EPSMaxDeg(InputStream is) {
		this((Graph) Parser.getBitSetIntGraph(is));
	}




	@Override
	public void selectEdgeToExpand()
	{



		int size = graph.nodes().length();
		int maxV = Integer.MIN_VALUE;

		seli = selj = -1;

		for(int i = 0; i < size; i++)
		{

			//BitSet neighs = graph.neighbors(i);
			BitSet cleanNeighs = cleanGraph.get(i);

			if(cleanNeighs == null);// System.out.println("No priority map for "+i);
			else
				for(int j = cleanNeighs.nextSetBit(0); j >= 0; j = cleanNeighs.nextSetBit(j+1))
				{

					if(graph.cardinality(i) > maxV)
					{
						seli = i;
						selj = j;
						maxV = graph.cardinality(i);
					}
					if(graph.cardinality(j) > maxV)
					{
						seli = i;
						selj = j;
						maxV = graph.cardinality(j);
					}
					//if(minV <= 2) return;
				}
		}
	}


}
