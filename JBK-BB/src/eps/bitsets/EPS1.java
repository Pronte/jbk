package eps.bitsets;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

import lib.graph.Graph;
import lib.parser.Parser;

public class EPS1 extends EPSBS {

	protected Map<Integer,BitSet> cleanGraph;
	
	protected int seli, selj;
	
	public EPS1(Graph graph) {
		super(graph);
	}
	public EPS1(InputStream is)
	{
		this(Parser.getBitSetIntGraph(is));
	}
	
	@Override
	public final void start() {
		
		//Priority Queue for Edges
		
		//int size = graph.nodes().size();
		
		solution = new ArrayList<BitSet>();
		deaths = 0;

		expand0();
		
		if(checkSolution) checkSolution();
		
	}
	
	
	public void expand0()
	{
		

		cleanGraph = new HashMap<Integer,BitSet>();
		for(int i = graph.nodes().nextSetBit(0); i>=0; i = graph.nodes().nextSetBit(i+1))
		{
			cleanGraph.put(i,(BitSet)graph.neighbors(i).clone());
		}
		if(!cleanGraph.containsKey(0))
			cleanGraph.put(0, new BitSet());
		
		selectEdgeToExpand();
		while(seli != -1)
		{
			
			/**
			 * -select edge
			 * process
			 * -select another edge until halt
			 */
			
			BitSet P = (BitSet)(graph.neighbors(seli).clone());
			P.and(graph.neighbors(selj));
			
			//if(P.nextSetBit(0) >= seli || P.nextSetBit(0) >= selj) return; //cutting: finding only cliques in order
			
			BitSet C = new BitSet();
			
			C.set(seli);
			C.set(selj);
			
			
			expand(C,P);
			
			selectEdgeToExpand();
		}
		

		
	}
	
	public void expand(BitSet C, BitSet P)
	{
		nodes++;
		
		if(P.isEmpty())
		{
			report(C);
			return;
		}
		
		int max=-1, maxClean=-1, maxSClean=-1,
			maxVal=-1,maxCVal=-1,maxSCVal=-1;
		
		//int maxC = C.length() -1;
		
		for(int i = P.nextSetBit(0); i >=0; i = P.nextSetBit(i+1))
		{
			BitSet cleanNeighs = cleanGraph.get(i);
			if(!cleanNeighs.isEmpty())
			{
				
				for(int cn = cleanNeighs.nextSetBit(0); cn >= 0; cn = cleanNeighs.nextSetBit(cn+1))
				{
					if(C.get(cn)) //node i has a neighbor in C, so it is clean
					{
						if(graph.cardinality(i) >= maxCVal)
						{
							maxCVal = graph.cardinality(i);
							maxClean = i; //pivot
							break;
						}
					}
				}
				
				if(maxClean ==-1 && graph.cardinality(i) >= maxSCVal) //node i is semi-clean (and no clean node has been found yet)
				{
					maxSCVal = graph.cardinality(i);
					maxSClean = i; //pivot
				}
			}
			else if(maxSClean ==-1 && maxClean==-1 && graph.cardinality(i) >= maxVal) //no clean or semiClean nodes found yet, and current node is dirty
			{
				maxVal = graph.cardinality(i);
				max = i; //pivot
			}
		}
		
		if(maxClean != -1)
		{
			max = maxClean;
		}
		else if(maxSClean != -1)
		{
			max = maxSClean;
		}
		else //no clean vertices, breaking maximality constraint and stopping.
		{
//			P.clear();
//			P = null;
			report(C);
			return;
		}
	
		//only retrieving 1 clique, iterating on the pivot
		process(C,P,max);


		/*
		for(int i = P.nextSetBit(0); i >=0; i = P.nextSetBit(i+1))
		{
			if(i == max || !graph.areNeighbors(i, max))
			{
				if(i>maxC) //selective: only finding cliques in crescent order!
				{
					process(C,P,i);
				}
			}
		} 
		*/
	}
	
	public void process(BitSet C, BitSet P, int x)
	{
		C.set(x);
		P.and(graph.neighbors(x));
		
		if(P.isEmpty())
		{
			report(C);
		}
		else
		{
			expand(C, P);
		}
		//C.clear(x);
	}
	
	/**
	 * Adds C to the solution and marks all edges in C as dirty.
	 */
	public void report(BitSet C)
	{
		//if(cliques%100 == 0) System.out.println(cliques+" cliques");
		cliques++;
		deaths += C.cardinality(); //abused variable for solution sum size
		solution.add(C);
		
		for(int i= C.nextSetBit(0); i>=0; i=C.nextSetBit(i+1)) //for each node in C
		{
			cleanGraph.get(i).andNot(C); //remove all nodes in C from its clean neighbors (i.e. mark the edges as dirty)
		}
	}
	
	/**
	 * Selects a clean edge, and puts its extremes in 'seli' and 'selj'
	 * If there are no clean edges, 'seli' is put to -1
	 * (all valid node IDs are assumed to be >= 0)
	 */
	public void selectEdgeToExpand()
	{
		int start = (int)(Math.random()*graph.nodes().size());
		int size = graph.nodes().length();
		int i;
		
		for(int ii = 0; ii < size; ii++)
		{
			i= (ii+start)%size;

			//BitSet neighs = graph.neighbors(i);
			BitSet cleanNeighs = cleanGraph.get(i);

			if(cleanNeighs != null && !cleanNeighs.isEmpty())
			{
				int randomPos = (int) (Math.random()*cleanNeighs.size()); //TODO check performance and quality impact
				
				int j = cleanNeighs.nextSetBit(randomPos); //TODO check performance and quality impact
				if(j == -1) j = cleanNeighs.nextSetBit(0);
				

				if(j != -1)
				{
					seli = i;
					selj = j;
					return;
				}
				
			}
			
		}

		//No clean edges in the graph, triggering halt condition.
		seli = -1;
		selj = -1;
	}

}