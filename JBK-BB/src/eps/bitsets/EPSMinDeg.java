package eps.bitsets;

import java.io.InputStream;
import java.util.BitSet;
import java.util.Map;

import lib.graph.Graph;
import lib.parser.Parser;

public class EPSMinDeg extends EPS1 {

	public EPSMinDeg(Graph g)
	{
		super(g);
	}
	
	public EPSMinDeg(InputStream is) {
		this((Graph) Parser.getBitSetIntGraph(is));
	}

	
	
	
	@Override
	public void selectEdgeToExpand()
	{
		
		

		int size = graph.nodes().length();
		int minV = Integer.MAX_VALUE;

		//TODO: Temp condition, only accepting edges with value 0.
		seli = -1;
		selj = -1;
		
		for(int i = 0; i < size; i++)
		{

			//BitSet neighs = graph.neighbors(i);
			BitSet cleanNeighs = cleanGraph.get(i);

			if(cleanNeighs == null);// System.out.println("No priority map for "+i);
			else
				for(int j = cleanNeighs.nextSetBit(0); j >= 0; j = cleanNeighs.nextSetBit(j+1))
				{
					if(graph.cardinality(i) < minV) //TODO improvised condition
					{
						seli = i;
						selj = j;
						minV = graph.cardinality(i); //cleanGraph.get(i).cardinality();
					}
					if(cleanGraph.get(j).cardinality() < minV)
					{
						seli = i;
						selj = j;
						minV = graph.cardinality(j); //cleanGraph.get(j).cardinality();
					}
					if(minV <= 2) return;
				}
		}
		
	}
	
	
}
