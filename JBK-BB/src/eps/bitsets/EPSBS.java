package eps.bitsets;

import java.io.InputStream;
import java.util.BitSet;
import java.util.List;
import java.util.Set;

import lib.graph.Graph;
import lib.graph.bs.BSIntGraph;
import lib.parser.Parser;
import eps.EPS;

public abstract class EPSBS extends EPS {

	protected BSIntGraph graph;
    protected List<BitSet> solution;
	
	public EPSBS(Graph graph) {
		super(graph);
		this.graph = (BSIntGraph) graph;
	}

	public EPSBS(InputStream is) {
		this(Parser.getBitSetIntGraph(is));
	}

	public Object solution()
	{
		return this.solution;
	}
	
	public boolean checkSolution()
	{
		if(solution == null){
			System.out.println("No solution");
			return false;
		}
		
		BSIntGraph g = new BSIntGraph();
		
		for(BitSet clq : solution)
		{
			for(int i = clq.nextSetBit(0); i>= 0 ; i = clq.nextSetBit(i+1))
			{
				for(int j = clq.nextSetBit(0); j>= 0 ; j = clq.nextSetBit(j+1))
				{
					if(i != j)
					{
						g.setNeighbors(i, j);
					}
				}
			}
		}
		
		BitSet nodes = graph.nodes();
		for(int i = nodes.nextSetBit(0); i>= 0 ; i = nodes.nextSetBit(i+1))
		{
			BitSet neighs = graph.neighbors(i);
			
			if(neighs != null)
				for (int j = neighs.nextSetBit(0); j >= 0; j = neighs.nextSetBit(j+1))
				{
					try{
					if(!g.areNeighbors(i, j)){
						System.out.println("The edge "+i+","+j+" is not covered in the solution!!");
						return false;
					}
					} catch(Exception e)
					{
						System.out.println("The edge "+i+","+j+" raised an exception!!");
					}
				}
			
		}
		
		System.out.println("The solution is correct.");
		return true;
	}
	
	@Override
	public void setFilters(Set<Integer> kernel, Set<Integer> borderVis) {
		System.out.println("Filters not needed/implemented for this class of algorithms.");
	}
}
