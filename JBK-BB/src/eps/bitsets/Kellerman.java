package eps.bitsets;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lib.graph.Graph;
import lib.parser.Parser;

public class Kellerman extends EPS1 {

	protected Map<Integer,Map<Integer,Integer>> priorityGraph;
	
	protected int seli, selj, minV;
	
	public Kellerman(Graph graph) {
		super(graph);
	}
	public Kellerman(InputStream is)
	{
		this(Parser.getBitSetIntGraph(is));
	}
	
	@Override
	public Object solution() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void expand0() {
		
		//Priority Queue for Edges
		
		//int size = graph.nodes().size();
		/*
		priorityGraph = new HashMap<Integer,Map<Integer,Integer>>();
		minV=0;
		
		for(int i = graph.nodes().nextSetBit(0); i>=0; i = graph.nodes().nextSetBit(i+1))
		{
			BitSet neighs = graph.neighbors(i);
			HashMap<Integer,Integer> m = new HashMap<Integer,Integer>(); 
			priorityGraph.put(i,m);
			
			for(int j = neighs.nextSetBit(0); j>=0; j=neighs.nextSetBit(j+1))
			{
				m.put(j, 0);
			}
		}
		priorityGraph.put(0, new HashMap<Integer,Integer>());
		*/
		int maxLabel = 1+graph.nodes().length();
		System.out.println("ml = "+maxLabel);
		List<BitSet> clqs = new ArrayList<BitSet>();
		
		for(int i = graph.nodes().nextSetBit(0); i>=0; i = graph.nodes().nextSetBit(i+1))
		{
			
			BitSet w = (BitSet) graph.neighbors(i).clone();
			w.clear(i, maxLabel); //all edges from i to nodes smaller than i
//			System.out.println("i: "+i+" , w: "+w.toString());
			if(w.isEmpty())
			{
				BitSet newClq = new BitSet();
				newClq.set(i);
				clqs.add(newClq);
			}
			else
			{
				BitSet u = new BitSet();
				
				for(BitSet clq : clqs)
				{
					boolean included = true;
					
					for(int n = clq.nextSetBit(0); n >= 0 ; n = clq.nextSetBit(n+1))
					{
						if(!w.get(n))
						{
							included = false;
							break;
						}
					}
					
					if(included)
					{
						u.or(clq); //set clq vertices as covered
						clq.set(i);
						
						if(u.cardinality() >= w.cardinality())
						{
							if(u.equals(w))
							{
								break;
							}
							else
							{
								System.out.println("Error!");
								System.out.println("U:"+u.toString());
								System.out.println("W: "+w.toString());
							}
						}
					}
				}
				w.andNot(u);
				int cicles = 0;
				while(!w.isEmpty())
				{
					cicles++;
					if(cicles % 1000 == 0) System.out.println(cicles+" cicles in the while, i ="+i);
					//TODO optimize
					BitSet max = new BitSet(), tmp;
					for(int ci = 0; ci < clqs.size(); ci++)
					{
						tmp = ((BitSet)clqs.get(ci).clone());
						tmp.and(w);
						
						if(tmp.cardinality() > max.cardinality())
						{
							max = tmp;
						}
					}
					
					max.set(i);
					w.andNot(max);
					clqs.add(max);
				}
			}
			
		}
		
		
		cliques = clqs.size();
		deaths = 0;
		for(BitSet cc : clqs) deaths += cc.cardinality();
		
		solution = clqs;
	}

}
